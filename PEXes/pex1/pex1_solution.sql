-- CS 364 PEX 1 Solution

-- Instructions:
--    Run sakila-schema.sql
--      The file is in data/sakila_db
--      When prompted, choose rename
--    Run sakila-data-modified.sql
--      The file is in data/sakila_db
--      When prompted, accept latin1 by clicking OK

DROP DATABASE IF EXISTS cs364_pex1;
CREATE DATABASE IF NOT EXISTS cs364_pex1;
USE cs364_pex1;

-- #1 --------------------------------------------------------------------------
/* Create a soaring database for tracking soaring pilots and the
   maneuvers they perform during competitions.  (See also the tables in
   question 2 for an idea what the database should look like.)
   a. For the pilots, store a pilot ID number, a pilot last name, and a
      pilot phone number.  Ensure you set an appropriate primary key.
   b. For the maneuvers, store a maneuver performed ID number, maneuver
      name, maneuver grade, and maneuver date.  Ensure you set an
      appropriate primary key.  Ensure you define any foreign keys that are
      appropriate.
   c. Require that all pilots and maneuvers have values for all attributes.
   d. Track the pilot that performed each maneuver assuming that each
      maneuver performance can be done by only one pilot but that each pilot
      can do many maneuver performances.
   e. Ensure that no pilot can be deleted out of the database until all her
      maneuvers are manually deleted first.
   f. If a pilot changes pilot ID numbers, ensure the pilot ID is updated
      appropriately in the maneuvers table. */

CREATE TABLE pilot (
  pilot_id      CHAR(3)     PRIMARY KEY,
  pilot_lname   VARCHAR(30) NOT NULL,
  pilot_phone   CHAR(10)    NOT NULL
);

CREATE TABLE maneuver (
  maneuver_id     CHAR(4)     PRIMARY KEY,
  maneuver_name   VARCHAR(30) NOT NULL,
  maneuver_grade  CHAR(1)     NOT NULL,
  maneuver_date   DATE        NOT NULL,
  pilot_id        CHAR(3)     NOT NULL REFERENCES pilot(pilot_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
);

-- Alternative approach

CREATE TABLE pilot (
  pilot_id      CHAR(3)     NOT NULL UNIQUE,
  pilot_lname   VARCHAR(30) NOT NULL,
  pilot_phone   CHAR(10)    NOT NULL,
  PRIMARY KEY (pilot_id)
);

CREATE TABLE maneuver (
  maneuver_id     CHAR(4)     NOT NULL UNIQUE,
  pilot_id        CHAR(3)     NOT NULL,
  maneuver_name   VARCHAR(30) NOT NULL,
  maneuver_grade  CHAR(1)     NOT NULL,
  maneuver_date   DATE        NOT NULL,
  PRIMARY KEY (maneuver_id),
  FOREIGN KEY (pilot_id) REFERENCES pilot(pilot_id)
    ON DELETE RESTRICT ON UPDATE CASCADE
);

-- #2 --------------------------------------------------------------------------
/* Add the following information to your pilot maneuvers
   performed database:

     Pilots:
     Pilot ID        Pilot Last Name         Pilot Phone
     001             Yeager                  7195551212
     002             Doolittle               3165551212
     003             Earhart                 2125551212

     Maneuvers Performed:
     Maneuver ID     Pilot       Maneuver Name     Maneuver Grade    Maneuver Date
     0001            Doolittle   Split S           C                 13 Jan 2015
     0002            Yeager      Wingover          A                  3 Dec 2014
     0003            Earhart     Wingover          B                 13 Jan 2015 */

INSERT INTO pilot VALUES
  ('001', 'Yeager', '7195551212'),
  ('002', 'Doolittle', '3165551212'),
  ('003', 'Earhart', '2125551212');

INSERT INTO maneuver VALUES
  ('0001', 'Split S', 'C', '2015-01-13', '002'),
  ('0002', 'Wingover', 'A', '2014-12-03', '001'),
  ('0003', 'Wingover', 'B', '2015-01-13', '003');

-- #3 --------------------------------------------------------------------------
/* Write SQL commands to do the following:
   a. There has been a change to the grading criteria, and all wingover
      maneuvers will now be ungraded.  Change the database so that all
      wingover maneuvers are graded 'P' for performed.
   b. Remove all maneuvers performed before year 2015.
   c. Show all attributes for all pilots.                                  */

UPDATE  maneuver
SET     maneuver_grade = 'P'
WHERE   maneuver_name = 'Wingover';

DELETE  FROM maneuver
WHERE   YEAR(maneuver_date) < 2015;

SELECT * FROM pilot;

-- FOR THE REMAINDER OF THE ASSESSMENT USE THE SAKILA DATABASE --

USE sakila;

-- #4 --------------------------------------------------------------------------
/* Write a query to obtain the customer ID, staff ID, rental
   ID, payment date, and amount for all payments greater than eight dollars
   made in either July or August of 2005.  Order them by payment date.
   (There are 673 such records.)                                           */

SELECT  c.customer_id, s.staff_id, r.rental_id, p.payment_date, p.amount
FROM    customer c
  JOIN rental r USING (customer_id)
  JOIN payment p USING (rental_id)
  JOIN staff s ON r.staff_id = s.staff_id
WHERE p.amount > 8 AND
  p.payment_date >= '2005-07-01' AND p.payment_date <= '2005-08-31'
ORDER BY p.payment_date;

-- Alternative
SELECT  c.customer_id, s.staff_id, r.rental_id, p.payment_date, p.amount
FROM    customer c
  JOIN rental r USING (customer_id)
  JOIN payment p USING (rental_id)
  JOIN staff s ON r.staff_id = s.staff_id
WHERE p.amount > 8 AND
  YEAR(p.payment_date = 2015) AND
  (MONTH(p.payment_date) = 7 OR MONTH(p.payment_date) = 8)
ORDER BY p.payment_date;

-- #5 --------------------------------------------------------------------------
/* Write a query to obtain the first name and last name of
   each customer who has not rented a video.  The result set is below:

   first_name    last_name
   Bryan         Williams                                                  */

SELECT  first_name, last_name
FROM    customer
WHERE   customer_id NOT IN (SELECT customer_id FROM rental);

-- Alternate
SELECT  first_name, last_name
FROM    customer c
  LEFT JOIN rental r USING (customer_id)
WHERE   r.rental_id IS NULL;

-- #6 --------------------------------------------------------------------------
/* Report the average number of times that videos beginning
   with the letter A were rented. (Hint: each video was rented at least
   once, so you need not check for videos never checked out.)  Use the
   column name average_times_rented in your result.  The result set is
   below.

   average_times_rented
   17.6585                                                                 */

SELECT  AVG(rental_count) FROM (
SELECT  COUNT(r.rental_id) AS rental_count
FROM    film f
  JOIN inventory i USING (film_id)
  JOIN rental r  USING (inventory_id)
WHERE f.title LIKE ('A%')
GROUP BY f.film_id) AS temp;

-- Alternate
CREATE VIEW rental_counts AS
SELECT  COUNT(r.rental_id) AS rental_count
FROM    film f
  JOIN inventory i USING (film_id)
  JOIN rental r  USING (inventory_id)
WHERE f.title LIKE ('A%')
GROUP BY f.film_id;

SELECT AVG(rental_count) FROM rental_counts;

-- #7 --------------------------------------------------------------------------
/* Produce a report showing the number of times each actor has
   acted in each category of film.   List only those actors who performed
   in a particular category 6 or more times.  Order by performances so that
   the actor who performed the most times in one category is listed first.
   Show the following fields: actor first name, actor last name, category,
   and number performances.  The first several rows are shown below (your
   solution may have a different order after the first record).

   first_name      last_name   Category    Count(film_id)
   Ben             Willis      Sports      9
   Julia           McQueen     Horror      7
   Humphrey        Willis      Foreign     7                               */

SELECT  a.first_name, a.last_name, c.name AS Category, COUNT(film_id)
FROM    actor a
  JOIN film_actor fa USING (actor_id)
  JOIN film f USING (film_id)
  JOIN film_category fc USING (film_id)
  JOIN category c USING (category_id)
GROUP BY actor_id, category_id
HAVING COUNT(film_id) >= 6
ORDER BY COUNT(film_id);

-- #8 --------------------------------------------------------------------------
/* Give an interesting query of your own that is not already in the
   assignment. The query should involve an aggregation operation, and a
   nested SELECT. Give, along with the query, the English explanation and
   the answer.                                                            */







