# CS364 Spring 2016 Final Project
*Note*: If you have installed a Markdown plugin to your IDE or into your browser, you can view this file as a webpage

## Table of Contents

+ [Project Due Dates](#markdown-header-project-due-dates)
+ [Getting Started](#markdown-header-getting-started)
+ [Final Project Overview](#markdown-header-final-project-overview)
+ [Submission Requirements](#markdown-header-submission-requirements)
+ [Final Project Requirements](#markdown-header-submission-requirements)
+ [Deliverables](#markdown-header-deliverables):  
    + [Proposal](#markdown-header-proposal)      
    + [Design Specification](#markdown-header-design-specification)     
    + [Tracer Bullet](#markdown-header-tracer-bullet)      
    + [Sprint 1](#markdown-header-sprint-1)      
    + [Final](#markdown-header-final-turn-in)  
+ [Attachment 1: README.md template](#markdown-header-attachment-1)
+ [Attachment 2: Project Setup Files](#markdown-header-attachment-2)
+ [Attachment 3: MySQL and MongoDB from the Command Line](#markdown-header-attachment-3)
    + [*MongoDB*](#markdown-header-mongodb)
    + [*MySQL*](#markdown-header-mysql)
+ [Attachment 4: Git Commands and Workflows](#markdown-header-attachment-4)
+ [Attachment 5: JSON Schema](#markdown-header-attachment-5)
+ [Attachment 6: Helpful Links](#markdown-header-attachment-6)


## Project Due Dates
 | Assignment    | Date             | Notes | Points |
 |---------------|------------------|-------|--------|
 | Proposal      | 2359 19 Apr 2016 |       |        |
 | Tracer Bullet | 2359 22 Apr 2016 |       |        |
 | Design Spec   | 2359 24 Apr 2016 |       |        |
 | Sprint 1      | 2359 3 May 2016  |       |        |
 | Final         | 2359 13 May 2016 |       |        |


## Getting Started
### Setup Final Project Repository
 *One* team member from each team should execute the following steps:

 1. Create a new repository in your bitbucket account
    + Name the repository `USAFA_CS364_2016_Final`
    + The repository should be private
 2. Change the settings of your repository:
    + Click on the gear icon and then select Access management
    + Add your team members, giving them at least write access
    + Add your instructor, giving him at least read access
 3. Email your instructor letting him know the steps above are complete and include in your email the names of your team members

## Final Project Overview
*Description*: Your final project will consist of a web application supported by a database backend.  The website will provide a way of interacting with the database in way that is attractive, easy to use, secure and robust, with a consistent look and feel.

### Help Policy
**Authorized Resources**: Any

+ Do not copy another person's work and submit it as your own
+ With the exception of packages that you install via `npm install`, do not copy code from another source and paste it into your final project. This includes your own previous work or code provided by your instructor. Part of the objective of this final project is to help reinforce concepts that you have learned and practiced throughout the semester, not just reuse them.
+ Do not jointly work on this assignment with other teams, but you may consult with and get help from others
+ DFCS will recommend a course grade of F for any cadet who egregiously violates this Help Policy or contributes to a violation by others.

### Documentation Policy
+ You must document all help received from any source. Unless quoting directly or paraphrasing, you do not need to document your course text, lectures, or any other course materials provided by your instructor. Rather documenting packages that you installed into your application via `npm install` or that you import via `<script>` tags, you will list them as dependencies.
+ Each documentation statement must be specific.  Ensure that every documentation statement explicitly describes **what** assistance was provided, **how** it was used in completing the assignment, and **who** provided the assistance.
+ If no help was received on this assignment, the document statement must state "None."
+ Vague documentation statements must be correct before the assignment will be graded and will result in a 5% deduction on the assignment.
+ Submit documentation statements in accordance with "submission requirements" section below.

## Submission Requirements
*Note*: In addition to these general submission requirements, see the specific submission requirements for each deliverable

Your git repository should have a similar directory structure as the following:
```bash
.
+---client\
|   |
|   +---app\
|   |
|   +---assets\
|   |   +---img\        // Images and icons
|   |   +---css\        // All styles and style related files
|   |   +---js\         // JavaScript files written for app that are not for angular
|   |   +---libs        // Third-party libraries
|   |
|   +---index.html
|   
+---node_modules\
|
+---server\
|   |
|   +---controllers\
|   |   +---products.js
|   |   +---routes.js
|   |   +---users.js
|   |
|   +---models\
|   |   +---products.js
|   |   +---users.js
|   |
|   +---seed\
|   |   +---users-seed.json                      //if using MongoDB
|   |   +---products-seed.json                   //if using MongoDB
|   |   +---...as many .json files are necessary //if using MongoDB
|   |   +---mydb-schema.sql                      //if using MySQL
|   |   +---mydb-data.sql                        //if using MySQL
|   |   
|   +---services\
|   |   +---db.js
|   |   
|   +---app.js
|   +---initialize.bat
|  
+---.gitignore   
+---initialize.bat
+---package.json
+---README.md
```


+ All submissions will be submitted via a `git push` to the remote repository on bitbucket of one team member. Submissions must include the appropriate comment and tag (see specific submission requirements) for the current submission. For example, the proposal submission would require the following workflow (or a similar workflow in an IDE):

```bash
git add *
git commit -am "submit proposal"
git tag -a proposal -m "submit proposal"
git push
git push --tags
```

+ The repository that is pushed must be the **master** branch and include all previous submissions (as modified). In other words, if your team is working on multiple branches, and one of the assignments is completed on a separate branch, that branch must be merged back onto the master branch before submitting the master branch. Do not infer from this requirement that you should not use separate branches for development. Branches can be very helpful for managing a team without corrupting previous working versions of your code.
+ Each submission will require the `README.md` file to be updated prior to submission, according to the specific submission requirements. (see the sample `README.md` file in [Attachment 1](#markdown-header-attachment-1))
+ With the exception of the proposal and design specification, you will need to provide a script in your **packsage.json** that will execute your web application with all the appropriate command line options using `npm start` from the command line.
+ Your **package.json** should be complete so that the instructor could run `npm install` and download all required libraries.
+ Do not include your `node_modules` directory in your git repository (see the sample `.gitignore` file in [Attachment 2](#markdown-header-attachment-2))
+ Add all commands necessary to initialize the web application data and to download and install the npm packages to a `initialize.bat` file. This `initialize.bat` should not start your web application. (see the sample `initialize.bat` files in [Attachment 3](#markdown-header-attachment-3) and the sample `package.json` file with `scripts` syntax in [Attachment 2](#markdown-header-attachment-2)
+ If the previous three requirements are implemented, your instructor should be able to run to the following commands from the command line, and assuming the database server is already running, start your web application:

```bash
initialize.bat
npm start
```

## Final Project Requirements
1. General Requirements
    1. Develop a web application with 3-5 web pages served from a *node.js* server interact with data stored in 3-5 related data tables. Additional complexity is okay.
    2. Complete the project in 2-person teams. 1 and 3-person teams are acceptable with prior instructor approval. 3-person teams will require increased complexity and functionality (4-5 web pages and 4-5 data tables).
    3. Implement user roles.
    4. Use git to manage and to submit your project.  
2. Specific Requirements
    1. Data layer
        1. You may use either MySQL or MongoDB
        2. Use a consistent naming convention for tables and attributes
        3. Use 3-5 related tables.
            1. Using more tables is acceptable
            2. Tables that do not relate to any other are not counted in the minimum requirement
        2. MongoDB
            1. If using MongoDB, the 3-5 table requirement can be achieved by using embedded documents.
            2. You may denormalize your data to keep logically-related information together in a single document
            3. Each submission must include text files `*-seed.json` to load initial data for each collection in your MongoDB (See [Attachment 3](#markdown-header-attachment-3) for an example). These files must be executed from `initialize.bat`.
        3. MySQL
            1. All tables must be normalized to at least 3NF.
            2. Define an appropriate primary key on each table
            3. Relationships must be enforced through relational integrity
            4. Each submission must include two text files (See [Attachment 3](#markdown-header-attachment-3) for an example) that are executed from `initialize.bat`:
                + A `*-schema.sql` file that will create the database and tables necessary to support your web application
                + A `*-seed.sql` file that will load the database tables with initial data
    2. Web Server
        1. Implement a web server using *node.js*.
            1. Include in your `package.json` file a script named `start` that will execute your *node.js* server and all necessary command line arguments (including, if applicable, the port on which the web server should run).  (See [Attachment 2](#markdown-header-attachment-2) for example of `start` script).
            2. Assuming all databases have been created and initialized, and all database servers are running, you should be able to start your web application by typing `npm start` from the command line from the root of your final project directory.
        2. Using the `express.js` framework, create the middleware necessary to capture GET and POST requests and handle them according to the specific routes.
        3. Your web server will initiate connection(s) with your database and make all *SQL* or *mongodb* calls necessary to support your web application. Full **C**reate, **R**etrieve, **U**pdate, and **D**elete (CRUD) functionality must exist on at least one table.
    3. Web presentation
        1. Include a minimum of 3-5 web pages.
            1. Unique routes that are handled by *Angular.js* will meet the unique web page requirement.
            2. Pages that present substantively different types of information (e.g., user account data vs article reviews) should be handled with unique *Angular.js* applications.
        2. Your web presentation should present a consistent look-and-feel throughout the application.
            1. Use CSS to provide a consistent color, font, and text scheme.
            2. Use an appropriate layout with appropriate colors supporting ease of navigation and presentation appropriate for your business case.
        3. Properly implement a user-interface, including links, forms, and controls.
            1. Use *JavaScript*, *HTML5*, *JQuery*, or *CSS* for data validation and user feedback.
            2. Use *Angular.js* and *JQuery* to implement controls and elements on your webpages.
    4. Technologies
        1. You must use the following technologies: *node.js*, *express.js*, *Angular.js*,  *MySQL*, *MongoDB*, *CSS*, *JQuery*, *JSON*, *git*.
        2. You may use the following technologies without specific approval: *Sourcetree*, *HTMLPurifier*, *XML*, *Bootstrap*, and *node.js* packages available via `npm install`.
        3. Web services that provide *JSON* or *XML* datafeeds.
        4. Use of any other technology is not permitted without prior instructor approval.
    5. User roles. Implement at least two sets of permissions for different users, with either of these approaches:
        1. Unique user access wherein each user can only interact with their own data
        2. User and admin roles wherein users have certain privileges and admins have additional privileges.
        3. Unauthenticated users and users without appropriate permissions must be prevented from navigating to certain pages.
    6. Version Control System and software development
        1. Use *git* to manage your software (see [Attachment 4](#markdown-header-attachment-4) for *git* resources, sample commands, and workflows)
        2. Use at least 8 commits to show development over time
        3. Use at least two branches for isolated or feature development
    7. Security and robustness
        1. User input must be validated on the client-side
        2. Defend against XSS attacks as demonstrated in class using the *node-esapi* package functions as demonstrated in class
        3. Defend against SQL injection using prepared statements, as demonstrated in class
        4. Perform server-side validation of at least 4 inputs
        5. Prevent access to certain pages by unauthenticated users and users without appropriate permissions
    8. Special Features.  All projects must implement at least one special feature. Functionality required elsewhere in this document is not considered a special features. Special features may include one of the following:
        1. *Bootstrap* to enhance *CSS* library and provide HTML consistency
        2. Use of *HTMLPurifier*
        3. File upload capability
        4. BLOB fields in database maintained via web interface
        5. File upload capability
        6. Mobile device interface
        7. External sensor interface
        8. Acquire data from a web services
        9. XML-based interfaced to include proper XML translation
        10. Transaction processing and intelligent rollback
        11. Nontrivial analysis of data
        12. AJAX
        13. Other special feature you propose and for which you receive approval
## Deliverables
### Proposal
Propose the functionality you wish to include in your final project

1. Give your project a title
2. List all team-members of your project
3. Provide a two-paragraph description of your proposed project. This paragraph should make reference to your special feature.
4. If desired, use bulleted lists in addition to your two paragraphs
5. State your customer/user if there is one. If there is no customer, state intended users or fictional customer
6. Submit this information in the appropriate parts of your README.md file in your *git* repository.
    1. Your *git* tag should be `proposal` and your tag message should be  `submit proposal`
    2. Your *git* commit message should be `submit proposal`.
    3. The following are the sections of the README.md file that you should update (see [Attachment 1](#markdown-header-attachment-1)):
        + Project Title
        + Team Members
        + Description
### Design Specification
State the enumareted requirements of your proposal and provide a design

1. State all requirements.
    1. Enumerated requirements. Enumerate requirements using a markdown enumeration (see [Attachment 1](#markdown-header-attachment-1)). Be as detailed as possible with your enumerated requirements
        1. Each enumerated requirement will have an identifying number
        2. Each enumerated requirement will adhere to guidelines taught in class
        3. Each enumerated requirement will use a directive term such as "will" or "shall"
    2. Elaborated requirements. Use Data Flow Diagrams, Use Case Diagrams, or paragraphs to describe in greater detail what the software must do.  At a minimum, you must provide a design of your database and a design of your web layout.
        1. Database design (*MySQL*). If using MySQL provide an ERD of your proposed database design normalized to 3NF
        2. Database design (*mongoDB*). If using mongoDB, provide a JSON schema definition as described in class (see [Attachment 5](#markdown-header-attachment-5))
        3. Website design. Provide a proposed website and user interface design using a Use Case Diagram, Web Navigation Chart, Structure Chart, Story Boards, and/or Data Flow Diagrams.
        4. External interfaces (if applicable)
2. Development Plan
    1. Propose a work breakdown for each team member
    2. Propose suggested functionality for Sprint 1 (see requirements for Sprint 1)
    3. Propose functionality for Final turn-in (should be all requirements)
3. Each team member should provide a personal reflection that states at a minimum their contribution to this phase of the project, what they learned or found challenging, and what their concerns are for the next phase of development.
4. Submit this information in the appropriate parts of your README.md and in the appropriate directories (see [Submission Requirements](#markdown-header-submission-requirements)) of your final project git repository.
    1. Your *git* tag should be `design` and your tag message shoudl be `submit design`
    2. Your *git* commit message should be `submit design `.
    3. The following are sections of the README.md file that you should update (see [Attachment 1](#markdown-header-attachment-1)):
        + Updated Proposal, as directed by instructor
        + Specification
        + Development Plan
        + Design Specification Documentation
        + Design Specification Personal Reflections
    4. The following information should appear in your `design` folder (see [Submission Requirements](#markdown-header-submission-requirements) for project directory structure)
        + Database design
            + If using *MySQL*, your design submission will consist of a MySQL Workbench `*.mwb` file and an image of the same `*.png`
            + If using *mongoDB*, your design submission will consist of a text file (`*.txt` or `*.md`) with your JSON schema (see [Attachment 5](#markdown-header-attachment-5))
        + Website design. Proposed user interface design may be submitted as a Word or Powerpoint (see instructor first if you want to use a different format)
### Tracer Bullet
Demonstrate the use of all technologies you will use in your web application

1. At a minimum, demonstrate the ability to pull information from a *MySQL* or *mongoDB* database using *express.js* routes served using *node.js* to a client page enhanced with *Angular.js*
2. Each team member should provide a personal reflection that states at a minimum their contribution to this phase of the project, what they learned or found challenging, and what their concerns are for the next phase of development.
3. Submit this information in the appropriate parts of your README.md and in the appropriate directories (see [Submission Requirements](#markdown-header-submission-requirements)) ofyour final project git repository.
    1. Your *git* tag should be `tracer` and your tag message should be `submit tracer bullet`
    2. Your *git* commit message should be `submit tracer bullet`
    3. The following are sections of the README.md file that you should update (see [Attachment 1](#markdown-header-attachment-1)):
        + Tracer Bullet Documentation
        + Tracer Bullet Personal Reflections
        + Dependencies
### Sprint 1
Implement all functionality you proposed to accomplish in your specification.

1. Sprint 1 functionality must include at least a single instance of the following:
    1. Server-side data validation
    2. Client-side data validation
    3. Database query processing
    4. Insertion into a database
    5. XSS Defense
    6. SQL injection defense
    7. If using MySQL, two related tables with enforced referential integrity
    8. If using mongoDB, at least one document with an embedded document
    9. User authentitcation
    10. A page that is inaccessible to an unauthenticated user
    11. CSS formatting
2. Use at least one branch to develop a feature. Merge this branch back to master before submission.
3. Each team member should provide a personal reflection that states at a minimum their contribution to this phase of the project, what they learned or found challenging, and what their concerns are for the next phase of development.
4. Submit this information in the appropriate parts of your README.md and in the appropriate directories (see [Submission Requirements](#markdown-header-submission-requirements)) of your final project git repository.
    1. Your *git* tag should be `sprint1` and your tag message should be `submit sprint 1`
    1. Your *git* commit message should be `submit spring 1`
    2. The following are sections of the README.md file that you should update (see [Attachment 1](#markdown-header-attachment-1)):
        + Installation Instructions
        + Functionality shortfalls (as compared to Sprint 1 functionality requirements in design specification or this document)
        + Sprint 1 Documentation
        + Sprint 1 Personal Reflections
        + Dependencies
### Final Turn-in
Implement all remaining functionality

1. Each team member should provide a personal reflection that states at a minimum their contribution to this phase of the project, what they learned or found challenging, and what feedback they would offer for the course with respect to the design project.
2. Use at least one branch to develop a feature. Merge this branch back to master before submission.
3. Submit this information in the appropriate parts of your README.md and in the appropriate directories (see [Submission Requirements](#markdown-header-submission-requirements)) of your final project git repository.
    1. Your *git* tag should be `final` and your tag message should be `submit final`
    1. Your *git* commit message should be `submit final`
    2. The following are sections of the README.md file that you should update (see [Attachment 1](#markdown-header-attachment-1)):
        + Installation Instructions
        + Functionality shortfalls (as compared to design specification or this document)
        + Final Documentation
        + Final Personal Reflections
        + Dependencies

# Attachment 1
README.md Template
```md
### Project Title: place your title here
### Team Members:
+ Team Member 1
+ Team Member 2
+ Team Member 3

### Description
Two paragraph explanation of description. These paragraphs should also make reference to a special feature you will implement in your project as well as your intended customer or user.

### Installation Instructions
List here any specific installation instructions required to initialize the web application. If you followed the submission instructions it should be as simple as:

+ Requires MongoDB/MySQL Server to be running
+ `initialize.bat` to resolve dependencies and to seed the MongoDB/MySQL server
+ `npm start` to begin the application

### Functionality Shortfalls
List here any functionality shortfalls in your program. Identify those specific areas that your submission doesn't meet the assignment requirements or your own design specification. This section will be updated for both Sprint 1 and the Final Submission

### Specification
1. List your application requirements here
    1. You can list sub-bullets by indenting with four spaces
    2. Your requirements should be as specific as possible
1. The requirements should be written from the view of the 'user', not a 'developer'
    1. For example, 'uses JQuery' is not a requirement for this discussion
    2. Rather, 'User receives a visual warning when he enters an invalid date in the date field' is an appropriate requirement. It would be appropriate to have several such requirements for each input field
    3. Other sample requirements:
        1. Malicious data from user is not submitted to the database
        2. Malicious data from user is not executed as a route
        3. Malicious data from user is not stored or represented on the HTML page
        4. On the user information page, all user information is properly presented with visual appeal

### Development Plan
#### Sprint 1:
+ Team Member 1: Description of work for Team Member 1
+ Team Member 2: Description of work for Team Member 2
+ Team Member 3: Description of work for Team Member 3
+ Requirements for Sprint 1 (listed below are examples, but they should correspond to your own requirements enumeration):
    1. Requirement 2.7
    2. Requirement 3.8
#### Final:
+ Team Member 1: Description of work for Team Member 1
+ Team Member 2: Description of work for Team Member 2
+ Team Member 3: Description of work for Team Member 3   

### Documentation:
#### Tracer Bullet Submission
 1. Tracer Bullet specification documentation statement 1
 2. Tracer Bullet specification documentation statement 2
 3. Tracer Bullet specification documentation statement 3

#### Design Specification Submission
 1. Design specification documentation statement 1
 2. Design specification documentation statement 2
 3. Design specification documentation statement 3

#### Sprint 1 Submission
 1. Sprint 1 documentation statement 1
 2. Sprint 1 documentation statement 1
 3. Spring 1 documentation statement 1

#### Final Submission
 1. Final Submission specification documentation statement 1
 2. Final Submission specification documentation statement 2
 3. Final Submission specification documentation statement 2

### Reflections:
#### Design Specification Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

#### Tracer Bullet Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

#### Sprint 1 Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

#### Final Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

### Dependencies
 + List here the 1st dependency and version number (i.e., JQuery 2.2.3)
 + List here the 2nd dependency and version number (i.e., Angular 1.4.5)
 + List here the 3rd dependency and version number (i.e., express 4.13.3
 + The version number of packages available for install via npm are listed on the [npm site](https://www.npmjs.com/)
```
The md code above will render the following:

### Project Title: place your title here
### Project Team Members:
+ Team Member 1
+ Team Member 2
+ Team Member 3

### Description
Two paragraph explanation of description. These pararaphs should also make reference to a special feature you will implement in your project as well as your intended customer or user.

### Installation Instructions
List here any specific installation instructions required to initialize the web application. If you followed the submission instructions it should be as simple as:

+ Requires MongoDB/MySQL Server to be running
+ `initialize.bat` to resolve dependencies and to seed the MongoDB/MySQL server
+ `npm start` to begin the application

### Functionality Shortfalls
List here any functionality shortfalls in your program. Identify those specific areas that your submission doesn't meet the assignment requirements or your own design specification. This section will be updated for both Sprint 1 and the Final Submission

### Specification
1. List your application requirements here
    1. You can list sub-bullets by indenting with four spaces
    2. Your requirements should be as specific as possible
1. The requirements should be written from the view of the 'user', not a 'developer'
    1. For example, 'uses JQuery' is not a requirement for this discussion
    2. Rather, 'User receives a visual warning when he enters an invalid date in the date field' is an appropriate requirement. It would be appropriate to have several such requirements for each input field
    3. Other sample requirements:
        1. Malicious data from user is not submitted to the database
        2. Malicious data from user is not executed as a route
        3. Malicious data from user is not stored or represented on the HTML page
        4. On the user information page, all user information is properly presented with visual appeal

### Development Plan
#### Sprint 1:
+ Team Member 1: Description of work for Team Member 1
+ Team Member 2: Description of work for Team Member 2
+ Team Member 3: Description of work for Team Member 3
+ Requirements for Sprint 1 (listed below are examples, but they should correspond to your own requirements enumeration):
    1. Requirement 2.7
    2. Requirement 3.8
#### Final:
+ Team Member 1: Description of work for Team Member 1
+ Team Member 2: Description of work for Team Member 2
+ Team Member 3: Description of work for Team Member 3

### Documentation:
#### Design Specification Submission
 1. Design specification documentation statement 1
 2. Design specification documentation statement 2
 3. Design specification documentation statement 3

#### Tracer Bullet Submission
 1. Tracer Bullet specification documentation statement 1
 2. Tracer Bullet specification documentation statement 2
 3. Tracer Bullet specification documentation statement 3

#### Sprint 1 Submission
 1. Sprint 1 documentation statement 1
 2. Sprint 1 documentation statement 2
 3. Sprint 1 documentation statement 3

#### Final Submission
 1. Final Submission specification documentation statement 1
 2. Final Submission specification documentation statement 2
 3. Final Submission specification documentation statement 2

### Reflections:
#### Design Specification Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

#### Tracer Bullet Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

#### Sprint 1 Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

#### Final Reflections:
+ Team Member 1:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 2:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.
+ Team Member 3:
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.

### Dependencies
 + List here the 1st dependency and version number (i.e., JQuery 2.2.3)
 + List here the 2nd dependency and version number (i.e., Angular 1.4.5)
 + List here the 3rd dependency and version number (i.e., express 4.13.3
 + The version number of packages available for install via npm are listed on the [npm site](https://www.npmjs.com/)

# Attachment 2
Project setup files
##### `.gitignore`
You should not include `node-modules` in your git repository. Also, your git repository also should not include management files created by software you are using. Here are some examples:
+ `.mwb.bak` files created by MySQL Workbench
+ `.swp` files created by vim
+ `.log` files created by `node.js` when there is an error
+ `.idea` files created by *IntelliJ* products such as *PhpStorm*
+ Your development process may require other files to be excluded as well

The following sample `.gitignore` file will exclude the files created in the examples above
```gitignore
node-modules
*.mwb.bak
*.swp
*.log
*.idea
```
##### `package.json`
You must create a `package.json` or you will not be able to install `node.js` packages. Within the `package.json` file, `name` and `version` attributes are required.

`package.json` requirements:

+ `name` is required. While you may choose the name, it must be URL-safe and contain no spaces or capital letters
+ `version` is required. While you may choose the version, it should be three numbers separated by dots
+ `start` script is not required by the `package.json` specification, but you must include it to meet this project's turn-in requirements
+ You should update the `contributors` and `repository` values or remove those keys (you'll receive warnings if you remove `repository`)
+ `license` isn't required but you will receive warnings if you remove it. You may use any apporpriate OSI-Approved license from the [SPDX License List](https://spdx.org/licenses/). [AFL-3.0](https://spdx.org/licenses/AFL-3.0.html) or [MIT](https://spdx.org/licenses/MIT.html) are reasonable choices

```json
{
  "name": "my-final-project",
  "version": "0.0.0",       
  "description": "This is our CS364 Final Project which...",
  "contributors":
    [
      {"name": "Barney Rubble",
       "email": "b.rubble@bedrock.com"},
      {"name": "Fred Flintsone",
       "email": "f.flintstone@bedrock.com"}
    ],
  "scripts": {
    "start": "node server/app.js 8181"
      },
  "repository": {
    "type": "git",
    "url": "https://bitbucket.com/...."
  },
  "license": "MIT"  
}
```

# Attachment 3
MySQL and MongoDB from the Command Line

##### MongoDB
Running *MongoDB* commands from the command line require that your path include the location of your *MongoDB* executables (likely **C:\MongoDB\bin\\**) and that the **mongod** service is running. Below are sample commands that are most likely to be used in the final project development. Refer to the full [*MongoDB* Reference](https://docs.mongodb.org/manual/reference/program/) for information on these and other commands.

###### mongoimport

**Import from **__**JSON**__

This command will import the *JSON* data from the **server/contacts.json** file into the **contacts** collection of the **users** database, dropping any existing data before import. The file is assumed to be an array of *JSON* objects.
```bash
mongoimport --db users --collection contacts --type json --file server/contacts.json --jsonArray --drop
```

**Import from **__**CSV**__

This command will import the *CSV* data from the **server/contacts.csv** file into the **contacts** collection of the **users** database, using the first line of the CSV file to determine field names.
```bash
mongoimport --db users --collection contacts --type csv --headerline --file server/contacts.csv --drop
```

*Note:* Although *CSV* is a very convenient file format, with the ability to export spreadsheets and nearly any other data structure into *CSV*, it is not possible to import documents with embedded documents from a *CSV* file. For this, you'll need to use the *JSON* format.

See the [mongoimport reference page](https://docs.mongodb.org/manual/reference/program/mongoimport/#options) for additional command line options  

---

###### mongoexport

**Export to **__**JSON**__

This command will export the data in the **products** collection of the **users** database as an array of *JSON* objects into the file **server/contacts.json**. *Note:* If you do not specify the *--jsonArray* option, you will get one file per document.
```bash
mongoexport --db users --collection products --out server/products.json --jsonArray
```

See the [mongoexport reference page](https://docs.mongodb.org/manual/reference/program/mongoexport/#options) for additional command line options

__**Note**__: Do not use **mongoexport** and **mongoimport** to make full instance production backups of your database. These commands do not preserve all BSON data types and should only be used to import/export text data. If you need more rich data transfer, use **mongodump** and **mongorestore**.

---

###### initialize.bat using mongodb
Using **mongoimport**, it is possible to import data directly from the command line into your **mongo** database. Furthermore, if you have used your web application to add data to your database, calling **mongoexport** from the command line will allow you to preserve that data in a text file in *JSON* format. You could then seed your database with this new dataset.

Here is a sample **initialize.bat** file. This file will import data from the **server/users-seed.json** file and the **server/products-seed.json** file into the **users** collection and **products** collection, respectively, of the **final-project** database, dropping each collection before importing data.

**initialize.bat**

```bash
REM install node packages
REM call allows .bat file to continue executing after npm install completes
call npm install

REM npm install turns ECHO OFF, supressing output to command line
ECHO ON

REM execute a mongoimport command for each collection (-v provides verbose output)
mongoimport -v --db final-project --collection users --type json --file server/users-seed.json --jsonArray --drop
mongoimport -v --db final-project --collection users --type json --file server/products-seed.json --jsonArray --drop
```

##### MySQL
Running *MySQL* commands from the command line require that your path include the location of your *MySQL* executables (likely **C:\XAMPP\MySQL\bin\\**) and that the **MySQL** server is running. Below are sample commands that are most likely to be used in the final project development. Refer to the full [*MySQL* Command Line Reference](http://dev.mysql.com/doc/refman/5.7/en/mysql.html) for information on these and other commands.

*Note*: These commands assume a user 'root' has access to the *MySQL* database with a blank password (this is consistent with the installation instructions you were provided at the beginning of the semester).

###### Start the *MySQL* shell
This command will start the MySQL shell under the username 'root' with no password, using the final-project database:
```bash
mysql -u root final-project
# type exit to leave the MySQL shell
```

###### Backup all *MySQL* data
This command will create a full backup of all your databases (this might be a good idea before executing commands on your server from the command line):
```bash
mysqldump -u root --add-drop-database --all-databases > alldb.sql
```


###### Restore all *MySQL* data
This command will restore all *MySQL* data from **alldb.sql**:
```bash
mysql -u root < alldb.sql
```


###### Export a database schema
This command will export the schema for the **final-project** database to the file **server\seed\mydb-schema.sql**. The schema will include commands to drop the database so that the schema can be use to entirely reset the database:
```bash
mysqldump -u root --no-data --add-drop-database --databases final-project > server\seed\mydb-schema.sql
```


###### Export a database data
This command will export the data for the **final-project** database to the file **server\seed\mydb-data.sql**:
```bash
mysqldump -u root --no-create-info final-project > server\seed\mydb-data.sql
```


###### Execute SQL commands to create database
This command will execute the SQL commands in **server\seed\mydb-schema.sql**:
```bash
mysql -u root < server\seed\mydb-schema.sql
```


###### Execute SQL commands to import data
This command will execute the SQL commands in **server\seed\mydb-data.sql** in the database **final-project**:
```bash
mysql -u root final-project < server\seed\mydb-data.sql
```

###### Import *CSV* Data
This command will import the data in a **CSV** file named **server\seed\products.csv** into the **products** table of the **final-project** database. Note that the file name must match the table name and the table columns/fields must already be created:
```bash
mysqlimport --fields-optionally-enclosed-by="\"" --fields-terminated-by=, --lines-terminated-by="\r\n" -u root final-project server\seed\products.csv
# If you have a header row, add --ignore-lines=1 to skip the first 1 line
# If the fields are enclosed in double quotes and have doubled double quotes inside the value (e.g., "Tommy said, ""Hello"""), use --fields-escaped-by=\
```


###### initialize.bat using *MySQL*
Using the commands above, it is possible to import data directly from the command line into your *MySQL* database. Furthermore, if you have used your web application or *MySQL Workbench* to add data to your database, using one of the export commands will allow you to preseve your data in the form of SQL commands. You could then seed your database with this new dataset.

Here is a sample **initialize.bat** file. This file will execute the SQL commands from the **server/final-project-schema.sql** file and the **server/final-project-data.sql**. Assuming **server/final-project-schema.sql** contains your database schema, including **DROP DATABASE** commands, and **server/final-project-data.sql** contains the data for your database, this batch file will populate your database with the requisite data.

**initialize.bat**

```bash
REM install node packages
REM call allows .bat file to continue executing after npm install completes
call npm install

REM npm install turns ECHO OFF, supressing output to command line
ECHO ON

REM only two mysql commands required one for the schema, one for the data (-v provides verbose output)
mysql -v -u root < server\seed\mydb-schema.sql
mysql -v -u root final-project < server\seed\mydb-data.sql
```

# Attachment 4
Git Commands and Workflows

Some helpful resources:
+ [The Git Parable](http://tom.preston-werner.com/2009/05/19/the-git-parable.html)
+ [Git Reference](https://git-scm.com/docs)
+

Starting a new repository
```bash
# two methods:
git init                    // start a new git repository in current location
git clone <remote repo>     // clone the remote repository to the current location
```

Basic Snapshot Commands
```bash
# see the current state of your project, specifically the changes since the last commit
git status

# add a file to the git repository
git add [filename]

# add files to the git repository
git add *                   // add all files
git add --all               // add all files
git add <list of files>     // add the list of files
git add *.txt               // add the txt files in the current directory
git add docs/*.txt          // add the txt files in docs directory
git add docs/               // add all files in the docs directory
git add "*.txt"             // add all txt files in the whole project

# commit files to the git repository (the -a flag will add all changes
# of tracked files and then commit them)
git commit -am "commit message"

# undo last commit (don't do this after you've pushed)
git reset --soft HEAD^      // put changes into staging
git reset --hard HEAD^      // undo last commit and all changes
git reset --hard HEAD^^     // undo last two commits and all changes

# change last commit (this will add any new changes and staged files, removing
# files that have been unstaged)
git commit --amend -m "New Message"

# remove files from repository (use with caution)
git rm [filename]

# show differences  
git diff                    // unstaged differences since last commit
git diff --staged           // view staged differences since last commit
git diff f294ca2            // show differences between working tree and named commit (in this case f294ca2

# show changes committed
git log
```

Basic Workflow for commit
```bash
git status
git add *
git -am "commit message"
git status
```

Repository Sharing and Updating commands
```bash
# these commands are used to synchronize your local repository with your remote repository

# if changes have been made to the remote repository since your last push, git will
# will not allow you to execute git push until after you have done a git pull, to
# prevent you from overwriting changes on the remote

# setup a remote alias
git remote add origin <remote repo>     // this is done for you when you clone

# the following command will pull from the remote repository
git pull

# the following command will push to the remote respository
git push -u origin master               // push from local master branch to remote
                                        // -u saves settings so subsequent commands
                                        // can be git push
# if you cloned or already used the -u option you can just type git push
git push                                
```


Tagging commands
```bash
# tags mark specific commits for easy reference
# this is also possible from the PhpStorm VCS -> Git -> Tag...
git commit -am "commit message"
git tag -a v1.0.0 -m "version 1.0.0"          // create a tag v1.0.0 annotated with 'version 1.0.0'
git tag -a sprint1 -m submit "submit sprint1" // create a tag sprint 1 annotated with 'submit sprint1'
git push                                      // push changes to remote
git push --tags                               // push tags to remote
```


Branch commands
```Branch
# these commands are also possible from within the IDE
git branch <branch name>                // create a branch
git checkout <branch name>              // switch to branch
git checkout -b <branch name>           // create and switch to branch in single commands

git branch -r                           // show all remote branches
git checkout <remote branch name>       // switch to remote branch
```

# Attachment 5
Use [JSONSchema.net](http://jsonschema.net/#/) to generate your JSON Schema.  Type in a sample JSON object that will correspond with a *MongoDB* document. It is easier to provide an example of a "full" document with every possible attribute set, so that you don't have to do any manual creation of the JSON schema. Correct any errors in your JSON, then set the following options:
+ Uncheck use absolute IDs
+ Check Force required
+ Check Allow additional properties
+ Check Allow additional items
+ Check Schema array (tuple typing)

Click the `Generate Schema` button.  Your JSON Schema will appear at the bottom of the page. Copy this into your design specification submission. You should remove any items from the "required" arrays that are not actually required in your *MongoDB* documents. It is acceptable to leave "required" as an empty array if no items are required.

# Attachment 6
Helpful links

+ [Random data JSON Generator][]
+ [htpasswd generator][]
+ JSON Data services
    + Google Book API: https://www.googleapis.com/books/v1/volumes?q=isbn:1285196147

  [Random data JSON Generator]:http://www.json-generator.com/ "JSON Generator"
  [htpasswd generator]:http://aspirine.org/htpasswd_en.html "Password and hash generator"
