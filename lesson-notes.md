# T31
+ [Final Project Proposal Submission][]
+ git tagging and branching

    git tags are required for every final project submission  
    git branches are required for sprint1 and final submission

+ nodemon for development

    nodemon will run in the console and restart the server automatically whenever a file changes

        npm install -g nodemon
        nodemon server/server.js


+ Views with ejs: Clone [usafa_cs364_mean repo][]
+ Angular.js review:
    + [CodeSchool.com Shaping up with Angular.js][]
    + [CodeSchool.com Staying Sharp with Angular.js (lesson 1)][]
+ Other Resources:
    + Bootstrap
    + Embedded JavaScript
    + Jade
+ Password Storage and Management: [How Not to Store Passwords][]
+ For next time:
    + Clone [usafa_cs364_auth repo][]
    + Watch [rdegges on authorization and authentication][] (1:11:49)
+ Work on proposal

  [Final Project Proposal Submission]:https://bitbucket.org/wbwatkinson/usafa_cs364_2016/src/master/final/Assignment.md?at=master "Final Project Assignment Description"
  [How Not to Store Passwords]:https://www.youtube.com/watch?v=8ZtInClXe1Q "How NOT to Store Passwords! - Computerphile on YouTube"
  [usafa_cs364_mean repo]:https://bitbucket.org/wbwatkinson/usafa_cs364_mean "BitBucket repository for MEAN Walking Skeleton"
  [usafa_cs364_auth repo]:https://bitbucket.org/wbwatkinson/usafa_cs364_auth "BitBucket repository for fork from rdegges"
  [rdegges on authorization and authentication]:https://www.youtube.com/watch?v=yvviEA1pOXw
  [CodeSchool.com Shaping up with Angular.js]:https://www.codeschool.com/courses/shaping-up-with-angular-js "Shaping up with Angular.js on CodeSchool.com"
  [CodeSchool.com Staying Sharp with Angular.js (lesson 1)]:https://www.codeschool.com/courses/staying-sharp-with-angular-js "Staying Sharp with Angular.js on CodeSchool.com"


# T32 - Session Management
+ Authentication
+ Authorization
+ Cross-Site Request Forgery
+ DB Tracer/Walking Skeleton

# T33 - Database Security Models
+ Professional development
+ Database Security Models
  + DAC, MAC, RBAC
  + Polyinstantiation
  + Inference Attack
  + Effect of Aggregation
+ JavaScript Simple Authentication Tool with Role-Based Authorization

# T34 - Cloud Computing & Big Data
+ Public, Private and Community Clouds
+ SaaS, PaaS, IaaS
+ CAP Theorem
+ For next time: Read Ch 10

# T35 - Performance, Transactions & Concurrency

# T36 -  Indexing and B-Trees

# T37 - Policy and Ethics

# T38
+ GR Topics
  + SQL vs NoSQL Applications: [Intro to NoSQL][]
    + History of NoSQL databases
    + Comparison of NoSQL data models
    + Motivations for using a NoSQL database
  + Web Application Security:
    + The role of the model, the view, and the controller in the MVC design concept
    + [SQL Injection][] and [SQL Injection prevention techniques][]: [SQL Injection Video][], [Computerphile on SQL Injection][]
    + [XSS][] and [XSS prevention techniques]: [XSS Video][], [Computerphile on XSS][]
    + [Cross-Site Request Forgery][] and [CSRF prevention techniques][]: [Computerphile on CSRF]
    + [Authentication][]
    + [Session Management][]
    + [Password Storage][]
    + Client-side vs Server-side validation
  + Cloud Computing
    + SaaS, PaaS, IaaS
    + Public, Private, and Community Clouds
  + Web technologies:
    + Node.js
    + Express.js
    + HTML
    + CSS
    + JavaScript
    + Angular.js (ng-view, ng-repeat, ng-include, ng-controller, ng-show, ng-class)
  + Requirements principles - what is required of a good requirement?
  + Database Security Models
    + Polyinstantiation
    + DAC, MAC, RBAC
    + Inference Attack
    + CAP
      + Consistency
      + Availability
      + Persistence
  + Transactions and Concurrency
    + Definition of a transaction
    + Requirements of a transaction (ACID)
    + Compromise in scalable systems (BASE)
    + The concept of serializability of Transactions
    + The problems introduced when non-serializable transactions run concurrently (lost update, uncommitted data, inconsistent retrieval)
  + The need and benefit of indexes in a database
    + When not to index
    + Be able to insert a new value into a B-Trees and show the resulting tree
    + Be able to delete a value from the leaf or an internal node from a B-Tree and show the resulting tree
  + Policy and Ethics
    + HIPPA
    + FERPA
    + COPPA
    + Privacy Act
    + Constitutional Protections for privacy

  [Intro to NoSQL]:https://www.youtube.com/watch?v=qI_g07C_Q5I "Introduction to NoSQL - Martin Fowler"
  [SQL Injection]:https://www.owasp.org/index.php/SQL_Injection "OWASP on SQL Injection"
  [SQL Injection prevention techniques]:https://www.owasp.org/index.php/SQL_Injection_Prevention_Cheat_Sheet "OWASP SQL Injection Prevention Cheat Sheet"
  [SQL Injection Video]:https://www.youtube.com/watch?v=pypTYPaU7mM "OWASP Appsec Tutorial Series: SQL Injection"
  [Computerphile on SQL Injection]:https://youtu.be/vRBihr41JTo "Computerphile on SQL Injection"
  [XSS]:https://www.owasp.org/index.php/Cross-site_Scripting_(XSS) "OWASP on Cross-site Scripting (XSS)"
  [XSS prevention techniques]:https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet: "OWASP XSS Prevention Cheat Sheet"
  [XSS Video]:https://www.youtube.com/watch?v=_Z9RQSnf8-g: "OWASP Appsec Tutorial Series: XSS"
  [Cross-Site Request Forgery]:https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF) "OWASP on CSRF"
  [Computerphile on XSS]:https://youtu.be/vRBihr41JTo "Computerphile on XSS"
  [CSRF prevention techniques]:https://www.owasp.org/index.php/CSRF_Prevention_Cheat_Sheet "OWASP CSRF Prevention Cheat Sheet"
  [Computerphile on CSRF]:https://www.youtube.com/watch?v=vRBihr41JTo "Computerphile on CSRF"
  [Authentication]:https://www.owasp.org/index.php/Authentication_Cheat_Sheet "OWASP Authentication Cheat Sheet"
  [Session Management]:https://www.owasp.org/index.php/Session_Management_Cheat_Sheet "OWASP Session Management Cheat Sheet"
  [Password Storage]:https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet "OWASP Password Storage Cheat Sheet"

# T39 - Project Demo

# T40 - Project Demo
