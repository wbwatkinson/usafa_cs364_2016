REM @echo off

cd /d "%~dp0"

REM This commit ensures the following fetch does not clobber any of the cadet's working files
git commit -a -m "Commit before daily instructor fetch"

REM This really only needs to be run once.  It connects the instructor
REM source repository to the name 'instructor'
git remote add instructor_cs364 https://wbwatkinson@bitbucket.org/wbwatkinson/usafa_cs364_2016.git

REM This retrieves changes from the instructor repository but does not yet apply them.
git fetch instructor_cs364

REM This merges changes from the instructor repository.  There could be conflicts at this
REM point if the instructor and cadet both made changes to the same file
git merge instructor_cs364/master -m "merge instructor"

REM If there was a conflict, this ought to toss out the instructor changes and keep the
REM code that the cadets have been editing.
git checkout --ours .
