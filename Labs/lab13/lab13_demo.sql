CREATE DATABASE IF NOT EXISTS cs364_lesson15;
USE cs364_lesson15;

/*  Database Systems 11e 7.4.1
    Selecting Rows with Conditional Restrictions
    Using Comparison Operators on Numeric Attributes
*/

/*  Selections with conditional restrictions
    Using various operators, the SELECT statement will limit
        the rows returned by query to just those matching the
        specified condition(s)
*/
SELECT  p_descript, p_indate, p_price, v_code
FROM    product
WHERE   v_code = 21344;

/*  Not equal to operator
    In SQL, <> is the not equal to operator */
SELECT  p_descript, p_indate, p_price, v_code
FROM    product
WHERE   v_code <> 21344;

/*  Numerical inequality */
SELECT	p_descript, p_qoh, p_min, p_price
FROM    product
WHERE   p_price <= 10;

/*  Database Systems 11e 7.4.1
    Selecting Rows with Conditional Restrictions
    Using Comparison Operators on Character Attributes
*/

/*  Text based inequality uses ASCII coding to determine the order
    of characters */
SELECT	p_code, p_descript, p_qoh, p_min, p_price
FROM    product
WHERE   p_code <= '1558-QWI';

/*  Database Systems 11e 7.4.1
    Selecting Rows with Conditional Restrictions
    Using Comparison Operators on Dates
*/

/* Inequality operators can be used on dates */
SELECT  p_descript, p_qoh, p_min, p_price, p_indate
FROM    product
WHERE   p_indate >= '2014-01-20';


/*  Database Systems 11e 7.4.1
    Selecting Rows with Conditional Restrictions
    Using Computed Columns and Column Aliases
*/

/*  Mathematical operations can be used to generate new columns
    in the return result */
SELECT  p_descript, p_qoh, p_price, p_qoh * p_price
FROM    product;

/*  Column aliasing
    You can rename a column by using the AS operator */
SELECT  p_descript, p_qoh, p_price, p_qoh * p_price AS total_value
FROM    product;

/*  Date operations
    In MySQL, using DATE_SUB and DATE_ADD will perform operations on
        dates */
SELECT  p_code, p_indate, DATE_SUB(CURDATE(), INTERVAL 90 DAY) AS cutdate
FROM    product
WHERE   p_indate <= DATE_ADD(CURDATE(), INTERVAL -90 DAY);

SELECT  p_code, p_indate, DATE_ADD(p_indate, INTERVAL 90 DAY) AS expdate
FROM    PRODUCT;

/*  Database Systems 11e 7.4.3
    Logical Operators: AND, OR, and NOT
*/

/*  Boolean OR and AND operators */
SELECT  p_code, p_indate, p_price, v_code
FROM    product
WHERE   v_code = 21344 OR v_code = 24288;

SELECT  p_descript, p_indate, p_price, v_code
FROM    product
WHERE   p_price < 50 AND p_indate > '2014-01-14';

/*  Combining boolean operations
    Use parentheses to set an operational order for the
        boolean operations */
SELECT  p_descript, p_indate, p_price, v_code
FROM    product
WHERE   (p_price < 50 AND p_indate > '2014-01-15')
OR      v_code = 24288;

/*  Boolean NOT operator */
SELECT  *
FROM    product
WHERE   NOT (v_code = 21344);

/*  Database Systems 11e 7.4.4
    Special Operators
*/

/*  SQL BETWEEN operator */
SELECT  *
FROM    product
WHERE   p_price BETWEEN 50.00 AND 100.00;

/*  SQL IS NULL operator */
SELECT  p_code, p_descript, v_code
FROM    product
WHERE   v_code IS NULL;

SELECT  p_code, p_descript, p_indate
FROM    product
WHERE   p_indate IS NULL;

/*  SQL LIKE operator */
SELECT  v_name, v_contact, v_areacode, v_phone
FROM    vendor
WHERE   v_contact LIKE 'Smith%';

/*  In MySQL, LIKE is case insensitive
    Use regular expressions is you want a case sensitive query */
SELECT  v_name, v_contact, v_areacode, v_phone
FROM    vendor
WHERE   v_contact LIKE 'SMITH%';

/*  SQL NOT LIKE operator */
SELECT  v_name, v_contact, v_areacode, v_phone
FROM    vendor
WHERE   v_contact NOT LIKE 'SMITH%';

/*  Underscore searches for a single character rather than a string of 0
    or more */
SELECT  *
FROM    employee
WHERE   emp_lname LIKE 'Johns_n';

/*  SQL IN operator
    Returns results if v_code is the set provided */
SELECT  *
FROM    product
WHERE   v_code IN (21344,24288);

/*  SQL IN operator
    This uses IN with a set of strings */
SELECT  *
FROM    employee
WHERE   emp_lname IN ('Smith','Smythe');

/*  SQL EXISTS operator
    If any results are returned in the subquery, the operation is
        true, and the query returns all vendors
    In this case, there are no products whose p_qoh <= p_min, therefore
        this expression will return an empty table */
SELECT  *
FROM    vendor
WHERE   EXISTS (SELECT * FROM product WHERE p_qoh <= p_min);

/*  SQL EXISTS operator
    In this case, all vendors will be returned because there is at least
        one product where p_qoh <= p_min * 2 */
SELECT  *
FROM    vendor
WHERE   EXISTS (SELECT * FROM product WHERE p_qoh <= p_min * 2);