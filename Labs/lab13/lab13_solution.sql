
/*  Run the lab13_ConstructCo_schema file */
USE cs364_lab13;

/*  1. Write the SQL code required to list all employees whose last names start
    with Smith.  In other words, the rows for both Smith and Smithfield should
    be included in the listing.
 */

SELECT  *
FROM    employee
WHERE   emp_lname LIKE "SMITH%";

/*  2. Write SQL code required to list all job descriptions whose job charge
    hours is less than 50.
 */

SELECT  job_description
FROM    job
WHERE   job_chg_hour < 50;

/*  3. Write SQL code to list the assigned charges by assignment number and
    employee number.  Although the assigned charges are listed in the assignment
    table, do not use this field.  Instead, calculate product of assign_chg_hr
    and assign_hours, renaming the column to assigned_charges
 */

SELECT  assign_num, emp_num, assign_chg_hr * assign_hours AS assigned_charges
FROM    assignment;

/*  4. Write SQL code to display the employee number of all employees working on
    the Evergreen or Amber Wave Project.  Use the SQL IN operator

 */

SELECT  emp_num
FROM    assignment
WHERE   proj_num = 15 OR proj_num = 18;

/*  5. Write SQL code to display all employees who have been working for at
    least 10 years and who are either Database Designers or are Systems Analysts
 */

SELECT  *
FROM    employee
WHERE   emp_years >= 10
AND     (job_code = 502 OR job_code = 501);

/*  6. Write SQL code to display all job descriptions whose charge hours are
    between 50 and 100.
 */

SELECT  job_description
FROM    job
WHERE   job_chg_hour BETWEEN 50 AND 100;

/*  7. Write SQL code to display the last and first name of all employees
    who do not have a middle name
 */

SELECT  emp_lname,emp_fname
FROM    employee
WHERE   emp_initial = '';

/*  8. Write SQL code to list all project information if there is at least one
    person assigned to Starflight
 */

SELECT  *
FROM    project
WHERE   EXISTS (SELECT * FROM assignment WHERE proj_num = 25);
