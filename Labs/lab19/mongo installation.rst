Install MongoDB Community Edition on Windows
============================================

Install MongoDB Community Edition
---------------------------------
1) Save the MongoDB installation file available at
   M:\Organizations\DF\dfcs\CS364\Software\MongoDB to your local hard drive
2) Run the installer
3) Select Custom installation and change the installation directory to
   C:\MongoDB\
4) Keep the remaining default installation options and complete the installation

Setup the MongoDB environment
-----------------------------
1) Add MongoDB to your PATH environment variable by appending the following
   path to your environment by:
   a) Press the Windows button
   b) Right click on Computer
   c) Select Properties
   d) Select Advanced system settings, when prompted enter admin username and
      password
   e) On the Advanced tab, select Environment variables
   f) In the lower window, System Variables, find Path, and click Edit
   g) Add ;C:\MongoDB\bin\ to the end of the list in the Variable value field
   h) You'll need to reopen any command windows for the path changes to take effect

Run MongoDB Community Edition
-----------------------------
1) Create this folder using the command prompt:
   md \data\db
2) Start MongoDB with the following command:
   mongod.exe
   When the Windows Security alert dialog pops up, ensure all Private Networks,
   such as Work and Home (but not Public Networks) is selected
3) You can connect to MongoDB through the mongo.exe shell by opening another
   command prompt and typing the following command:
   mongo.exe
4) Stop the MongoDB server by pressing Ctrl+C where the mongod instance is running


Create a Windows Service for MongoDB Community Edition
------------------------------------------------------
1) Open an Administrator command prompt by pressing the Win key, type cmd.exe,
   and press Ctrl + Shift + Enter to run the Command Prompt as Administrator
2) Create directories for the database and log files using the following commands:
   a) mkdir c:\data\db
   b) mkdir c:\data\log
3) Copy the mongod.cfg file to your C:\MongoDB directory
4) Create the MongoDB service with the following command:
   "C:\mongodb\bin\mongod.exe" --config "C:\mongodb\mongod.cfg" --install
6) Start the MongoDB service with the following command:
   net start MongoDB
   You can stop the service with this command (don't do this, for reference only):
   net stop MongoDB
   To remove the MongoDB service, you can use the following command (don't do this)
   "C:\mongodb\bin\mongod.exe" --remove








