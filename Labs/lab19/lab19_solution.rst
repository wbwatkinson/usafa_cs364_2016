Lab 19
======

Lab 19 Demo
---------------
1) Start mongo from the command prompt
2) Get a list of commands and information
   help
3) Create/switch to database reviews:
   use reviews
4) Verify the current database
   db
5) Create a collection and insert a document into it:
   db.potions.insert(
     {
       "name":"Invisibility",
       "vendor":"Kettlecooked"
     }
   )
6) View all documents in potions collection
   db.potions.find()
7) Find specific potion in collection
   db.potions.find({"name":"Invisibility"})
8) Add another document to the collection:
   db.potions.insert(
     {
       "name":"Shrinking",
       "vendor":"Kettlecooked"
     }
   )
9) Find all potions having a vendor of Kettlecooked
    db.potions.find({"vendor":"Kettlecooked"})
10) Remove the Invisibility potion
    db.potions.remove(
      {
        "name": "Invisibility"
      }
    )
11) Insert Invisibility potion with more information
    db.potions.insert(
      {
        "name": "Invisibility",
        "vendor": "Kettlecooked",
        "price": 10.99,
        "score": 59,
        "tryDate": new Date(2012, 8, 13),
        "ingredients": ["newt toes", 42, "laughter"],
        "ratings": {"strength": 2, "flavor": 5}
      }
    )
12) Find a potion with ingredient of laughter:
    db.potions.find({"ingredients": "laughter"})
13) Find a potion with a flavor rating of 5
    db.potions.find({"ratings.flavor":5})
14) Execute js file
    load("/Users/Warren.Watkinson/Documents/My Data/DFCS/Coursework/6 Spring 2016/
    CS364/usafa_cs364_2016_dev/Labs/lab19/lab19.js")

Lab 19 Exercises
----------------
1) Type the command below to display all documents in the wands collection
   db.wands.find().pretty()
2) Type the command below to add a new wand document with the following
   properties.  The name is "Dream Bender" and the creator is "Foxmond"
   db.wands.insert(
     {
       "name": "Dream Bender",
       "creator": "Foxmond"
     }
   )
3) Type the command below to find a wand whose name is "Storm Seeker"
   db.wands.find({"name":"Storm Seeker"}).pretty()
4) Type the command below to find the wands whose creator is "Moonsap"
   db.wands.find({"creator":"Moonsap"}).pretty()
5) Type the command below to remove the wand named "Dream Bender" from the wands
   collection.
   db.wands.remove({"name":"Dream Bender"})
6) Type the command below to add a wand to the wands collection having the
   following attributes: name is "Dream Bender", creator is "Foxmond",
   level_required is 10, price is 34.9, it has powers of "Fire" and "Love"
   (hint: use an array), and it has damage properties of "magic" of 4 and
   "melee" of 2 (hint: use an embedded document)
   db.wands.insert(
       {
         "name":"Dream Bender",
         "creator":"Foxmond",
         "level_required":10,
         "price":34.9,
         "powers":["Fire","Love"],
         "damage":{"magic":4,"melee":2}
       }
   )
7) Type the command below to find the wands having "Fire" in their list of powers.
   db.wands.find({"powers":"Fire"}).pretty()
8) Type the command below to find the wands having a magic damage of 5
   db.wands.find({"damage.magic":5}).pretty()
