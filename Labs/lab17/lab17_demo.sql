
/*  Run the lab17_SaleCo_schema file */
USE cs364_lesson21;

/* 	8.3 SQL Functions */

/*	8.3.1 Date and Time Functions */

/*	Display the product code and date the product was last
	received into stock for all products: */
SELECT	p_code, DATE_FORMAT(p_indate, '%m/%d/%y')
FROM	product;

SELECT	p_code, DATE_FORMAT(p_indate, '%M %d, %Y')
FROM	product;

/*	List all employees born in 1974: */
SELECT	emp_lname, emp_fname, emp_dob, YEAR(emp_dob) AS year
FROM	employee
WHERE	YEAR(emp_dob) = 1974;

/*	List all employees born in November: */
SELECT	emp_lname, emp_fname, emp_dob, MONTH(emp_dob) AS month
FROM	employee
WHERE	MONTH(emp_dob) = 11;

/* 	List all employees born on the 14th day of the month: */
SELECT	emp_lname, emp_fname, emp_dob, DAY(emp_dob) AS day
FROM	employee
WHERE	DAY(emp_dob) = 14;

/*	List all products with the date they will have been on the shelf for 30 days: */
/*	ADDDATE(date_value, n) Adds a number of days to a date */
SELECT	p_code, p_indate, ADDDATE(p_indate, 30) AS '30 day shelf-life'
FROM	product
ORDER BY ADDDATE(p_indate, 30);

/* 	List all products with their expiration date (two years from the date received into inventory: */
/*	DATE_ADD(date, INTERVAL n unit) Adds the number of days, months, or years to a date */
SELECT	p_code, p_indate, DATE_ADD(p_indate, INTERVAL 2 YEAR) AS 'Expiration Date'
FROM	product
ORDER BY DATE_ADD(p_indate, INTERVAL 2 YEAR);

/*	List all employees who were hired within the last seven days of the month: */
/*	LAST_DAY(date_value) returns the last day of the month of the given date */
SELECT	emp_lname, emp_fname, emp_hire_date
FROM	employee
WHERE	emp_hire_date >= DATE_ADD(LAST_DAY(emp_hire_date), INTERVAL -7 DAY);

/*	8.3.2 Numeric Functions */
SELECT	1.95, -1.93, ABS(1.95), ABS(-1.93);

/*	List the product prices rounded to one and zero decimal places: */
SELECT	p_code, p_price, ROUND(p_price,1) AS price1, ROUND(p_price,0) AS price0
FROM	product;

/*	List the product price, the smallest integer greater than or equal to the product
	price, and the largest integer equal to or lss than the product price: */
SELECT	p_price, CEIL(p_price), FLOOR(p_price)
FROM	product;

/* Alternatively */
SELECT	p_price, CEILING(p_price), FLOOR(p_price)
FROM	product;

/*	8.3.3 String Functions */
/*	List all employee names: */
SELECT	CONCAT(CONCAT(emp_lname, ', '), emp_fname) AS name
FROM 	employee;

/*	List all employe nams in all capital letters: */
SELECT	UPPER(CONCAT(CONCAT(emp_lname, ', '), emp_fname)) AS nam
FROM	employee;

/*	List the first three characters of all employee phone numbers */
SELECT	emp_phone, SUBSTRING(emp_phone,1,3) AS prefix
FROM	employee;

/*	List all employee last names and the length of their names in 
	descending order by last name length: */
SELECT	emp_lname, LENGTH(emp_lname) AS namesize
FROM	employee
ORDER BY namesize DESC;

/*	8.3.4 Conversion functions */
/*	List all product prices, product received date, and percent discount using
	formatted values */
SELECT	p_code, 
		p_price, CAST(p_price AS CHAR(8)) AS price,
		p_indate, CAST(p_indate AS CHAR(20)) as indate,
		p_discount, CAST(p_discount AS CHAR(4)) AS disc
FROM	product;

SELECT	p_code, 
		p_price, CONVERT(p_price, CHAR(8)) AS price,
		p_indate, CONVERT(p_indate, CHAR(20)) as indate,
		p_discount, CONVERT(p_discount, CHAR(4)) AS disc
FROM	product;

SELECT	v_code, v_state, CASE 
							WHEN v_state = 'CA' THEN 0.8
                            WHEN v_state = 'FL' THEN 0.5
							WHEN v_state = 'TN' THEN 0.85
							ELSE 0.00
						 END AS tax
FROM	vendor;

/*	8.4 Relational Set Operators */
/*	8.4.1 Union */
/*	Note that duplicate rows between the two customer tables are excluded */
SELECT	cus_lname, cus_fname, cus_initial, cus_areacode, cus_phone
FROM	customer
UNION
SELECT	cus_lname, cus_fname, cus_initial, cus_areacode, cus_phone
FROM	customer_2;

/*	8.4.2 Union All */
/*	Note that duplicate rows between the two customer tables are included */
SELECT	cus_lname, cus_fname, cus_initial, cus_areacode, cus_phone
FROM	customer
UNION ALL
SELECT	cus_lname, cus_fname, cus_initial, cus_areacode, cus_phone
FROM	customer_2;

/*	8.4.3 Intersect */
/*  MySQL dos not support Intersect directly, but similar functionality can be achieved with IN */

/*	These two queries identify all area codes having both a customr and a vendor */
SELECT	DISTINCT cus_areacode FROM customer
WHERE	cus_areacode IN (SELECT v_areacode FROM vendor);
        
/*	Also a join clause: */
SELECT	DISTINCT cus_areacode
FROM	customer JOIN vendor ON customer.cus_areacode = vendor.v_areacode;

/*	8.4.4 Except */
/*	MySQL does not support Minus directly, but similar functionality can be achievd with NOT IN */
SELECT	cus_code
FROM	customer
WHERE	cus_areacode = '615' AND
		cus_code NOT IN (SELECT DISTINCT cus_code FROM invoice);
        
        
/*	8.5 Virtual tables: Creating a view */

DROP VIEW IF EXISTS priceGT50;
CREATE VIEW priceGT50 AS
SELECT	p_descript, p_qoh, p_price
FROM	product
WHERE	p_price < 50.00;

SELECT * FROM priceGT50;

DROP VIEW IF EXISTS prod_stats;
CREATE VIEW prod_stats AS
SELECT	v_code, SUM(p_qoh*p_price) AS totcost, MAX(p_qoh) AS maxQty, MIN(p_qoh) AS minQty, AVG(p_qoh) AS avgQty
FROM	product
GROUP BY v_code;

SELECT * FROM prod_stats;

/*	8.5.1 Updateable Views */
START TRANSACTION;
#UPDATE	prodmaster, prodsales
#SET		prodmaster.prod_qoh = prod_qoh - ps_qty
#WHERE	prodmaster.prod_id = prodsales.prod_id;

DROP VIEW IF EXISTS psvupd;
CREATE VIEW psvupd AS 
SELECT	prodmaster.prod_id, prod_qoh, ps_qty
FROM	prodmaster, prodsales
WHERE	prodmaster.prod_id = prodsales.prod_id;

SELECT * FROM psvupd;

SELECT * FROM prodmaster;

SELECT * FROM prodsales;

UPDATE	psvupd
SET		prod_qoh = prod_qoh - ps_qty;

SELECT * FROM prodmaster;

ROLLBACK;