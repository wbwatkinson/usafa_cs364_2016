/*  Run the lab17_LargeCo_schema file */
USE cs364_lab17;

/*  1. Write a query to include all employees who were hired in 1978. A correct
    solution will return 13 records. */


/*  2. Write a query to list all customers living in Alabama. A correct solution
    will return 50 records. */


/*  3. Write a query to list all customers living in Alabama and the total
    amount of purchases they've ever made (customers who have never purchased
    anything should have 0.00 instead of NULL). Display the customer's first
    and last name and the total of all purchases. Sort the list by the total
    amount spent in decreasing order. Hint: There is one customer that has
    made no purchases. An inner join will exclude this individual. You'll need
    to use a left join. However, using a left join will result in that
    individual showing up in the result set, his total will be NULL. You'll need
    to use a CASE clause to convert NULL to 0.00. A correct solution will return
    50 rows. */


/*  4. Save the query in 3 above to a view */


/*  5. LargeCo is planning a new promotion in Alabama (AL) and wants to know
    about the largest purchases made by customers in that state. Write a query
    to display the customer code, customer first name, last name, full address,
    invoice date, and invoice total of the largest purchase made by each
    customer in Alabama. Be certain to include any customers in Alabama who have
    never made a purchase (their invoice dates should be NULL and the invoice
    totals should display as 0).  Hint: A left join won't help here, since you'll
    be doing a correlated query, and the inner query will never find a record
    for a customer who hasn't made a purchase. First, write a query that will return
    the largest invoice for each customer in Alabama.  Next, UNION that query
    with a query that will return all customers who have never made a purchase.
    This second query will require you manually add NULL and 0 as the column fields,
    since a UNION requires both queries to have the same number of rows.
 */