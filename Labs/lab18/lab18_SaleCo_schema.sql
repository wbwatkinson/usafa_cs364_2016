/* Database Systems, Coronel/Morris */
/* Type of SQL : SQL Server */


DROP DATABASE IF EXISTS cs364_lesson22;
CREATE DATABASE IF NOT EXISTS cs364_lesson22;
USE cs364_lesson22;

CREATE TABLE customer (
  cus_code     INT,
  cus_lname    VARCHAR(15),
  cus_fname    VARCHAR(15),
  cus_initial  VARCHAR(1),
  cus_areacode VARCHAR(3),
  cus_phone    VARCHAR(8),
  cus_balance  FLOAT(8)
);
INSERT INTO customer
VALUES ('10010', 'Ramas', 'Alfred', 'A', '615', '844-2573', '0');
INSERT INTO customer
VALUES ('10011', 'Dunne', 'Leona', 'K', '713', '894-1238', '0');
INSERT INTO customer
VALUES ('10012', 'Smith', 'Kathy', 'W', '615', '894-2285', '345.86');
INSERT INTO customer
VALUES ('10013', 'Olowski', 'Paul', 'F', '615', '894-2180', '536.75');
INSERT INTO customer
VALUES ('10014', 'Orlando', 'Myron', '', '615', '222-1672', '0');
INSERT INTO customer
VALUES ('10015', 'O''Brian', 'Amy', 'B', '713', '442-3381', '0');
INSERT INTO customer
VALUES ('10016', 'Brown', 'James', 'G', '615', '297-1228', '221.19');
INSERT INTO customer
VALUES ('10017', 'Williams', 'George', '', '615', '290-2556', '768.93');
INSERT INTO customer
VALUES ('10018', 'Farriss', 'Anne', 'G', '713', '382-7185', '216.55');
INSERT INTO customer
VALUES ('10019', 'Smith', 'Olette', 'K', '615', '297-3809', '0');

CREATE TABLE customer_2 (
  cus_code     INT,
  cus_lname    VARCHAR(15) NOT NULL,
  cus_fname    VARCHAR(15) NOT NULL,
  cus_initial  CHAR(1),
  cus_areacode CHAR(3),
  cus_phone    CHAR(8)
);

INSERT INTO customer_2
VALUES (345, 'Terrell', 'Justine', 'H', '615', '322-9870');
INSERT INTO customer_2 VALUES (347, 'Olowski', 'Paul', 'F', '615', '894-2180');
INSERT INTO customer_2
VALUES (351, 'Hernandez', 'Carlos', 'J', '723', '123-7654');
INSERT INTO customer_2
VALUES (352, 'McDowell', 'George', NULL, '723', '123-7768');
INSERT INTO customer_2
VALUES (365, 'Tirpin', 'Khaleed', 'G', '723', '123-9876');
INSERT INTO customer_2 VALUES (368, 'Lewis', 'Marie', 'J', '734', '332-1789');
INSERT INTO customer_2 VALUES (369, 'Dunne', 'Leona', 'K', '713', '894-1238');


/* -- */


CREATE TABLE emp (
  emp_num       INT,
  emp_title     VARCHAR(4),
  emp_lname     VARCHAR(15),
  emp_fname     VARCHAR(15),
  emp_initial   VARCHAR(1),
  emp_dob       DATETIME,
  emp_hire_date DATETIME,
  emp_areacode  VARCHAR(3),
  emp_phone     VARCHAR(8),
  emp_mgr       INT
);
INSERT INTO emp VALUES
  ('100', 'Mr.', 'Kolmycz', 'George', 'D', Str_To_Date('6/15/1942', '%m/%d/%Y'), Str_To_Date('3/15/1985', '%m/%d/%Y'), '615',
   '324-5456', '');
INSERT INTO emp VALUES
  ('101', 'Ms.', 'Lewis', 'Rhonda', 'G', Str_To_Date('3/19/1965', '%m/%d/%Y'), Str_To_Date('4/25/1986', '%m/%d/%Y'), '615',
   '324-4472', '100');
INSERT INTO emp VALUES
  ('102', 'Mr.', 'VanDam', 'Rhett', '', Str_To_Date('11/14/1958', '%m/%d/%Y'), Str_To_Date('12/20/1990', '%m/%d/%Y'), '901',
   '675-8993', '100');
INSERT INTO emp VALUES
  ('103', 'Ms.', 'Jones', 'Anne', 'M', Str_To_Date('10/16/1974', '%m/%d/%Y'), Str_To_Date('8/28/1994', '%m/%d/%Y'), '615',
   '898-3456', '100');
INSERT INTO emp VALUES
  ('104', 'Mr.', 'Lange', 'John', 'P', Str_To_Date('11/8/1971', '%m/%d/%Y'), Str_To_Date('10/20/1994', '%m/%d/%Y'), '901',
   '504-4430', '105');
INSERT INTO emp VALUES
  ('105', 'Mr.', 'Williams', 'Robert', 'D', Str_To_Date('3/14/1975', '%m/%d/%Y'), Str_To_Date('11/8/1998', '%m/%d/%Y'), '615',
   '890-3220', '');
INSERT INTO emp VALUES
  ('106', 'Mrs.', 'Smith', 'Jeanine', 'K', Str_To_Date('2/12/1968', '%m/%d/%Y'), Str_To_Date('1/5/1989', '%m/%d/%Y'), '615',
   '324-7883', '105');
INSERT INTO emp VALUES
  ('107', 'Mr.', 'Diante', 'Jorge', 'D', Str_To_Date('8/21/1974', '%m/%d/%Y'), Str_To_Date('7/2/1994', '%m/%d/%Y'), '615',
   '890-4567', '105');
INSERT INTO emp VALUES
  ('108', 'Mr.', 'Wiesenbach', 'Paul', 'R', Str_To_Date('2/14/1966', '%m/%d/%Y'), Str_To_Date('11/18/1992', '%m/%d/%Y'), '615',
   '897-4358', '');
INSERT INTO emp VALUES
  ('109', 'Mr.', 'Smith', 'George', 'K', Str_To_Date('6/18/1961', '%m/%d/%Y'), Str_To_Date('4/14/1989', '%m/%d/%Y'), '901',
   '504-3339', '108');
INSERT INTO emp VALUES
  ('110', 'Mrs.', 'Genkazi', 'Leighla', 'W', Str_To_Date('5/19/1970', '%m/%d/%Y'), Str_To_Date('12/1/1990', '%m/%d/%Y'), '901',
   '569-0093', '108');
INSERT INTO emp VALUES
  ('111', 'Mr.', 'Washington', 'Rupert', 'E', Str_To_Date('1/3/1966', '%m/%d/%Y'), Str_To_Date('6/21/1993', '%m/%d/%Y'), '615',
   '890-4925', '105');
INSERT INTO emp VALUES
  ('112', 'Mr.', 'Johnson', 'Edward', 'E', Str_To_Date('5/14/1961', '%m/%d/%Y'), Str_To_Date('12/1/1983', '%m/%d/%Y'), '615',
   '898-4387', '100');
INSERT INTO emp VALUES
  ('113', 'Ms.', 'Smythe', 'Melanie', 'P', Str_To_Date('9/15/1970', '%m/%d/%Y'), Str_To_Date('5/11/1999', '%m/%d/%Y'), '615',
   '324-9006', '105');
INSERT INTO emp VALUES
  ('114', 'Ms.', 'Brandon', 'Marie', 'G', Str_To_Date('11/2/1956', '%m/%d/%Y'), Str_To_Date('11/15/1979', '%m/%d/%Y'), '901',
   '882-0845', '108');
INSERT INTO emp VALUES
  ('115', 'Mrs.', 'Saranda', 'Hermine', 'R', Str_To_Date('7/25/1972', '%m/%d/%Y'), Str_To_Date('4/23/1993', '%m/%d/%Y'), '615',
   '324-5505', '105');
INSERT INTO emp VALUES
  ('116', 'Mr.', 'Smith', 'George', 'A', Str_To_Date('11/8/1965', '%m/%d/%Y'), Str_To_Date('12/10/1988', '%m/%d/%Y'), '615',
   '890-2984', '108');

/* -- */

CREATE TABLE employee (
  emp_num       INT,
  emp_title     VARCHAR(4),
  emp_lname     VARCHAR(15),
  emp_fname     VARCHAR(15),
  emp_initial   VARCHAR(1),
  emp_dob       DATETIME,
  emp_hire_date DATETIME,
  emp_areacode  VARCHAR(3),
  emp_phone     VARCHAR(8)
);
INSERT INTO employee VALUES
  ('100', 'Mr.', 'Kolmycz', 'George', 'D', Str_To_Date('6/15/1942', '%m/%d/%Y'), Str_To_Date('3/15/1985', '%m/%d/%Y'), '615',
   '324-5456');
INSERT INTO employee VALUES
  ('101', 'Ms.', 'Lewis', 'Rhonda', 'G', Str_To_Date('3/19/1965', '%m/%d/%Y'), Str_To_Date('4/25/1986', '%m/%d/%Y'), '615',
   '324-4472');
INSERT INTO employee VALUES
  ('102', 'Mr.', 'VanDam', 'Rhett', '', Str_To_Date('11/14/1958', '%m/%d/%Y'), Str_To_Date('12/20/1990', '%m/%d/%Y'), '901',
   '675-8993');
INSERT INTO employee VALUES
  ('103', 'Ms.', 'Jones', 'Anne', 'M', Str_To_Date('10/16/1974', '%m/%d/%Y'), Str_To_Date('8/28/1994', '%m/%d/%Y'), '615',
   '898-3456');
INSERT INTO employee VALUES
  ('104', 'Mr.', 'Lange', 'John', 'P', Str_To_Date('11/8/1971', '%m/%d/%Y'), Str_To_Date('10/20/1994', '%m/%d/%Y'), '901',
   '504-4430');
INSERT INTO employee VALUES
  ('105', 'Mr.', 'Williams', 'Robert', 'D', Str_To_Date('3/14/1975', '%m/%d/%Y'), Str_To_Date('11/8/1998', '%m/%d/%Y'), '615',
   '890-3220');
INSERT INTO employee VALUES
  ('106', 'Mrs.', 'Smith', 'Jeanine', 'K', Str_To_Date('2/12/1968', '%m/%d/%Y'), Str_To_Date('1/5/1989', '%m/%d/%Y'), '615',
   '324-7883');
INSERT INTO employee VALUES
  ('107', 'Mr.', 'Diante', 'Jorge', 'D', Str_To_Date('8/21/1974', '%m/%d/%Y'), Str_To_Date('7/2/1994', '%m/%d/%Y'), '615',
   '890-4567');
INSERT INTO employee VALUES
  ('108', 'Mr.', 'Wiesenbach', 'Paul', 'R', Str_To_Date('2/14/1966', '%m/%d/%Y'), Str_To_Date('11/18/1992', '%m/%d/%Y'), '615',
   '897-4358');
INSERT INTO employee VALUES
  ('109', 'Mr.', 'Smith', 'George', 'K', Str_To_Date('6/18/1961', '%m/%d/%Y'), Str_To_Date('4/14/1989', '%m/%d/%Y'), '901',
   '504-3339');
INSERT INTO employee VALUES
  ('110', 'Mrs.', 'Genkazi', 'Leighla', 'W', Str_To_Date('5/19/1970', '%m/%d/%Y'), Str_To_Date('12/1/1990', '%m/%d/%Y'), '901',
   '569-0093');
INSERT INTO employee VALUES
  ('111', 'Mr.', 'Washington', 'Rupert', 'E', Str_To_Date('1/3/1966', '%m/%d/%Y'), Str_To_Date('6/21/1993', '%m/%d/%Y'), '615',
   '890-4925');
INSERT INTO employee VALUES
  ('112', 'Mr.', 'Johnson', 'Edward', 'E', Str_To_Date('5/14/1961', '%m/%d/%Y'), Str_To_Date('12/1/1983', '%m/%d/%Y'), '615',
   '898-4387');
INSERT INTO employee VALUES
  ('113', 'Ms.', 'Smythe', 'Melanie', 'P', Str_To_Date('9/15/1970', '%m/%d/%Y'), Str_To_Date('5/11/1999', '%m/%d/%Y'), '615',
   '324-9006');
INSERT INTO employee VALUES
  ('114', 'Ms.', 'Brandon', 'Marie', 'G', Str_To_Date('11/2/1956', '%m/%d/%Y'), Str_To_Date('11/15/1979', '%m/%d/%Y'), '901',
   '882-0845');
INSERT INTO employee VALUES
  ('115', 'Mrs.', 'Saranda', 'Hermine', 'R', Str_To_Date('7/25/1972', '%m/%d/%Y'), Str_To_Date('4/23/1993', '%m/%d/%Y'), '615',
   '324-5505');
INSERT INTO employee VALUES
  ('116', 'Mr.', 'Smith', 'George', 'A', Str_To_Date('11/8/1965', '%m/%d/%Y'), Str_To_Date('12/10/1988', '%m/%d/%Y'), '615',
   '890-2984');

/* -- */

CREATE TABLE invoice (
  inv_number INT,
  cus_code   INT,
  inv_date   DATETIME
);
INSERT INTO invoice VALUES ('1001', '10014', Str_To_Date('1/16/2014', '%m/%d/%Y'));
INSERT INTO invoice VALUES ('1002', '10011', Str_To_Date('1/16/2014', '%m/%d/%Y'));
INSERT INTO invoice VALUES ('1003', '10012', Str_To_Date('1/16/2014', '%m/%d/%Y'));
INSERT INTO invoice VALUES ('1004', '10011', Str_To_Date('1/17/2014', '%m/%d/%Y'));
INSERT INTO invoice VALUES ('1005', '10018', Str_To_Date('1/17/2014', '%m/%d/%Y'));
INSERT INTO invoice VALUES ('1006', '10014', Str_To_Date('1/17/2014', '%m/%d/%Y'));
INSERT INTO invoice VALUES ('1007', '10015', Str_To_Date('1/17/2014', '%m/%d/%Y'));
INSERT INTO invoice VALUES ('1008', '10011', Str_To_Date('1/17/2014', '%m/%d/%Y'));

/* -- */

CREATE TABLE line (
  inv_number  INT,
  line_number INT,
  p_code      VARCHAR(10),
  line_units  FLOAT(8),
  line_price  FLOAT(8)
);
INSERT INTO line VALUES ('1001', '1', '13-Q2/P2', '1', '14.99');
INSERT INTO line VALUES ('1001', '2', '23109-HB', '1', '9.95');
INSERT INTO line VALUES ('1002', '1', '54778-2T', '2', '4.99');
INSERT INTO line VALUES ('1003', '1', '2238/QPD', '1', '38.95');
INSERT INTO line VALUES ('1003', '2', '1546-QQ2', '1', '39.95');
INSERT INTO line VALUES ('1003', '3', '13-Q2/P2', '5', '14.99');
INSERT INTO line VALUES ('1004', '1', '54778-2T', '3', '4.99');
INSERT INTO line VALUES ('1004', '2', '23109-HB', '2', '9.95');
INSERT INTO line VALUES ('1005', '1', 'PVC23DRT', '12', '5.87');
INSERT INTO line VALUES ('1006', '1', 'SM-18277', '3', '6.99');
INSERT INTO line VALUES ('1006', '2', '2232/QTY', '1', '109.92');
INSERT INTO line VALUES ('1006', '3', '23109-HB', '1', '9.95');
INSERT INTO line VALUES ('1006', '4', '89-WRE-Q', '1', '256.99');
INSERT INTO line VALUES ('1007', '1', '13-Q2/P2', '2', '14.99');
INSERT INTO line VALUES ('1007', '2', '54778-2T', '1', '4.99');
INSERT INTO line VALUES ('1008', '1', 'PVC23DRT', '5', '5.87');
INSERT INTO line VALUES ('1008', '2', 'WR3/TT3', '3', '119.95');
INSERT INTO line VALUES ('1008', '3', '23109-HB', '1', '9.95');

/* -- */

CREATE TABLE product (
  p_code     VARCHAR(10),
  p_descript VARCHAR(35),
  p_indate   DATETIME,
  p_qoh      INT,
  p_min      INT,
  p_price    FLOAT(8),
  p_discount FLOAT(8),
  v_code     INT
);
INSERT INTO product VALUES
  ('11QER/31', 'Power painter, 15 psi., 3-nozzle', Str_To_Date('11/3/2013', '%m/%d/%Y'), '8', '5',
   '109.99', '0', '25595');
INSERT INTO product VALUES
  ('13-Q2/P2', '7.25-in. pwr. saw blade', Str_To_Date('12/13/2013', '%m/%d/%Y'), '32', '15', '14.99',
   '0.05', '21344');
INSERT INTO product VALUES
  ('14-Q1/L3', '9.00-in. pwr. saw blade', Str_To_Date('11/13/2013', '%m/%d/%Y'), '18', '12', '17.49',
   '0', '21344');
INSERT INTO product VALUES
  ('1546-QQ2', 'Hrd. cloth, 1/4-in., 2x50', Str_To_Date('1/15/2014', '%m/%d/%Y'), '15', '8', '39.95',
   '0', '23119');
INSERT INTO product VALUES
  ('1558-QW1', 'Hrd. cloth, 1/2-in., 3x50', Str_To_Date('1/15/2014', '%m/%d/%Y'), '23', '5', '43.99',
   '0', '23119');
INSERT INTO product VALUES
  ('2232/QTY', 'B\&D jigsaw, 12-in. blade', Str_To_Date('12/30/2013', '%m/%d/%Y'), '8', '5', '109.92',
   '0.05', '24288');
INSERT INTO product VALUES
  ('2232/QWE', 'B\&D jigsaw, 8-in. blade', Str_To_Date('12/24/2013', '%m/%d/%Y'), '6', '5', '99.87',
   '0.05', '24288');
INSERT INTO product VALUES
  ('2238/QPD', 'B\&D cordless drill, 1/2-in.', Str_To_Date('1/20/2014', '%m/%d/%Y'), '12', '5', '38.95',
   '0.05', '25595');
INSERT INTO product VALUES
  ('23109-HB', 'Claw hammer', Str_To_Date('1/20/2014', '%m/%d/%Y'), '23', '10', '9.95', '0.1', '21225');
INSERT INTO product VALUES
  ('23114-AA', 'Sledge hammer, 12 lb.', Str_To_Date('1/2/2014', '%m/%d/%Y'), '8', '5', '14.40', '0.05',
   '');
INSERT INTO product VALUES
  ('54778-2T', 'Rat-tail file, 1/8-in. fine', Str_To_Date('12/15/2013', '%m/%d/%Y'), '43', '20', '4.99',
   '0', '21344');
INSERT INTO product VALUES
  ('89-WRE-Q', 'Hicut chain saw, 16 in.', Str_To_Date('2/7/2014', '%m/%d/%Y'), '11', '5', '256.99',
   '0.05', '24288');
INSERT INTO product VALUES
  ('PVC23DRT', 'PVC pipe, 3.5-in., 8-ft', Str_To_Date('2/20/2014', '%m/%d/%Y'), '188', '75', '5.87', '0',
   '');
INSERT INTO product VALUES
  ('SM-18277', '1.25-in. metal screw, 25', Str_To_Date('3/1/2014', '%m/%d/%Y'), '172', '75', '6.99', '0',
   '21225');
INSERT INTO product VALUES
  ('SW-23116', '2.5-in. wd. screw, 50', Str_To_Date('2/24/2014', '%m/%d/%Y'), '237', '100', '8.45', '0',
   '21231');
INSERT INTO product VALUES
  ('WR3/TT3', 'Steel matting, 4''x8''x1/6", .5" mesh', Str_To_Date('1/17/2014', '%m/%d/%Y'), '18', '5',
   '119.95', '0.1', '25595');

/* -- */

CREATE TABLE vendor (
  v_code     INT,
  v_name     VARCHAR(15),
  v_contact  VARCHAR(50),
  v_areacode VARCHAR(3),
  v_phone    VARCHAR(8),
  v_state    VARCHAR(2),
  v_order    VARCHAR(1)
);
INSERT INTO vendor
VALUES ('21225', 'Bryson, Inc.', 'Smithson', '615', '223-3234', 'TN', 'Y');
INSERT INTO vendor
VALUES ('21226', 'SuperLoo, Inc.', 'Flushing', '904', '215-8995', 'FL', 'N');
INSERT INTO vendor
VALUES ('21231', 'D&E Supply', 'Singh', '615', '228-3245', 'TN', 'Y');
INSERT INTO vendor
VALUES ('21344', 'Gomez Bros.', 'Ortega', '615', '889-2546', 'KY', 'N');
INSERT INTO vendor
VALUES ('22567', 'Dome Supply', 'Smith', '901', '678-1419', 'GA', 'N');
INSERT INTO vendor
VALUES ('23119', 'Randsets Ltd.', 'Anderson', '901', '678-3998', 'GA', 'Y');
INSERT INTO vendor
VALUES ('24004', 'Brackman Bros.', 'Browning', '615', '228-1410', 'TN', 'N');
INSERT INTO vendor
VALUES ('24288', 'ORDVA, Inc.', 'Hakford', '615', '898-1234', 'TN', 'Y');
INSERT INTO vendor
VALUES ('25443', 'B&K, Inc.', 'Smith', '904', '227-0093', 'FL', 'N');
INSERT INTO vendor
VALUES ('25501', 'Damal Supplies', 'Smythe', '615', '890-3529', 'TN', 'N');
INSERT INTO vendor
VALUES ('25595', 'Rubicon Systems', 'Orton', '904', '456-0092', 'FL', 'Y');

CREATE TABLE prodmaster (
prod_id varchar(50),
prod_desc varchar(50),
prod_qoh int
);
INSERT INTO prodmaster VALUES('A123','SCREWS','67');
INSERT INTO prodmaster VALUES('BX34','NUTS','37');
INSERT INTO prodmaster VALUES('C583','BOLTS','50');

/* -- */


CREATE TABLE prodsales (
prod_id varchar(50),
ps_qty int
);
INSERT INTO prodsales VALUES('A123','7');
INSERT INTO prodsales VALUES('BX34','3');
