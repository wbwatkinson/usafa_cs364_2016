/*  Run the lab18_schema file */
USE cs364_lab18;

/*	1. Create a trigger named trg_line_total to write the LINE_TOTAL value in
    the LINE table every time you add a new LINE row. (The LINE_TOTAL value is
    the product of the LINE_UNITS and the LINE_PRICE values.)
 */


/*  2. Create a trigger named trg_line_prod that will automatically update the
    product quantity on hand for each product sold after a new LINE row is added.
 */


/*  3. Create a stored procedure named prc_inv_amounts to update the
    INV_SUBTOTAL, INV_TAX, and INV_TOTAL. The procedure takes the invoice number
    as a parameter. The INV_SUBTOTAL is the sum of the LINE_TOTAL amounts for
    the invoice, the INV_TAX is the product of the INV_SUBTOTAL and the tax rate
     (8%), and the INV_TOTAL is the sum of the INV_SUBTOTAL and the INV_TAX.
 */


/*  4. Create a procedure named prc_cus_balance_update that will take the
    invoice number as a parameter and update the customer balance.
 */