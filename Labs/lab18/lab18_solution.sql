/*  Run the lab18_schema file */
USE cs364_lab18;

/*	1. Create a trigger named trg_line_total to write the LINE_TOTAL value in
    the LINE table every time you add a new LINE row. (The LINE_TOTAL value is
    the product of the LINE_UNITS and the LINE_PRICE values.)
 */

ALTER TABLE line
  ADD COLUMN line_total NUMERIC(8,2);

DROP TRIGGER IF EXISTS trg_line_total;
DELIMITER $$
CREATE TRIGGER trg_line_total
BEFORE INSERT ON line
FOR EACH ROW
  BEGIN
    SET NEW.line_total = NEW.line_units * NEW.line_price;
  END; $$
DELIMITER ;

-- Demonstrate that it works, adding a line to invoice 1001
START TRANSACTION;
SELECT * FROM line;
INSERT INTO line VALUES('1001', '3', '1546-QQ2', '2', '39.95', 0);
SELECT * FROM line;
ROLLBACK;
SELECT * FROM line;

/*  2. Create a trigger named trg_line_prod that will automatically update the
    product quantity on hand for each product sold after a new LINE row is added.
 */
-- Cannot add another BEFORE INSERT ON line trigger in this version of MySQL
DROP TRIGGER IF EXISTS trg_line_prod;
DELIMITER $$
CREATE TRIGGER trg_line_prod
AFTER INSERT ON line
FOR EACH ROW
  BEGIN
    UPDATE  product
    SET     product.p_qoh = product.p_qoh - NEW.line_units
    WHERE   product.p_code = NEW.p_code;
  END; $$
DELIMITER ;

-- Demonstrate that it works, using product 1546-QQ2
START TRANSACTION;
SELECT * FROM product;
INSERT INTO line VALUES('1001', '3', '1546-QQ2', '2', '39.95', 0);
SELECT * FROM product;
ROLLBACK;
SELECT * FROM product;

/*  3. Create a stored procedure named prc_inv_amounts to update the
    INV_SUBTOTAL, INV_TAX, and INV_TOTAL. The procedure takes the invoice number
    as a parameter. The INV_SUBTOTAL is the sum of the LINE_TOTAL amounts for
    the invoice, the INV_TAX is the product of the INV_SUBTOTAL and the tax rate
     (8%), and the INV_TOTAL is the sum of the INV_SUBTOTAL and the INV_TAX.
 */

DROP PROCEDURE IF EXISTS prc_inv_amounts;
DELIMITER $$
CREATE PROCEDURE prc_inv_amounts(IN inv_num INT)
  BEGIN
	-- create temp values for storing interim calculations
    DECLARE w_subtotal NUMERIC(8,2);
    DECLARE w_tax NUMERIC(8,2);
    DECLARE w_total NUMERIC (8,2);

	-- get subtotal value from sum of lines
    SELECT  SUM(line_total) INTO w_subtotal
    FROM    line
    GROUP BY inv_number
    HAVING  inv_number = inv_num;

    SET w_tax = w_subtotal * 0.08;

    SET w_total = w_subtotal + w_tax;

	-- Debugging statement to verify temp values
	SELECT w_tax, w_total, w_subtotal;

	-- Update invoice with appropriate values
	UPDATE	invoice
    SET		inv_subtotal = w_subtotal,
    		inv_tax = w_tax,
    		inv_total = w_total
    WHERE	inv_number = inv_num;

  END; $$
DELIMITER ;

-- Demonstrate that it works, first updating line total for every record in the line table

SELECT * FROM invoice WHERE inv_number = 1001;
START TRANSACTION;

UPDATE line
SET line_total = line_units * line_price;

CALL prc_inv_amounts(1001);
SELECT * FROM invoice WHERE inv_number = 1001;
ROLLBACK;
SELECT * FROM invoice WHERE inv_number = 1001;

/*  4. Create a procedure named prc_cus_balance_update that will take the
    invoice number as a parameter and update the customer balance. (Hint: You
    can use the DECLARE section to define a TOTINV numeric variable that holds
    the computed invoice total.)
 */
 
DROP PROCEDURE IF EXISTS prc_cus_balance_update;
DELIMITER $$
CREATE PROCEDURE prc_cus_balance_update(IN inv_num INT)
  BEGIN
	DECLARE w_cus CHAR(5);

    SELECT  cus_code INTO w_cus
    FROM    invoice
    WHERE	inv_number = inv_num;

	-- Debugging statement to verify value of w_cus
    SELECT w_cus;

	UPDATE	customer
    SET		cus_balance = cus_balance + (SELECT inv_total FROM invoice WHERE inv_number = inv_num)
    WHERE	cus_code = w_cus;

  END; $$
DELIMITER ;

-- Demonstrate that it works, using invoice 1001, customer 10014, first updating the invoice and line records
SELECT * FROM customer WHERE cus_code = 10014;
START TRANSACTION;

UPDATE line
SET line_total = line_units * line_price;

CALL prc_inv_amounts(1001);

CALL prc_cus_balance_update(1001);
SELECT * FROM customer WHERE cus_code = 10014;
ROLLBACK;
SELECT * FROM customer WHERE cus_code = 10014;
