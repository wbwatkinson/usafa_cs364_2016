/*  Run the lab18_SaleCo_schema file */
DROP DATABASE IF EXISTS cs364_lab18;
CREATE DATABASE cs364_lab18;
USE cs364_lab18;

CREATE TABLE customer LIKE cs364_lesson22.customer;
INSERT INTO customer SELECT * FROM cs364_lesson22.customer;

CREATE TABLE employee LIKE cs364_lesson22.employee;
INSERT INTO employee SELECT * FROM cs364_lesson22.employee; 

CREATE TABLE invoice LIKE cs364_lesson22.invoice;
INSERT INTO invoice SELECT * FROM cs364_lesson22.invoice;

CREATE TABLE line LIKE cs364_lesson22.line;
INSERT INTO line SELECT * FROM cs364_lesson22.line;

CREATE TABLE product LIKE cs364_lesson22.product;
INSERT INTO product SELECT * FROM cs364_lesson22.product;

CREATE TABLE vendor LIKE cs364_lesson22.vendor;
INSERT INTO vendor SELECT * FROM cs364_lesson22.vendor;

ALTER TABLE invoice
  ADD COLUMN inv_subtotal NUMERIC(8,2),
  ADD COLUMN inv_tax      NUMERIC(8,2),
  ADD COLUMN inv_total    NUMERIC(8,2);