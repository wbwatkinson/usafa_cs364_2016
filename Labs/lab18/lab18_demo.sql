/*  Run the lab18_SaleCo_schema file */
USE cs364_lesson22;

/* 	8.6 Sequences */
/*  Unlike Oracle, where Sequences are treated as a separate data type, MySQL
    uses sequences assigned to a column */

/* 	Modify the cus_code column to auto increment */
ALTER TABLE customer
CHANGE cus_code cus_code INT AUTO_INCREMENT PRIMARY KEY;

SELECT *
FROM customer;

/*	Set the auto increment starting value */
ALTER TABLE customer AUTO_INCREMENT = 20010;

/*	Change inv_number to auto increment and set the starting value */
ALTER TABLE invoice
CHANGE inv_number inv_number INT AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE invoice AUTO_INCREMENT = 4010;

/*  To invoke the auto increment values, use NULL */
INSERT INTO customer
VALUES (NULL, 'Connery', 'Sean', NULL, '615', '898-2007', 0.00);

INSERT INTO invoice VALUES (NULL, 20010, SYSDATE());
/*	LAST_INSERT_ID() returns the most recent auto increment value */
SELECT LAST_INSERT_ID();

/*	Add line items to invoice using the auto increment value for the corresponding
  invoice record */
INSERT INTO line VALUES (LAST_INSERT_ID(), 1, '13-Q2/P2', 1, 14.99);
INSERT INTO line VALUES (LAST_INSERT_ID(), 2, '23109-HB', 1, 9.95);

SELECT *
FROM customer;
SELECT *
FROM invoice;
SELECT *
FROM line;

DELETE FROM invoice
WHERE inv_number = 4010;
DELETE FROM customer
WHERE cus_code = 20010;


/*	8.7 Procedural SQL */

/* Examples of stored procedures */

/*	Simple stored procedure that returns the products table */
DROP PROCEDURE IF EXISTS getallproducts;
DELIMITER $$
CREATE PROCEDURE getallproducts()
  BEGIN
    SELECT *
    FROM product;
  END;
$$
DELIMITER ;

CALL getAllProducts();

/*	Stored procedure that sets a reorder flag is p_qoh is less than p_min */
DROP PROCEDURE IF EXISTS product_reorder;
DELIMITER $$
CREATE PROCEDURE product_reorder()
  BEGIN
    UPDATE product
    SET p_reorder = 1
    WHERE p_qoh <= p_min;
  END;
$$
DELIMITER ;

/*	Stored procedure that will adjust the price by a designated percentage */
DROP PROCEDURE IF EXISTS increaseprice;
DELIMITER $$
CREATE PROCEDURE increaseprice(IN percent NUMERIC(8, 2))
  BEGIN
    UPDATE product
    SET p_price = ROUND(p_price + p_price * percent, 2);
  END;
$$
DELIMITER ;

START TRANSACTION;
SELECT *
FROM product;
CALL increasePrice(.10);
SELECT *
FROM product;
ROLLBACK;

/*	Stored procedure that simplifies a SELECT query by allowing the input
  parameter designate a search term */
DROP PROCEDURE IF EXISTS getproducts;
DELIMITER $$
CREATE PROCEDURE getproducts(IN descript VARCHAR(35))
  BEGIN
    SELECT *
    FROM product
    WHERE p_descript LIKE CONCAT(CONCAT('%', descript), '%');
  END;
$$
DELIMITER ;

CALL getProducts('blade');
CALL getProducts('hammer');

/*	8.7.1 Triggers */

SELECT *
FROM product;
ALTER TABLE product
ADD COLUMN p_reorder INT NOT NULL DEFAULT 0;

/*	Trigger to set a reorder value on product update */
DROP TRIGGER IF EXISTS trg_product_reorder_update;
DELIMITER $$
CREATE TRIGGER trg_product_reorder_update
BEFORE UPDATE ON product FOR EACH ROW
  BEGIN
    IF new.p_qoh < new.p_min
    THEN
      SET new.p_reorder = 1;
    ELSE
      SET new.p_reorder = 0;
    END IF;
  END;
$$
DELIMITER ;

/*  Trigger to set a reorder flag on a reorder insert */
DROP TRIGGER IF EXISTS trg_product_reorder_insert;
DELIMITER $$
CREATE TRIGGER trg_product_reorder_insert
BEFORE INSERT ON product FOR EACH ROW
  BEGIN
    IF new.p_qoh < new.p_min
    THEN
      SET new.p_reorder = 1;
    ELSE
      SET new.p_reorder = 0;
    END IF;
  END;
$$
DELIMITER ;

SELECT *
FROM product
WHERE p_code = '11QER/31';

UPDATE product
SET p_qoh = 4
WHERE p_code = '11QER/31';

SELECT *
FROM product;

UPDATE product
SET p_qoh = 10
WHERE p_code = '11QER/31';

SELECT *
FROM product;

/*	Helper procuedure that will update the p_qoh based on an input amount.
  This procedure could be used after a shipment arrival or a customer
  purchase */
DROP PROCEDURE IF EXISTS updateprodqoh;
DELIMITER $$
CREATE PROCEDURE updateprodqoh(IN amount INT, IN prod VARCHAR(10))
  BEGIN
    UPDATE product
    SET product.p_qoh = product.p_qoh + amount
    WHERE product.p_code = prod;
  END;
$$
DELIMITER ;

/*	Helper procedure that will update a customer's balance.  This could
  be called after a payment is made or a purchase */
DROP PROCEDURE IF EXISTS updatecusbalance;
DELIMITER $$
CREATE PROCEDURE updatecusbalance(IN amount NUMERIC(8, 2), IN cus INT)
  BEGIN
    UPDATE customer
    SET customer.cus_balance = customer.cus_balance + amount
    WHERE customer.cus_code = cus;
  END;
$$
DELIMITER ;

/*	Trigger to update p_qoh and customer balance on a new invoice line */
DROP TRIGGER IF EXISTS trg_line;
DELIMITER $$
CREATE TRIGGER trg_line
AFTER INSERT ON line
FOR EACH ROW
  BEGIN
    DECLARE w_cus CHAR(5);

    SELECT cus_code
    INTO w_cus
    FROM invoice
    WHERE invoice.inv_number = new.inv_number;

    CALL updateProdQOH(-1.0 * new.line_units, new.p_code);
    CALL updateCusBalance(new.line_price * new.line_units, w_cus);
  END;
$$
DELIMITER ;

SELECT *
FROM product;
SELECT *
FROM customer;

INSERT INTO invoice VALUES (1009, 10014, SYSDATE());
INSERT INTO line VALUES (1009, 1, '13-Q2/P2', 1, 14.99);
INSERT INTO line VALUES (1009, 2, '11QER/31', 1, 109.99);

SELECT *
FROM product;
SELECT *
FROM customer;

