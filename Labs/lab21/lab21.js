/******************************************************************************
 Your Name,  Lab 21
 CS364, Information Storage and Retrieval, Spring 2016
 Instructor: Lt Col Blair Watkinson

 Documentation:
 Your detailed documentation statement here

 Observations, Feedback, and questions:
 Any feedback or questions to the instructor here
 ******************************************************************************/

// reset reviews database
load('reviews.js')

//  1. Query based on multiple criteria. Find all potions whose vendor is
//  Kettlecooked and strength rating is 5.
  db.potions.find(
    {
      "vendor": "Kettlecooked",
      "ratings.strength": 5
    }
  )

//  2. Query with conditionals. Find all potions with a price less than $20
    db.potions.find({"price": {"$lt": 20}}).pretty()

//  3. Query based on a range. Find all potions whose price is between
//  $10 and $20
    db.potions.find({"price": {"$gt":10, "$lt": 20}}).pretty()

//  4. Queries of non-equality. Find all potions whose vendor is not "Brewers"
    db.potions.find({"vendor": {"$ne": "Brewers"}}).pretty()

//  5. Range of queries on an array. Find all potions whose size is between 8
//  and 16
    db.potions.find({"sizes": {"$elemMatch": {"$gt": 8, "$lt": 16}}}).pretty()

//  6. Incorrect way to do a range query with arrays.
    db.potions.find({"sizes": {"$gt": 8, "$lt": 16}}).pretty()
    /* This query will return any document where there are values within the
    sizes array that match all criteria. However, no particular element is
    required to meet all the criteria.
     */

//  7. Projections: include only specific fields. Display just the vendor and
//  name of potions whose grade is greater than or equal to 80
    db.potions.find(
      {"grade": {"$gte": 80}},
      {"vendor": true, "name": true}
    ).pretty()

//  8. Projections: exclude specific fields. Do not display the vendor or price
//  of those potions whose grade is greater than or equal to 80
    db.potions.find(
      {"grade": {"$gte": 80}},
      {"vendor": false, "name": false}
    ).pretty()

//  9. Count the number of potions
    db.potions.find().count()

//  10. Sort the potions by price
    db.potions.find().sort({"price": 1}).pretty()
    /* -1 is descending; 1 is ascending */

//  11. Get the first three potions
    db.potions.find().limit(3).pretty()

//  12. Skip 2 potions, and return the next 2
    db.potions.find().skip(2).limit(2).pretty()

//  13. Display the two cheapest potions
    db.potions.find().sort({"price":1}).limit(2).pretty()


load('reviews.js')


/******************************************************************************
 * Lab Exercises
 ******************************************************************************/
//  1. Write the command that will find all wands whose maker is "Moonsap" and
//  level_required is 5


//  2. Write the command that will find all wands whose level_required is less
//  than or equal to 5


//  3. Write the command to find all winds that do not have "Love Burst" in
//  their powers.


//  4. Write the command that will find all wands having a melee damage between
//  30 and 40.


//  5. Write a query that will find wands having lengths between 2.5 and 3.


//  6. Write a query that will find wands whose maker is not "Foxmond",
//  level_required is less than or equal to 75, the price is less than 75 and
//  lengths are between 3 and 4.


//  7. Write a query that will find all wands, but display only the name field.
//  Sort the list by name.


//  8. Write a query that will find all wands, but exclude the price, lengths,
//  and _id field.


//  9. Write a query that will count the number of wands whose level_required
//  is 2


//  10. Write a query that will return the 3 most expensive wands.


