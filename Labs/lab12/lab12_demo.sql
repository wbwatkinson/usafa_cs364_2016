CREATE DATABASE IF NOT EXISTS cs364_lesson14;
USE cs364_lesson14;

/*  Drop tables if they exist
	Warning, this will delete all data in the tables and the tables themselves
	These commands should not be used in a production environment
	If the tables are linked by relationships, the tables on the "M" side of a 1:M relationship
	must be dropped first, if referential integrity is enforced */
DROP TABLE IF EXISTS line;
DROP TABLE IF EXISTS invoice;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS vendor;
DROP TABLE IF EXISTS customer;

/*  When tables are related through a 1:M relationship, 
	you must create the tables on the "1" side first */

# Create table for vendors
CREATE TABLE IF NOT EXISTS vendor (
	vendor_id		INTEGER 		PRIMARY KEY,
    vendor_name		VARCHAR(35)		NOT NULL,
    vendor_contact	VARCHAR(25)		NOT NULL,
    vendor_areacode	CHAR(3)			NOT NULL,
    vendor_phone	CHAR(8)			NOT NULL,
    vendor_state	CHAR(2)			NOT NULL,
    vendor_order	CHAR(1)			NOT NULL);

/* 	Create table for products
	Demonstrate alternative method to set the primary key
	ON UPDATE CASCADE on FOREIGN KEY does the following:
	Cannot delete a record from the vendor table if there is at least one product referencing it
	If a change to an existing vendor's vendor_id is made, that change is automatically updated
		in the corresponding product records
	Consequently, it is impossible for a product to refer to a vendor that doesn't exist, ensuring
		referential integrity */
CREATE TABLE IF NOT EXISTS product (
	product_id		VARCHAR(10)		NOT NULL,
    product_desc	VARCHAR(35)		NOT NULL,
    product_indate	DATE			NOT NULL,
	product_qoh		SMALLINT 		NOT NULL,
    product_min		SMALLINT		NOT NULL,
    product_price	NUMERIC(8,2)	NOT NULL,
    product_disc	NUMERIC(5,2)	NOT NULL,
    vendor_id		INTEGER,
    PRIMARY KEY (product_id),
    FOREIGN KEY (vendor_id) REFERENCES vendor(vendor_id) ON UPDATE CASCADE);

/*	Create a table called customer
	This table sets a default value for the area code
	Sets default balance to 0
	The last line as an additional constraint requiring that the 
		combination of last name and first name be unique.  This is
		for demonstration purposes, typical databases should allow 
	multiple people having the same last name and first name. */
CREATE TABLE IF NOT EXISTS customer (
	customer_id			INTEGER			PRIMARY KEY,
    customer_lname		VARCHAR(15)		NOT NULL,
    customer_fname		VARCHAR(15)		NOT NULL,
    customer_init		CHAR(1),
    customer_areacode	CHAR(3)			DEFAULT '615' NOT NULL,
    customer_phone		CHAR(8)			NOT NULL,
    customer_balance	NUMERIC(9,2)	DEFAULT 0.00,
    CONSTRAINT customer_u1 UNIQUE (customer_lname,customer_fname));

/*	Create table called invoice 
	Demonstrate alternative method to set foreign key */
CREATE TABLE IF NOT EXISTS invoice (
	invoice_id			INTEGER			PRIMARY KEY,
    customer_id			INTEGER			NOT NULL REFERENCES customer(customer_id),
    invoice_date		DATE			NOT NULL);

/*	Create a line table for lines in an invoice
	Demonstrate a composite primary key */
CREATE TABLE IF NOT EXISTS line (
	invoice_id			INTEGER			NOT NULL,
    line_number			NUMERIC(2,0)	NOT NULL,
    product_id			VARCHAR(10)		NOT NULL,
    line_units			NUMERIC(9,2)	NOT NULL,
    line_price			NUMERIC(9,2)	NOT NULL,
    PRIMARY KEY (invoice_id, line_number),
    FOREIGN KEY (invoice_id) REFERENCES invoice(invoice_id) ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES product(product_id),
    CONSTRAINT line_u1 UNIQUE (invoice_id, product_id));
    
INSERT INTO customer VALUES('10010','Ramas','Alfred','A','615','844-2573','0');
INSERT INTO customer VALUES('10011','Dunne','Leona','K','713','894-1238','0');
INSERT INTO customer VALUES('10012','Smith','Kathy','W','615','894-2285','345.859985351562');
INSERT INTO customer VALUES('10013','Olowski','Paul','F','615','894-2180','536.75');
INSERT INTO customer VALUES('10014','Orlando','Myron','','615','222-1672','0');
INSERT INTO customer VALUES('10015','O''Brian','Amy','B','713','442-3381','0');
INSERT INTO customer VALUES('10016','Brown','James','G','615','297-1228','221.190002441406');
INSERT INTO customer VALUES('10017','Williams','George','','615','290-2556','768.929992675781');
INSERT INTO customer VALUES('10018','Farriss','Anne','G','713','382-7185','216.550003051758');
INSERT INTO customer VALUES('10019','Smith','Olette','K','615','297-3809','0');

SELECT * FROM customer;

INSERT INTO vendor VALUES('21225','Bryson, Inc.','Smithson','615','223-3234','TN','Y');
INSERT INTO vendor VALUES('21226','SuperLoo, Inc.','Flushing','904','215-8995','FL','N');
INSERT INTO vendor VALUES('21231','D&E Supply','Singh','615','228-3245','TN','Y');
INSERT INTO vendor VALUES('21344','Gomez Bros.','Ortega','615','889-2546','KY','N');
INSERT INTO vendor VALUES('22567','Dome Supply','Smith','901','678-1419','GA','N');
INSERT INTO vendor VALUES('23119','Randsets Ltd.','Anderson','901','678-3998','GA','Y');
INSERT INTO vendor VALUES('24004','Brackman Bros.','Browning','615','228-1410','TN','N');
INSERT INTO vendor VALUES('24288','ORDVA, Inc.','Hakford','615','898-1234','TN','Y');
INSERT INTO vendor VALUES('25443','B&K, Inc.','Smith','904','227-0093','FL','N');
INSERT INTO vendor VALUES('25501','Damal Supplies','Smythe','615','890-3529','TN','N');
INSERT INTO vendor VALUES('25595','Rubicon Systems','Orton','904','456-0092','FL','Y');

SELECT * FROM vendor;

/*	Add product values to database
	Note that products with null vendor ids are still added (products are not required to have a vendor) */
INSERT INTO product VALUES('11QER/31','Power painter, 15 psi., 3-nozzle','2013-11-3','8','5','109.99','0','25595');
INSERT INTO product VALUES('13-Q2/P2','7.25-in. pwr. saw blade','2013-12-13','32','15', '14.99','0.05','21344');
INSERT INTO product VALUES('14-Q1/L3','9.00-in. pwr. saw blade','2013-11-13','18','12','17.49','0','21344');
INSERT INTO product VALUES('1546-QQ2','Hrd. cloth, 1/4-in., 2x50','2014-1-15','15','8','39.95','0','23119');
INSERT INTO product VALUES('1558-QW1','Hrd. cloth, 1/2-in., 3x50','2014-1-15','23','5','43.99','0','23119');
INSERT INTO product VALUES('2232/QTY','B\&D jigsaw, 12-in. blade','2013-12-30','8','5','109.92','0.05','24288');
INSERT INTO product VALUES('2232/QWE','B\&D jigsaw, 8-in. blade','2013-12-24','6','5','99.87','0.05','24288');
INSERT INTO product VALUES('2238/QPD','B\&D cordless drill, 1/2-in.','2014-1-20','12','5','38.95','0.05','25595');
INSERT INTO product VALUES('23109-HB','Claw hammer','2014-1-20','23','10','9.95','0.1','21225');
INSERT INTO product VALUES('23114-AA','Sledge hammer, 12 lb.','2014-1-2','8','5','14.40','0.05', NULL);
INSERT INTO product VALUES('54778-2T','Rat-tail file, 1/8-in. fine','2013-12-15','43','20','4.99','0','21344');
INSERT INTO product VALUES('89-WRE-Q','Hicut chain saw, 16 in.','2014-2-7','11','5','256.99','0.05','24288');
INSERT INTO product VALUES('PVC23DRT','PVC pipe, 3.5-in., 8-ft','2014-2-20','188','75','5.87','0', NULL);
INSERT INTO product VALUES('SM-18277','1.25-in. metal screw, 25','2014-3-1','172','75','6.99','0','21225');
INSERT INTO product VALUES('SW-23116','2.5-in. wd. screw, 50','2014-2-24','237','100','8.45','0','21231');
INSERT INTO product VALUES('WR3/TT3','Steel matting, 4''x8''x1/6", .5" mesh','2014-1-17','18','5','119.95','0.1','25595');

SELECT * FROM product;

INSERT INTO invoice VALUES('1001','10014','2014-1-16');
INSERT INTO invoice VALUES('1002','10011','2014-1-16');
INSERT INTO invoice VALUES('1003','10012','2014-1-16');
INSERT INTO invoice VALUES('1004','10011','2014-1-17');
INSERT INTO invoice VALUES('1005','10018','2014-1-17');
INSERT INTO invoice VALUES('1006','10014','2014-1-17');
INSERT INTO invoice VALUES('1007','10015','2014-1-17');
INSERT INTO invoice VALUES('1008','10011','2014-1-17');

SELECT * FROM invoice;

INSERT INTO line VALUES('1001','1','13-Q2/P2','1','14.99');
INSERT INTO line VALUES('1001','2','23109-HB','1','9.95');
INSERT INTO line VALUES('1002','1','54778-2T','2','4.99');
INSERT INTO line VALUES('1003','1','2238/QPD','1','38.95');
INSERT INTO line VALUES('1003','2','1546-QQ2','1','39.95');
INSERT INTO line VALUES('1003','3','13-Q2/P2','5','14.99');
INSERT INTO line VALUES('1004','1','54778-2T','3','4.99');
INSERT INTO line VALUES('1004','2','23109-HB','2','9.95');
INSERT INTO line VALUES('1005','1','PVC23DRT','12','5.87');
INSERT INTO line VALUES('1006','1','SM-18277','3','6.99');
INSERT INTO line VALUES('1006','2','2232/QTY','1','109.92');
INSERT INTO line VALUES('1006','3','23109-HB','1','9.95');
INSERT INTO line VALUES('1006','4','89-WRE-Q','1','256.99');
INSERT INTO line VALUES('1007','1','13-Q2/P2','2','14.99');
INSERT INTO line VALUES('1007','2','54778-2T','1','4.99');
INSERT INTO line VALUES('1008','1','PVC23DRT','5','5.87');
INSERT INTO line VALUES('1008','2','WR3/TT3','3','119.95');
INSERT INTO line VALUES('1008','3','23109-HB','1','9.95');

SELECT * FROM line;

UPDATE	product
SET		product_indate = '2014-01-18'
WHERE	product_id = '13-Q2/P2';

SELECT * FROM product;

UPDATE product
SET    product_indate = '2014-01-18', product_price = 17.99, product_min = 10
WHERE  product_id = '13-Q2/P2';

SELECT * FROM product;

DELETE FROM	product
WHERE		product_code = 'BRT-345';

SELECT * FROM product;

DELETE FROM product
WHERE		product_min = 5;

SELECT * FROM product;