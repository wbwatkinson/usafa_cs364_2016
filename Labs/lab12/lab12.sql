/* Lab 12 Assignment */

CREATE DATABASE IF NOT EXISTS cs364_lesson14;
USE cs364_lesson14;

/* 1. Create a table 
	Write the SQL code that will create the table structure
	for a table named employee1. The basic employee table structure is 
	summarized in the table below.
 emp_num		CHAR(3)
 emp_lname		VARCHAR(15)
 emp_fname		VARCHAR(15)
 emp_initial	CHAR(1)
 emp_hiredate	DATE
 job_code		CHAR(3) */

/* 2. Insert data into table
	Having created the table structure in Problem 3, write the SQL code to
	enter the first three rows for the table shown below. Note that job_code
	is a foreign key to job
	emp_num	emp_lname	emp_fname	emp_initial	emp_hiredate	job_code
	101		News		John		G			08-Nov-00		502
	102		Senior		David		H			12-Jul-89		501
	103		Arbough		June		E			01-Dec-96		500
	104		Ramoras		Anne		K			01-Dec-96	    502 */


/* 3. Display attributes
	Assuming the data shown in the employee1 table have been entered,
	write the SQL code that will list all attributes for a job code of 502. */


/* 4. Change a row
	Write the SQL code to change the job code to 501 for the person 
	whose employee number (employee_id) is 107 in the employee table. After you 
	have completed the task, examine the results, and then reset the job code 
	to its original value. */


/* 5. Delete a row
	Write the SQL code to delete the row for the person named William Smithfield,
	who was hired on June 22, 2004, and whose job code classification is 500. 
	(Hint: Use logical operators to include all of the information given in this problem.) */
    
    