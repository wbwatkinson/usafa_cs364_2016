# MEAN Tracer

* MongoDB
* Express
* Angular
* Node

*Requires MongoDB server running*

## Developing

* `npm install` to resolve dependencies
* Seed database: `mongoimport --db bookswap --collection users --type json --file server/users-seed.json --jsonArray --drop`
