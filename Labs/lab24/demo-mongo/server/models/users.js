/**
 * Created by Warren.Watkinson on 4/15/2016.
 */

var mongo = require('mongodb');

var Users = {
  getAll: function (callback) {
    var collection = mongo.DB.collection('users');

    collection.find().toArray(function (err, docs) {
      callback(err, docs);
    });
  },

  findByUserName: function (username, callback) {
    var collection = mongo.DB.collection('users');

    var query = {username: username};
    collection.find(query).limit(1).next(function (err, doc) {
      callback(err, doc);
    });

  }
};

module.exports = Users;