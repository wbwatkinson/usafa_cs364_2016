/**
 * Created by Warren.Watkinson on 4/13/2016.
 */

var express = require('express');
var path = require('path');
var mongoUtil = require(path.join(__dirname,'services','mongoUtil'));


// Connect to Mongo
mongoUtil.connect();

var app = express();
var routes = require(path.join(__dirname,'controllers','routes'));

app.use(express.static(path.join(__dirname,'..','client')));
app.use(routes);

var port = process.argv[2] || 3000;
app.listen(port, function() {
  var d = new Date();
  console.log(d.toUTCString() + ': Listening on port ' + port + '...');
});