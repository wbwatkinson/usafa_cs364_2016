/**
 * Created by Warren.Watkinson on 4/15/2016.
 */
var express = require('express');
var router = express.Router();
var path = require('path');

var Users = require(path.join(__dirname,'..','models','users'));

router
  .get("/", function(req, res) {
    Users.getAll(function(err, docs) {
      if(err) {
        console.log('There was an error finding all users at route /users')
      }
      console.log("Users:" + JSON.stringify(docs));
      res.json(docs);
    });
  })

  .get("/:username", function(req, res) {
    var username = req.params.username;
    Users.findByUserName(username, function (err, doc) {
      if (err) {
        console.log('There was an error finding user with username: ' + username +
          ' at route /users/' + username);
      }
      console.log("User:" + JSON.stringify(doc));
      res.json(doc);
    });
  });

module.exports = router;