/**
 * Created by Warren.Watkinson on 4/15/2016.
 */
var express = require('express')
var router = express.Router();
var path = require('path');

var users = require(path.join(__dirname,'users'));

router.use('/users',users);

module.exports = router;