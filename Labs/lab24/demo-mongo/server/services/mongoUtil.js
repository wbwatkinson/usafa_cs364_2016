/**
 * Created by Warren.Watkinson on 4/13/2016.
 */

var mongo = require('mongodb');
var connection = mongo.MongoClient;

exports.connect = function() {
  if(mongo.DB) {return mongo.DB}

  connection.connect('mongodb://localhost:27017/bookswap', function (err, db) {
    var d = new Date();
    if (err) {
      console.log(d.toUTCString() + ': Problem with mongo, shutting down server');
      process.exit();
    } else {
      mongo.DB = db;
      console.log(d.toUTCString() + ': Mongo server starting');
    }
  });
};

exports.users = function() {
  console.log(_db.collection('users'));
  return _db.collection('users');
};
