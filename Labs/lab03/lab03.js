/******************************************************************************
 Your Name,  Assignment Name
 CS364, Information Storage and Retrieval, Spring 2016
 Instructor: Lt Col Blair Watkinson

 Documentation:
 Your detailed documentation statement here

 Observations, Feedback, and questions:
 Any feedback or questions to the instructor here
 ******************************************************************************/

var usafaClasses = [
  ['Class Year','Class Exemplar','Graduation Date'],
  ['2000','General James H. &ldquo;Jimmy&rdquo; Doolittle','31 May 2000'],
  ['2001','Brigadier General William &ldquo;Billy&rdquo; Mitchell','30 May 2001'],
  ['2002','Captain Lance P. Sijan','29 May 2002'],
  ['2003','Major Richard I. Bong','28 May 2003'],
  ['2004','Captain Eddie Rickenbacker','2 Jun 2004'],
  ['2005','General George S. Patton Jr.','1 Jun 2005'],
  ['2006','General Carl A. &ldquo;Tooey&rdquo; Spaatz','31 May 2006'],
  ['2007','Lieutenant Colonel Virgil I. &ldquo;Gus&rdquo; Grissom','30 May 2007'],
  ['2008','1LT Karl W. Richter','28 May 2008'],
  ['2009','Colonel Hubert &ldquo;Hub&rdquo; Zemke','27 May 2009'],
  ['2010','2Lt Frank Luke Jr.','26 May 2010'],
  ['2011','Brigadier General Robin Olds','25 May 2011'],
  ['2012','General of the AF Henry H. &ldquo;Hap&rdquo; Arnold','23 May 2012'],
  ['2013','General Curtis E. Lemay','29 May 2013'],
  ['2014','Lieutenant Colonel Jay Zeamer Jr.','28 May 2014'],
  ['2015','Wilbur and Orville Wright','28 May 2015'],
  ['2016','Major David Brodeur',''],
  ['2017','Col George Everett &lsquo;Bud&rsquo; Day','']];

/* insertTable(dataTable) generates a table from a 2D array */
function insertTable(dataTable) {
  var html = '';
  var selectedClassYear = Math.floor(Math.random() * 18 + 2000);
  console.log(selectedClassYear);

  /*  iterate through each row in the 2D array
        set the array at the row to a temp value
        if it's the first row, set <thead> tag
        if the first element of the row is equal to the selectedClassYear,
          add a tr tag with the select-class-year id
        else just add a tr tag

        iterate through each column in the row
          add a <td> tag, followed by the value in the array and then a </td> tag
        add a </tr> tag
        if it's the first row, add </thead> and <tbody> tags
      close the </tbody> tag
      set the innerHTML of the usafa-class-table id to the created html string
  */

}

insertTable(usafaClasses);

