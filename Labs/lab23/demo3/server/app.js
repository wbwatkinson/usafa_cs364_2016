/**
 * Demo Basic Web Server with basic static routes
 */

var path = require('path');
var port = process.argv[2];

var express = require('express');
var app = express();

app.use(express.static(path.join(__dirname,'/../client')));

app.listen(port, function() {
  var d = new Date();
  console.log(d.toUTCString() + ': Listening on port ' + port + '...');
});