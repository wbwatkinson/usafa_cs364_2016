/**
 * Demo Basic Web Server
 *
 * This web server will serve the file index.html upon connection
 */

var http = require('http');
var fs = require('fs');
var port = process.argv[2] || 8231; // use command line argument or port 8231

http.createServer(function(request,response){
  /**
   * Write a 200 "Status OK" HTTP header
   * See https://en.wikipedia.org/wiki/List_of_HTTP_status_codes for additional
   *status codes
   */
  response.writeHead(200,{'Content-Type':'text/html'});

  /**
   * Serve the file named index.html
   * fs.readFile will attempt to asynchronously read the contents of index.html
   * and write them to the variable contents.
   */
  fs.readFile('index.html', function(err,contents) {
    /**
     * write the contents of index.html to the stream
     */
    response.write(contents);
    /**
     * end the stream
     */
    response.end();
  });

/**
 * Listen for connections on 8231 or the port passed in via command line
  */
}).listen(port);

console.log("Basic web server is listening on port " + port + "...");