var express = require('express');
var router = express.Router();
var _ = require('lodash');  // the lodash library simplifies array, etc, operations

var bodyParser = require('body-parser');
var parseCityName = require('./parse-city-name')();

var originalCities = {
  'Lotopia': 'Rough and mountainous',
  'Caspiana': 'Sky-top island',
  'Indigo': 'Vibrant and thriving',
  'Paradise': 'Lush, green plantation',
  'Flotilla': 'Bustling urban oasis'
};

var cities = _.clone(originalCities);

var resetRoute = router.route('/reset');
resetRoute.get(function(request, response) {
  cities = _.clone(originalCities);
  response.redirect('/');
});

var parseUrlencoded = bodyParser.urlencoded({ extended: false });

router.route('/')
  .get(function (request, response) {
    var names = Object.keys(cities);
    if(request.query.limit >= 0){
      response.json(names.slice(0, request.query.limit));
    }else{
      response.json(names);
    }
  })
  .post(parseUrlencoded, function (request, response) {
    var newCity = request.body;
    cities[newCity.name] = newCity.description;

    response.status(201).json(newCity.name);
  });

router.route('/:name')
  .all(parseCityName)
  .get(function (request, response) {
    var description = cities[request.cityName];

    if(!description){
      response.status(404).json('No description found for ' + request.model);
    }else{
      response.json(description);
    }
  })
  .delete(function (request, response) {
    delete cities[request.cityName];
    response.sendStatus(200);
  });

module.exports = router;
