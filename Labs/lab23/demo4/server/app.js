var express = require('express');
var path = require('path');

var app = express();

var logger = require('./logger');
app.use(logger);

app.use(express.static(path.join(__dirname,'/../client')));

var cities = require('./routes/cities');
app.use('/cities', cities);

var port = process.argv[2] || 3000;
app.listen(port, function() {
  var d = new Date();
  console.log(d.toUTCString() + ': Listening on port ' + port + '...');
});
