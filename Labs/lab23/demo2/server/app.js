/**
 * Demo Basic Web Server with basic static routes
 *
 * This web server will serve the file index.html upon connection
 */

var path = require('path');
var port = process.argv[2];

var express = require('express');
var app = express();

app.get('/index.html',function(request,response){
  response.sendFile(path.join(__dirname, '../client', 'index.html'));
  console.log('Sent: index.html');
});

app.get('/other.html',function(request,response){
  response.sendFile(path.join(__dirname, '../client', 'other.html'));
  console.log('Sent: other.html');
});

app.get('/*',function(request,response){
  console.log('File not found: ' + request.params[0]);
  response.sendStatus(404);
});

app.listen(port, function() {
  console.log("Listening on port " + port + "...");
});