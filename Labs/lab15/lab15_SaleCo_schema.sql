/* SQLINTRODBINIT.SQL                                 	*/
/* Introduction to SQL 					*/
/* Script file for ORACLE  DBMS				*/
/* This script file creates the following tables:	*/
/* v  - default vendor table data			*/
/* p  - default product table data			*/
/* vendor, product, customer, invoice, line		*/
/* employee, emp					*/
/* and loads the default data rows			*/
DROP DATABASE IF EXISTS cs364_lesson19;
CREATE DATABASE IF NOT EXISTS cs364_lesson19;
USE cs364_lesson19;

DROP TABLE IF EXISTS line;
DROP TABLE IF EXISTS invoice;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS vendor;
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS emp;
DROP TABLE IF EXISTS p;
DROP TABLE IF EXISTS v;

CREATE TABLE v (
    v_code 		INTEGER PRIMARY KEY,
    v_name 		VARCHAR(35) NOT NULL,
    v_contact 	VARCHAR(15) NOT NULL,
    v_areacode 	CHAR(3) NOT NULL,
    v_phone 	CHAR(8) NOT NULL,
    v_state 	CHAR(2) NOT NULL,
    v_order 	CHAR(1) NOT NULL);


CREATE TABLE p (
    p_code 		VARCHAR(10)     PRIMARY KEY,
    p_descript 	VARCHAR(35)     NOT NULL,
    p_indate 	DATE            NOT NULL,
    p_qoh	 	INTEGER         NOT NULL,
    p_min 		INTEGER         NOT NULL,
    p_price 	NUMERIC(8,2)    NOT NULL,
    p_discount 	NUMERIC(5,2)    NOT NULL,
    v_code 		INTEGER);


CREATE TABLE vendor (
    v_code 		INTEGER,
    v_name 		VARCHAR(35)     NOT NULL,
    v_contact 	VARCHAR(15)     NOT NULL,
    v_areacode 	CHAR(3)         NOT NULL,
    v_phone 	CHAR(8)         NOT NULL,
    v_state 	CHAR(2)         NOT NULL,
    v_order 	CHAR(1)         NOT NULL,
    PRIMARY KEY (v_code));


CREATE TABLE product (
    p_code 	    VARCHAR(10)     PRIMARY KEY,
    p_descript  VARCHAR(35)     NOT NULL,
    p_indate 	DATE            NOT NULL,
    p_qoh 	  	NUMERIC         NOT NULL,
    p_min 		NUMERIC         NOT NULL,
    p_price 	NUMERIC(8,2)    NOT NULL,
    p_discount 	NUMERIC(5,2)    NOT NULL,
    v_code 		INTEGER,
    FOREIGN KEY (v_code) REFERENCES vendor(v_code));

CREATE TABLE customer (
    cus_code	    NUMERIC         PRIMARY KEY,
    cus_lname	    VARCHAR(15)     NOT NULL,
    cus_fname	    VARCHAR(15)     NOT NULL,
    cus_initial	    CHAR(1),
    cus_areacode 	CHAR(3)         DEFAULT '615' NOT NULL CHECK(CUS_AREACODE IN ('615','713','931')),
    cus_phone	    CHAR(8)         NOT NULL,
    cus_balance	    NUMERIC(9,2)    DEFAULT 0.00,
    CONSTRAINT cus_ui1 UNIQUE(cus_lname,cus_fname));


CREATE TABLE invoice (
    inv_number  INTEGER     PRIMARY KEY,
    cus_code	INTEGER     NOT NULL REFERENCES customer(cus_code),
    inv_date  	DATE        NOT NULL);


CREATE TABLE line (
    inv_number 	INTEGER         NOT NULL,
    line_number	NUMERIC(2,0)    NOT NULL,
    p_code		VARCHAR(10)     NOT NULL,
    line_units	NUMERIC(9,2)    DEFAULT 0.00 NOT NULL,
    line_price	NUMERIC(9,2)    DEFAULT 0.00 NOT NULL,
    PRIMARY KEY (inv_number,line_number),
    FOREIGN KEY (inv_number) REFERENCES invoice(inv_number) ON DELETE CASCADE,
    FOREIGN KEY (p_code) REFERENCES product(p_code),
    CONSTRAINT line_ui1 UNIQUE(inv_number, p_code));


CREATE TABLE employee (
    emp_num		    NUMERIC 	PRIMARY KEY,
    emp_title	    CHAR(10),
    emp_lname	    VARCHAR(15) NOT NULL,
    emp_fname	    VARCHAR(15) NOT NULL,
    emp_initial	    CHAR(1),
    emp_dob		    DATE,
    emp_hire_date	DATE,
    emp_years	    INTEGER,
    emp_areacode 	CHAR(3),
    emp_phone	    CHAR(8));


CREATE TABLE emp (
    emp_num		    NUMERIC 	PRIMARY KEY,
    emp_title	    CHAR(10),
    emp_lname	    VARCHAR(15) NOT NULL,
    emp_fname	    VARCHAR(15) NOT NULL,
    emp_initial	    CHAR(1),
    emp_dob		    DATE,
    emp_hire_date	DATE,
    emp_areacode 	CHAR(3),
    emp_phone	    CHAR(8),
    emp_mgr		    NUMERIC);



/* v rows						*/
INSERT INTO v VALUES(21225,'Bryson, Inc.'    ,'Smithson','615','223-3234','TN','Y');
INSERT INTO v VALUES(21226,'SuperLoo, Inc.'  ,'Flushing','904','215-8995','FL','N');
INSERT INTO v VALUES(21231,'D&E Supply'     ,'Singh'   ,'615','228-3245','TN','Y');
INSERT INTO v VALUES(21344,'Gomez Bros.'     ,'Ortega'  ,'615','889-2546','KY','N');
INSERT INTO v VALUES(22567,'Dome Supply'     ,'Smith'   ,'901','678-1419','GA','N');
INSERT INTO v VALUES(23119,'Randsets Ltd.'   ,'Anderson','901','678-3998','GA','Y');
INSERT INTO v VALUES(24004,'Brackman Bros.'  ,'Browning','615','228-1410','TN','N');
INSERT INTO v VALUES(24288,'ORDVA, Inc.'     ,'Hakford' ,'615','898-1234','TN','Y');
INSERT INTO v VALUES(25443,'B&K, Inc.'      ,'Smith'   ,'904','227-0093','FL','N');
INSERT INTO v VALUES(25501,'Damal Supplies'  ,'Smythe'  ,'615','890-3529','TN','N');
INSERT INTO v VALUES(25595,'Rubicon Systems' ,'Orton'   ,'904','456-0092','FL','Y');

/* p rows						*/
INSERT INTO p VALUES('11QER/31','Power painter, 15 psi., 3-nozzle'     ,'2013-11-03',  8,  5,109.99,0.00,25595);
INSERT INTO p VALUES('13-Q2/P2','7.25-in. pwr. saw blade'              ,'2013-12-13', 32, 15, 14.99,0.05,21344);
INSERT INTO p VALUES('14-Q1/L3','9.00-in. pwr. saw blade'              ,'2013-11-13', 18, 12, 17.49,0.00,21344);
INSERT INTO p VALUES('1546-QQ2','Hrd. cloth, 1/4-in., 2x50'            ,'2014-01-15', 15,  8, 39.95,0.00,23119);
INSERT INTO p VALUES('1558-QW1','Hrd. cloth, 1/2-in., 3x50'            ,'2014-01-15', 23,  5, 43.99,0.00,23119);
INSERT INTO p VALUES('2232/QTY','B&D jigsaw, 12-in. blade'            ,'2013-12-30',  8,  5,109.92,0.05,24288);
INSERT INTO p VALUES('2232/QWE','B&D jigsaw, 8-in. blade'             ,'2013-12-24',  6,  5, 99.87,0.05,24288);
INSERT INTO p VALUES('2238/QPD','B&D cordless drill, 1/2-in.'         ,'2014-01-20', 12,  5, 38.95,0.05,25595);
INSERT INTO p VALUES('23109-HB','Claw hammer'                          ,'2014-01-20', 23, 10,  9.95,0.10,21225);
INSERT INTO p VALUES('23114-AA','Sledge hammer, 12 lb.'                ,'2014-01-02',  8,  5, 14.40,0.05,NULL);
INSERT INTO p VALUES('54778-2T','Rat-tail file, 1/8-in. fine'          ,'2013-12-15', 43, 20,  4.99,0.00,21344);
INSERT INTO p VALUES('89-WRE-Q','Hicut chain saw, 16 in.'              ,'2014-02-07', 11,  5,256.99,0.05,24288);
INSERT INTO p VALUES('PVC23DRT','PVC pipe, 3.5-in., 8-ft'              ,'2014-02-20',188, 75,  5.87,0.00,NULL);
INSERT INTO p VALUES('SM-18277','1.25-in. metal screw, 25'             ,'2014-03-01',172, 75,  6.99,0.00,21225);
INSERT INTO p VALUES('SW-23116','2.5-in. wd. screw, 50'                ,'2014-02-24',237,100,  8.45,0.00,21231);
INSERT INTO p VALUES('WR3/TT3' ,'Steel matting, 4''x8''x1/6", .5" mesh','2014-01-17', 18,  5,119.95,0.10,25595);


/* vendor rows						*/
INSERT INTO vendor (SELECT * FROM v);

/* product rows						*/
INSERT INTO product (SELECT * FROM p);

/* customer rows					*/
INSERT INTO customer VALUES(10010,'Ramas'   ,'Alfred','A' ,'615','844-2573',0);
INSERT INTO customer VALUES(10011,'Dunne'   ,'Leona' ,'K' ,'713','894-1238',0);
INSERT INTO customer VALUES(10012,'Smith'   ,'Kathy' ,'W' ,'615','894-2285',345.86);
INSERT INTO customer VALUES(10013,'Olowski' ,'Paul'  ,'F' ,'615','894-2180',536.75);
INSERT INTO customer VALUES(10014,'Orlando' ,'Myron' ,NULL,'615','222-1672',0);
INSERT INTO customer VALUES(10015,'O''Brian','Amy'   ,'B' ,'713','442-3381',0);
INSERT INTO customer VALUES(10016,'Brown'   ,'James' ,'G' ,'615','297-1228',221.19);
INSERT INTO customer VALUES(10017,'Williams','George',NULL,'615','290-2556',768.93);
INSERT INTO customer VALUES(10018,'Farriss' ,'Anne'  ,'G' ,'713','382-7185',216.55);
INSERT INTO customer VALUES(10019,'Smith'   ,'Olette','K' ,'615','297-3809',0);

/* invoice rows						*/
INSERT INTO invoice VALUES(1001,10014,'2014-01-16');
INSERT INTO invoice VALUES(1002,10011,'2014-01-16');
INSERT INTO invoice VALUES(1003,10012,'2014-01-16');
INSERT INTO invoice VALUES(1004,10011,'2014-01-17');
INSERT INTO invoice VALUES(1005,10018,'2014-01-17');
INSERT INTO invoice VALUES(1006,10014,'2014-01-17');
INSERT INTO invoice VALUES(1007,10015,'2014-01-17');
INSERT INTO invoice VALUES(1008,10011,'2014-01-17');

/* line rows						*/
INSERT INTO line VALUES(1001,1,'13-Q2/P2',1,14.99);
INSERT INTO line VALUES(1001,2,'23109-HB',1,9.95);
INSERT INTO line VALUES(1002,1,'54778-2T',2,4.99);
INSERT INTO line VALUES(1003,1,'2238/QPD',1,38.95);
INSERT INTO line VALUES(1003,2,'1546-QQ2',1,39.95);
INSERT INTO line VALUES(1003,3,'13-Q2/P2',5,14.99);
INSERT INTO line VALUES(1004,1,'54778-2T',3,4.99);
INSERT INTO line VALUES(1004,2,'23109-HB',2,9.95);
INSERT INTO line VALUES(1005,1,'PVC23DRT',12,5.87);
INSERT INTO line VALUES(1006,1,'SM-18277',3,6.99);
INSERT INTO line VALUES(1006,2,'2232/QTY',1,109.92);
INSERT INTO line VALUES(1006,3,'23109-HB',1,9.95);
INSERT INTO line VALUES(1006,4,'89-WRE-Q',1,256.99);
INSERT INTO line VALUES(1007,1,'13-Q2/P2',2,14.99);
INSERT INTO line VALUES(1007,2,'54778-2T',1,4.99);
INSERT INTO line VALUES(1008,1,'PVC23DRT',5,5.87);
INSERT INTO line VALUES(1008,2,'WR3/TT3',3,119.95);
INSERT INTO line VALUES(1008,3,'23109-HB',1,9.95);

/* emp rows						*/
INSERT INTO emp VALUES(100,'Mr.' ,'Kolmycz'   ,'George' ,'D' ,'1942-06-15','1985-03-15','615','324-5456',NULL);
INSERT INTO emp VALUES(101,'Ms.' ,'Lewis'     ,'Rhonda' ,'G' ,'1965-03-19','1986-04-25','615','324-4472',100);
INSERT INTO emp VALUES(102,'Mr.' ,'Vandam'    ,'Rhett'  ,NULL,'1958-11-14','1990-12-20','901','675-8993',100);
INSERT INTO emp VALUES(103,'Ms.' ,'Jones'     ,'Anne'   ,'M' ,'1974-10-16','1994-08-28','615','898-3456',100);
INSERT INTO emp VALUES(104,'Mr.' ,'Lange'     ,'John'   ,'p' ,'1971-11-08','1994-10-20','901','504-4430',105);
INSERT INTO emp VALUES(105,'Mr.' ,'Williams'  ,'Robert' ,'D' ,'1975-03-14','1998-11-08','615','890-3220',NULL);
INSERT INTO emp VALUES(106,'Mrs.','Smith'     ,'Jeanine','K' ,'1968-02-12','1989-01-05','615','324-7883',105);
INSERT INTO emp VALUES(107,'Mr.' ,'Diante'    ,'Jorge'  ,'D' ,'1074-08-21','1994-07-02','615','890-4567',105);
INSERT INTO emp VALUES(108,'Mr.' ,'Wiesenbach','Paul'   ,'R' ,'1966-02-14','1992-11-18','615','897-4358',NULL);
INSERT INTO emp VALUES(109,'Mr.' ,'Smith'     ,'George' ,'K' ,'1961-06-18','1989-04-14','901','504-3339',108);
INSERT INTO emp VALUES(110,'Mrs.','Genkazi'   ,'Leighla','W' ,'1970-05-19','1990-12-01','901','569-0093',108);
INSERT INTO emp VALUES(111,'Mr.' ,'Washington','Rupert' ,'E' ,'1966-01-03','1993-06-21','615','890-4925',105);
INSERT INTO emp VALUES(112,'Mr.' ,'Johnson'   ,'Edward' ,'E' ,'1961-05-14','1983-12-01','615','898-4387',100);
INSERT INTO emp VALUES(113,'Ms.' ,'Smythe'    ,'Melanie','p' ,'1970-09-15','1999-05-11','615','324-9006',105);
INSERT INTO emp VALUES(114,'Ms.' ,'Brandon'   ,'Marie'  ,'G' ,'1956-11-02','1979-11-15','901','882-0845',108);
INSERT INTO emp VALUES(115,'Mrs.','Saranda'   ,'Hermine','R' ,'1972-07-25','1993-04-23','615','324-5505',105);
INSERT INTO emp VALUES(116,'Mr.' ,'Smith'     ,'George' ,'A' ,'1965-11-08','1988-12-10','615','890-2984',108);

/* employee rows					*/
INSERT INTO employee VALUES(100,'Mr.' ,'Kolmycz'   ,'George' ,'D' ,'1942-06-15','1985-03-15',24,'615','324-5456');
INSERT INTO employee VALUES(101,'Ms.' ,'Lewis'     ,'Rhonda' ,'G' ,'1965-03-19','1986-04-25',22,'615','324-4472');
INSERT INTO employee VALUES(102,'Mr.' ,'Vandam'    ,'Rhett'  ,NULL,'1958-11-14','1990-12-20',18,'901','675-8993');
INSERT INTO employee VALUES(103,'Ms.' ,'Jones'     ,'Anne'   ,'M' ,'1974-10-16','1994-08-28', 14,'615','898-3456');
INSERT INTO employee VALUES(104,'Mr.' ,'Lange'     ,'John'   ,'p' ,'1971-11-08','1994-10-20', 14,'901','504-4430');
INSERT INTO employee VALUES(105,'Mr.' ,'Williams'  ,'Robert' ,'D' ,'1975-03-14','1998-11-08', 10,'615','890-3220');
INSERT INTO employee VALUES(106,'Mrs.','Smith'     ,'Jeanine','K' ,'1968-02-12','1989-01-05',20,'615','324-7883');
INSERT INTO employee VALUES(107,'Mr.' ,'Diante'    ,'Jorge'  ,'D' ,'1974-08-21','1994-07-02', 14,'615','890-4567');
INSERT INTO employee VALUES(108,'Mr.' ,'Wiesenbach','Paul'   ,'R' ,'1966-02-14','1992-11-18',16,'615','897-4358');
INSERT INTO employee VALUES(109,'Mr.' ,'Smith'     ,'George' ,'K' ,'1961-06-18','1989-04-14',19,'901','504-3339');
INSERT INTO employee VALUES(110,'Mrs.','Genkazi'   ,'Leighla','W' ,'1970-05-19','1990-12-01',18,'901','569-0093');
INSERT INTO employee VALUES(111,'Mr.' ,'Washington','Rupert' ,'E' ,'1966-01-03','1993-06-21', 15,'615','890-4925');
INSERT INTO employee VALUES(112,'Mr.' ,'Johnson'   ,'Edward' ,'E' ,'1961-05-14','1983-12-01',25,'615','898-4387');
INSERT INTO employee VALUES(113,'Ms.' ,'Smythe'    ,'Melanie','p' ,'1970-09-15','1999-05-11', 9,'615','324-9006');
INSERT INTO employee VALUES(114,'Ms.' ,'Brandon'   ,'Marie'  ,'G' ,'1956-11-02','1979-11-15',29,'901','882-0845');
INSERT INTO employee VALUES(115,'Mrs.','Saranda'   ,'Hermine','R' ,'1972-07-25','1993-04-23', 15,'615','324-5505');
INSERT INTO employee VALUES(116,'Mr.' ,'Smith'     ,'George' ,'A' ,'1965-11-08','1988-12-10',20,'615','890-2984');