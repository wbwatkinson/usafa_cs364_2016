
/*  Run the lab15_SaleCo_schema file */
USE cs364_lesson19;

/*  Database Systems 11e 7.7
    Joining Database Tables
*/

/* 7.7 Joining Database Tables */

SELECT  p_descript, p_price, v_name, v_contact, v_areacode, v_phone
FROM    product, vendor
WHERE   product.v_code = vendor.v_code;

/*  If you do not use the WHERE clause, the join will create a cartesian 
    product, which is not usually desired */
SELECT  p_descript, p_price, v_name, v_contact, v_areacode, v_phone
FROM    product, vendor;

/*  In general, joins do not return a consistent order of the result set
    To impose order, use the ORDER BY clause.  The example below also uses table
    names were used a prefixes.  This is normally not required unless the same 
    column name appears in both tables */
SELECT      product.p_descript, product.p_price, vendor.v_name, vendor.v_contact,
    vendor.v_areacode, vendor.v_phone
FROM        product, vendor
WHERE       product.v_code = vendor.v_code
ORDER BY    product.p_price;
   
/* Using a compound selection */
SELECT      p_descript, p_price, v_name, v_contact, v_areacode, v_phone
FROM        product, vendor
WHERE       product.v_code = vendor.v_code
AND         p_indate > '2014-01-15';

/* Joining more than one table */
SELECT      cus_lname, invoice.inv_number, inv_date, p_descript
FROM        customer, invoice, line, product
WHERE       customer.cus_code = invoice.cus_code
AND         invoice.inv_number = line.inv_number
AND         line.p_code = product.p_code
AND         customer.cus_code = 10014
ORDER BY    inv_number;

/* 7.7.1 Joining tables with an alias */

/* An alias can be used to identify the source table */
SELECT      p_descript, p_price, v_name, v_contact, v_areacode, v_phone
FROM        product p, vendor v
WHERE       p.v_code = v.v_code
ORDER BY    p_price;


/* 7.7.2 Recursive joins */

/* A table can be joined to itself */
SELECT      e.emp_num, e.emp_lname, e.emp_mgr, m.emp_lname
FROM        emp e, emp m
WHERE       e.emp_mgr = m.emp_num
ORDER BY    e.emp_mgr;
 
