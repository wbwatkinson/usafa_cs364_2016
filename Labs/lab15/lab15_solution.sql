
/*  Run the lab15_LargeCo_schema file */
USE cs364_lab15;
START TRANSACTION;

/*	1. Write a query to display the first name, last name, street, city, state,
    and zip code of any customer who purchased a Foresters Best brand top coat
    between July 15, 2013, and July 31, 2013.  If a customer purchased more than
    one such product, display the customer's information only once in the
    output.  Sort the output by state, last name, and then first name.

    Hints:
    a.  customers are linked to brands via a join series including invoice,
        line, and product
    b.  The 'Foresters Best' brand is found in the brand_name of the lgbrand
        table
    c.  The 'Top Coat' product category is found in the prod_category of the
        lgproduct table
    d.  The invoice dates are found in the inv_date of the lginvoice table
    e.  A correct solution will return 25 rows.  Here are the first 6:

    cust_fname  cust_lname  cust_street               cust_city     cust_state  cust_zip
    LUPE	      SANTANA	    1292 WEST 70TH PLACE	    Phenix City	  AL	        36867
    HOLLIS	    STILES	    1493 DOLLY MADISON CIRCLE	Snow Hill	    AL	        36778
    LISETTE	    WHITTAKER	  339 NORTHPARK DRIVE	      Montgomery	  AL	        36197
    DEANDRE	    JAMISON	    1571 HANES STREET	        Miami	        FL	        33169
    CATHLEEN	  WHITMAN	    1712 NORTHFIELD DRIVE	    Marshallville	GA	        31057
    SHERIE	    STOVER	    640 MOUNTAIN VIEW DRIVE	  Parksville	  KY	        40464
 */

SELECT  DISTINCT cust_fname, cust_lname, cust_street, cust_city, cust_state, cust_zip
FROM    lgcustomer c, lginvoice i, lgline l, lgproduct p, lgbrand b
WHERE   c.cust_code = i.cust_code
AND   	i.inv_num = l.inv_num
AND   	l.prod_sku = p.prod_sku
AND   	p.brand_id = b.brand_id
AND 	b.brand_name = 'Foresters Best'
AND		p.prod_category = 'Top Coat'
AND		i.inv_date BETWEEN '2013-07-15' AND '2013-07-31'
ORDER BY cust_state, cust_lname, cust_fname;

/*  2. Write a query to display the employee number, last name, email address,
    title, and department name of each employee whose job title ends in the word
    'ASSOCIATE'.  Sort the output by department name and employee title.
    A correct solution will return 168 rows.  Here are the first 6:

    emp_num emp_lname emp_email                 emp_title dept_name
    84526	  LASSITER	F.LASSIT8@LGCOMPANY.COM	  ASSOCIATE	ACCOUNTING
    83583	  ROLLINS	  M.ROLLIN99@LGCOMPANY.COM	ASSOCIATE	ACCOUNTING
    83378	  DUNHAM	  F.DUNHAM5@LGCOMPANY.COM	  ASSOCIATE	ACCOUNTING
    83661	  FINN	    D.FINN87@LGCOMPANY.COM	  ASSOCIATE	ACCOUNTING
    84386	  RIVERA	  D.RIVERA76@LGCOMPANY.COM	ASSOCIATE	ACCOUNTING
    83517	  ALBRIGHT	SO.ALBRI96@LGCOMPANY.COM	ASSOCIATE	ACCOUNTING
 */
 
SELECT 	e.emp_num, emp_lname, emp_email, emp_title, dept_name
FROM	lgemployee e, lgdepartment d
WHERE	e.dept_num = d.dept_num
AND		emp_title LIKE '%ASSOCIATE'
ORDER BY dept_name, emp_title;


/*  3. Write a query to display a brand name and the number of products of that
    brand that are in the database.  Sort the output by the brand name.
    A correct solution will return 9 rows:

    brand_name        numproducts
    BINDER PRIME	    27
    BUSTERS	          25
    FORESTERS BEST	  15
    HOME COMFORT	    36
    LE MODE	          36
    LONG HAUL	        41
    OLDE TYME QUALITY	27
    STUTTENFURST	    27
    VALU-MATTE	      18
 */
 
SELECT	brand_name, COUNT(prod_sku)
FROM	lgbrand b, lgproduct p
WHERE	b.brand_id = p.brand_id
GROUP BY p.brand_id
ORDER BY brand_name;

/*  4. Write a query to display the number of products in each category that
    have a water base
    A correct solution will return 5 rows:

    prod_category numproducts
    Cleaner	      2
    Filler	      2
    Primer	      16
    Sealer	      1
    Top Coat	    81
 */
 
SELECT	prod_category, COUNT(prod_sku) as numproducts
FROM	lgproduct
WHERE	prod_base = 'WATER'
GROUP BY prod_category;

/*  5. Write a query to display the number of products within each base and type
    combination. Order in decreasing order of number of products.  You will not
    need a join, but you will need two columns in your GROUP BY clause
    A correct solution will return 4 rows:

    prod_base prod_type numproducts
    Solvent	  Interior	83
    Solvent	  Exterior	67
    Water	    Interior	63
    Water	    Exterior	39
 */

SELECT	prod_base, prod_type, COUNT(prod_sku) AS numproducts
FROM	lgproduct
GROUP BY prod_base, prod_type
ORDER BY numproducts DESC;

/*  6. Write a query to display the total inventory--that is, the sum of all
    products on hand for each brand name.  Sort the output by brand name.
    A correct solution will return 9 rows:
    
    brand_name          inventory
    BINDER PRIME	      2158
    BUSTERS	            1735
    FORESTERS BEST	    1293
    HOME COMFORT	      2596
    LE MODE	            2431
    LONG HAUL	          3012
    OLDE TYME QUALITY	  2200
    STUTTENFURST	      1829
    VALU-MATTE	        1117
 */
 
SELECT	brand_name, SUM(p.prod_qoh) as inventory
FROM	lgbrand b, lgproduct p
WHERE	b.brand_id = p.brand_id
GROUP BY p.brand_id
ORDER BY brand_name;

/*  7. Write a query to display the customer code, first name, last name, and
    sum of all invoice total for customers with cumulative invoice totals
    greater than $1,500.  Sort the output by the sum of invoice totals in
    descending order.
    A correct answer will return 43 rows.  Here are the first 6:
    
    cust_code cust_fname  cust_lname  total
    215	      CHARMAINE	  BRYAN	      3134.15
    98	      VALENTIN	  MARINO	    3052.46
    152	      LISETTE	    WHITTAKER	  3042.78
    117	      KARON	      MATA	      3009.63
    97	      ERWIN	      ANDERSON	  2895.49
    112	      LAN	        NICHOLS	    2867.14
 */

SELECT	c.cust_code, cust_fname, cust_lname, SUM(inv_total) AS total
FROM	lgcustomer c, lginvoice i
WHERE	c.cust_code = i.cust_code
GROUP BY cust_code
HAVING	total > 1500
ORDER BY total DESC;

/*  8. Write a query to display the department number, department name,
	department phone number, employee number, and last name of each department
    manager.  Sort the output by department name.
    A correct answer will return 8 rows:
    
    dept_num  dept_name           dept_phone  emp_num emp_lname
    600	      ACCOUNTING	        555-2333	  84583   YAZZIE
    250	      CUSTOMER SERVICE	  555-5555	  84001	  FARMER
    500	      DISTRIBUTION	      555-3624	  84052	  FORD
    280	      MARKETING	          555-8500	  84042	  PETTIT
    300	      PURCHASING	        555-4873	  83746	  RANKIN
    200	      SALES	              555-2824	  83509	  STOVER
    550	      TRUCKING	          555-0057	  83683	  STONE
    400	      WAREHOUSE	          555-1003	  83759	  CHARLES
 */

SELECT	d.dept_num, dept_name, dept_phone, d.emp_num, emp_lname
FROM	lgemployee e, lgdepartment d
WHERE	e.emp_num = d.emp_num
ORDER BY dept_name;

/*	9. Write a query to display every employee's last name, first name,
	and manager's name (the manager is the employee assigned to the department
	A correct answer will return 363 rows.  The first 6 are below:

	emp_lname emp_fname   manager
	ABERNATHY	LOURDES	    STOVER
    ACEVEDO	  LAVINA	    RANKIN
    ALBRIGHT	SAMANTHA	  STONE
    ALBRIGHT	SONDRA	    YAZZIE
    ALDRICH	  ARMAND	    PETTIT
    ALVARADO	LELIA	      CHARLES
*/

SELECT	e.emp_lname, e.emp_fname, e2.emp_lname as manager
FROM	lgemployee e, lgdepartment d, lgemployee e2
WHERE	e.dept_num = d.dept_num
AND		d.emp_num = e2.emp_num
ORDER BY e.emp_lname, e.emp_fname;

/*  10. Write a query to display every employee's last name, first name, title
    and current salary.  Hint: the current salary is the value of salary_amount
    in the lsalargy_history record having the a NULL value in the sal_end field
    for that employee.  Sort the result by salary in descending order.
    A correct answer will return 363 rows.  The first 6 are below:
    
    emp_lname   emp_fname   emp_title             sal_amount
    STOVER	    FRANKLYN	  SENIOR SALES MANAGER  210000.00
    YAZZIE	    LINDSEY	    SENIOR ACCOUNTANT	    164700.00
    NICHOLS	    ALVA	      SENIOR ACCOUNTANT	    154800.00
    MIRANDA	    EVETTE	    SENIOR ACCOUNTANT	    148500.00
    BARR	      JOSE	      SALES MANAGER	        147000.00
    ENGLISH	    CLEO	      SENIOR ASSOCIATE	    136000.00
 */
 
SELECT	emp_lname, emp_fname, emp_title, sal_amount
FROM	lgemployee e, lgsalary_history s
WHERE	e.emp_num = s.emp_num
AND		s.sal_end IS NULL
ORDER BY sal_amount DESC;


COMMIT;