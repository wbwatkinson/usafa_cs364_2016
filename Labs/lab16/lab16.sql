USE cs364_lab16;

START TRANSACTION;

/*  1.	Write a query to display the products that have a price greater than
    $50.  A correct answer will have 3 records:

    prod_sku	prod_descript prod_type	prod_base	prod_category	prod_price	prod_qoh	prod_min	brand_id
    1021-MTI	"Elastomeric, Exterior, Industrial Grade, Water Based"	Exterior	Water	"Top Coat"	62.99	22	25	35
    3694-XFJ	"Epoxy-Modified Latex, Interior, Semi-Gloss (MPI Gloss Level 5)"	Interior	Water	"Top Coat"	54.89	39	25	27
    1964-OUT	"Fire Resistant Top Coat, for Interior Wood"	Interior	Solvent	"Top Coat"	78.49	120	10	30
  */


/*  2. Write a query to display the starting salary for each employee. The
    starting salary would be the entry in the salary history with the oldest
    salary start date for each employee. Sort the output by employee number. A
    correct answer will have 363 records.  The first 3 are below:

    emp_num   emp_lname   emp_fname   sal_amount
    83304	    MCDONALD	  TAMARA	    19770.00
    83308	    LOVE	      CONNIE	    11230.00
    83312	    BAKER	      ROSALBA	    39260.00

 */

/*  3. Write a query to display the invoice number, line numbers, product
    SKUs, product descriptions, and brand ID for sales of sealer and top coat
    products of the same brand on the same invoice. A correct answer will have
    130 rows.  Several of the rows are displayed below. Hint: you might consider
    showing the prod_category for each product in order to confirm it belongs
    in the list (then remove it for your final answer).

    inv_num line_num prod_sku prod_descript line_num prod_sku prod_descript brand_id
    115	2	5140-RTG	Fire Resistant Sealer, for Exterior Wood (ULC Approved)	1	1203-AIS	Fire Retardant Coating, Latex, Interior, Flat (ULC Approved)	35
    118	2	5140-RTG	Fire Resistant Sealer, for Exterior Wood (ULC Approved)	5	5046-TTC	Aluminum Paint, Heat Resistant (Up to 427°C - 800°F)	35
    135	5	3036-PCT	Sealer, for Knots	2	1074-VVJ	Light Industrial Coating, Exterior, Water Based (eggshell-like - MPI Gloss Level 3) 	25

 */

/*  4. The Binder Prime Company wants to recognize the employee who sold the
    most of their products during a specified period. Write a query to display
    the employee number, employee first name, employee last name, e-mail
    address, and total units sold for the employee who sold the most Binder
    Prime brand products between November 1, 2013, and December 5, 2013. If
    there is a tie for most units sold, sort the output by employee last name. A
    correct answer will have two records:

    emp_num emp_fname emp_lname emp_email                 total
    83850	  RUSTY	    MILES	    M.RUSTY95@LGCOMPANY.COM	  23
    84134	  ROSALIE	  GARLAND	  G.ROSALI98@LGCOMPANY.COM  23
 */


/*  5. The purchasing manager is concerned about the impact of price on
    sales. Write a query to display the brand name, brand type, product SKU,
    product description, and price of any products that are not a premium brand,
    but that cost more than the most expensive premium brand products. A correct
    answer will return one record:

    brand_name  brand_type  prod_sku  prod_descript                               prod_price
    LONG HAUL	  CONTRACTOR	1964-OUT	Fire Resistant Top Coat, for Interior Wood	78.49
 */


ROLLBACK;