
/*  Run the lab16_SaleCo_schema file */
USE cs364_lesson20;

/* 8.1 SQL Join Operations */

/* 8.1.1 Cross Join */

/* A cross join is equivalent to a cartesian product of two tables */
SELECT  *
FROM    invoice
        CROSS JOIN line;

SELECT  i.inv_number, cus_code, inv_date, p_code
FROM    invoice i
        CROSS JOIN line;

/* A cartesian product can also be achieved with explicitly using CROSS JOIN */
SELECT  i.inv_number, cus_code, inv_date, p_code
FROM    invoice i, line;

/* 8.1.2 NATURAL JOIN */

/*  The use of NATURAL JOIN is discouraged in practice, as it
    (1) does not document the nature of the join condition
    (2) makes the code more difficult to maintain without
    (3) requires a DBMS to "guess" how to join tables
    (4) could cause changes to column names to break SQL commands from working
        in the future
 */
/*  A natural join returns all rows with matching values in matching columns
    and eliminates duplicate columns
 */

SELECT  cus_code, cus_lname, inv_number, inv_date
FROM    customer
        NATURAL JOIN invoice;

/*  A NATURAL JOIN of more than one table */
SELECT  inv_number, p_code, p_descript, line_units, line_price
FROM    invoice
        NATURAL JOIN line
        NATURAL JOIN product;

/* 8.1.3 JOIN USING clause */

SELECT  inv_number, p_code, p_descript, line_units, line_price
FROM    invoice
        JOIN line USING (inv_number)
        JOIN product USING (p_code);

/* 8.1.4 JOIN ON clause */

SELECT  i.inv_number, p.p_code, p_descript, line_units, line_price
FROM    invoice i
        JOIN line l ON i.inv_number=l.inv_number
        JOIN product p ON l.p_code = p.p_code;

/*  The advantage of JOIN ON is that you can join tables even if they don't have
    a commonly named attribute */
SELECT  e.emp_mgr, m.emp_lname, e.emp_num, e.emp_lname
FROM    emp e
        JOIN EMP m ON e.emp_mgr = m.emp_num
ORDER BY e.emp_mgr;

/* 8.1.5 OUTER JOIN */

/*  Display all rows from left table, but only the corresponding rows from the
    right table */
SELECT  p_code, v.v_code, v_name
FROM    vendor v
        LEFT JOIN product p ON v.v_code = p.v_code;

/*  Display all rows from right table, but only corresponding rows from the
    left table */
SELECT  p_code, v.v_code, v_name
FROM    vendor v
        RIGHT JOIN product p ON v.v_code = p.v_code;

/* 8.2 Subqueries and Correlated Queries */

/*  Display a list of vendors who do not provide products.  The subquery
    identifies all vendors that provide products */
SELECT  v_code, v_name
FROM    vendor
WHERE   v_code NOT IN (SELECT v_code FROM product);

/*  Display a list of products whose price is greater than or equal to the
    average price of all products.  The subquery finds the average price of
    all products */
SELECT  p_code, p_price
FROM    product
WHERE   p_price >= (SELECT AVG(p_price) FROM product);

START TRANSACTION;

UPDATE product
SET p_price = (SELECT AVG(p_price))
WHERE v_code IN (SELECT v_code
                 FROM vendor
                 WHERE v_areacode = '615');

DELETE  FROM product
WHERE   v_code IN (SELECT v_code FROM vendor WHERE v_areacode = '615');

SELECT * FROM product;

ROLLBACK;

/* 8.2.1 WHERE Subqueries */

/*  Find all products with a price greater than or equal to th average product
    price */
SELECT  p_code, p_price
FROM    product
WHERE   p_price >= (SELECT AVG(p_price) FROM product);

/* List all customers who ordered a claw hammer */
SELECT  DISTINCT cus_code, cus_lname, cus_fname
FROM    customer  JOIN invoice USING (cus_code)
                  JOIN line USING (inv_number)
                  JOIN product USING (p_code)
WHERE   p_code = (SELECT p_code FROM product WHERE p_descript = 'Claw hammer');

/*  The following query has the same effect as the previous one.  In the
    previous query, the subquery found all product codes where the description
    was 'Claw hammer'.  The product codes of the joined table are compared
    to the result of this subquery.  If the subquery retruns more than one
    product code this query will result in an error.  The query below will
    simply returns those records having 'Claw hammer' in the product description.
 */
SELECT  DISTINCT cus_code, cus_lname, cus_fname
FROM    customer  JOIN invoice USING (cus_code)
                  JOIN line USING (inv_number)
                  JOIN product USING (p_code)
WHERE   p_descript = 'Claw hammer';

/*  8.2.2 IN Subqueries */

/* List all customers who have purchased hammers, saws, or saw blades */
SELECT  DISTINCT cus_code, cus_lname, cus_fname
FROM    customer  JOIN invoice USING (cus_code)
                  JOIN line USING (inv_number)
                  JOIN product USING (p_code)
WHERE   p_code IN (SELECT p_code FROM product
                   WHERE p_descript LIKE '%hammer%'
                   OR p_descript LIKE '%saw%');

/*  8.2.3 HAVING Subqueries */

/*  List all products with a total quantity sold greater than the average
    quantity sold */
SELECT    p_code, SUM(line_units)
FROM      line
GROUP BY  p_code
HAVING    SUM(line_units) > (SELECT AVG(line_units) FROM line);

/*  8.2.4 Multirow Subquery operators: ANY and ALL */

/*  List the products that cost more than all individual products provided by
    vendors from Florida.  The inner most subquery returns all v_codes for
    vendors in Florida.  The next subquery returns the inventory price of
    each product from a vendor based out of Florida.  The outermost query
    will list the product code and the inventory value of all products whose
    inventory price is greater than the inventory value of all products from
    Florida */

SELECT  p_code, p_qoh*p_price
FROM    product
WHERE   p_qoh * p_price > ALL (SELECT p_qoh * p_price
                               FROM    product
                               WHERE   v_code IN (SELECT v_code
                                                  FROM vendor
                                                  WHERE v_state= 'FL'));

/*  8.2.5 FROM Subqueries */

/*  List all customers who purchased both products 13-Q2/PS and 23109-HB.  The
    FROM clause uses 3 tables.  The first table is customer, the second table
    includes customer codes of customers who purchased product code '13-Q2/P2,
    and the third table includes customers who purchased product code
    '23109-HB'*/
SELECT  DISTINCT c.cus_code, c.cus_lname
FROM    customer c,
        (SELECT invoice.cus_code
         FROM   invoice JOIN line USING (inv_number)
         WHERE  p_code = '13-Q2/P2') cp1,
        (SELECT invoice.cus_code
         FROM   invoice JOIN line USING (inv_numbeR)
         WHERE  p_code = '23109-HB') cp2
WHERE   c.cus_code = cp1.cus_code AND cp1.cus_code = cp2.cus_code;

/*  8.2.6 Attribute List Subqueries */

/*  Show each the difference between each product's price and the average
    price */
SELECT  p_code, p_price, (SELECT AVG(p_price) FROM product) AS avgprice,
        p_price - (SELECT AVG(p_price) FROM product) AS diff
FROM    product;

/*  Display the product code, total sales by product, and the contribution by
    employee of each product's sales */
SELECT    p_code, SUM(line_units * line_price) AS sales,
          (SELECT COUNT(*) FROM employee) AS ecount,
          SUM(line_units * line_price)/(SELECT COUNT(*) FROM employee) as contrib
FROM      line
GROUP BY  p_code;

/* Alternatively */
SELECT  p_code, sales, ecount, sales/ecount AS contrib
FROM    (SELECT p_code, SUM(line_units * line_price) AS sales,
           (SELECT COUNT(*) FROM employee) AS ecount
        FROM      LINE
        GROUP BY  p_code);

/*  8.2.7 Correlated subqueries */

/*  A correlated subquery executes once for each line in the outer query */

/*  List all product sales in which the units sold value is greater than average
    units sold value for that product.  The inner query calculates the
    average value for a particular product.  The outer query selects the
    products where the units solds is greater than the average */
SELECT  inv_number, p_code, line_units
FROM    line ls
WHERE   ls.line_units > (SELECT AVG(line_units)
                         FROM line la JOIN ls USING (p_code));

/*  You can verify the accuracy of the above query by outputting the average
    units sold for each product */
SELECT  inv_number, p_code, line_units,
        (SELECT AVG(line_units) FROM line lx WHERE lx.p_code = ls.pcode) AS avg
FROM    line ls
WHERE   ls.line_units > (SELECT AVG(line_units)
                         FROM line la
                         WHERE la.p_code = ls.p_code);

/* List all the names of all customers who have recently placed an order */
SELECT  cus_code, cus_lname, cus_fname
FROM    customer c
WHERE   EXISTS  (SELECT cus_code FROM invoice JOIN ON c (cus_code));

/*  List the vendors you must contact to products that are approaching the
    minimum quantity-on-hand value */
SELECT  v_code, v_name
FROM    vendor v
WHERE   EXISTS  (SELECT *
                 FROM   product p
                 WHERE  p_qoh < p_min * 2
                 AND    v.v_code = p.v_code);

