USE cs364_lab16;

START TRANSACTION;

/*  1.	Write a query to display the products that have a price greater than
    $50.  A correct answer will have 3 records:

    prod_sku	prod_descript prod_type	prod_base	prod_category	prod_price	prod_qoh	prod_min	brand_id
    1021-MTI	"Elastomeric, Exterior, Industrial Grade, Water Based"	Exterior	Water	"Top Coat"	62.99	22	25	35
    3694-XFJ	"Epoxy-Modified Latex, Interior, Semi-Gloss (MPI Gloss Level 5)"	Interior	Water	"Top Coat"	54.89	39	25	27
    1964-OUT	"Fire Resistant Top Coat, for Interior Wood"	Interior	Solvent	"Top Coat"	78.49	120	10	30
  */

SELECT *
FROM lgproduct
WHERE prod_price > 50;

/*  2. Write a query to display the starting salary for each employee. The
    starting salary would be the entry in the salary history with the oldest
    salary start date for each employee. Sort the output by employee number. A
    correct answer will have 363 records.  The first 3 are below:

    emp_num   emp_lname   emp_fname   sal_amount
    83304	    MCDONALD	  TAMARA	    19770.00
    83308	    LOVE	      CONNIE	    11230.00
    83312	    BAKER	      ROSALBA	    39260.00

 */

SELECT  e.emp_num, emp_lname, emp_fname, sal_amount
FROM    lgemployee e JOIN lgsalary_history s ON e.emp_num = s.emp_num
WHERE   sal_from = (SELECT Min(sal_from)
                  FROM lgsalary_history s2
                  WHERE e.emp_num = s2.emp_num)
ORDER BY e.emp_num;


/*  3. Write a query to display the invoice number, line numbers, product
    SKUs, product descriptions, and brand ID for sales of sealer and top coat
    products of the same brand on the same invoice. A correct answer will have
    130 rows.  Several of the rows are displayed below. Hint: you might consider
    showing the prod_category for each product in order to confirm it belongs
    in the list (then remove it for your final answer).

    inv_num line_num prod_sku prod_descript line_num prod_sku prod_descript brand_id
    115	2	5140-RTG	Fire Resistant Sealer, for Exterior Wood (ULC Approved)	1	1203-AIS	Fire Retardant Coating, Latex, Interior, Flat (ULC Approved)	35
    118	2	5140-RTG	Fire Resistant Sealer, for Exterior Wood (ULC Approved)	5	5046-TTC	Aluminum Paint, Heat Resistant (Up to 427°C - 800°F)	35
    135	5	3036-PCT	Sealer, for Knots	2	1074-VVJ	Light Industrial Coating, Exterior, Water Based (eggshell-like - MPI Gloss Level 3) 	25

 */

SELECT  l.inv_num, l.line_num, p.prod_sku, p.prod_descript, l2.line_num, p2.prod_sku, p2.prod_descript, p.brand_id
FROM    (lgline l JOIN lgproduct p ON l.prod_sku = p.prod_sku) JOIN
        (lgline l2 JOIN lgproduct p2 ON l2.prod_sku = p2.prod_sku)
        ON l.inv_num = l2.inv_num
WHERE   p.brand_id = p2.brand_id
        AND p.prod_category = 'Sealer'
        AND p2.prod_category = 'Top Coat'
ORDER BY l.inv_num, l.line_num;


/*	Alternative approach is to create query that returns the Sealers and the query that returns the Top Coats.
	Then, these two result sets can be joined on the inv_num. */
SELECT 	r1.inv_num, r1.line_num, r1.prod_sku, r1.prod_descript, r2.line_num, r2.prod_sku, r2.prod_descript, r1.brand_id
FROM	(SELECT	l.inv_num, l.line_num, p.prod_sku, p.prod_descript, p.brand_id
		 FROM	lgline l JOIN lgproduct p ON l.prod_sku = p.prod_sku
		 WHERE	p.prod_category = 'Sealer') AS r1 JOIN
		(SELECT	l.inv_num, l.line_num, p.prod_sku, p.prod_descript, p.brand_id
		 FROM	lgline l JOIN lgproduct p ON l.prod_sku = p.prod_sku
		 WHERE	p.prod_category = 'Top Coat') AS r2 ON r1.inv_num = r2.inv_num
WHERE	r1.brand_id = r2.brand_id
ORDER BY r1.inv_num, r1.line_num;

/*  4. The Binder Prime Company wants to recognize the employee who sold the
    most of their products during a specified period. Write a query to display
    the employee number, employee first name, employee last name, e-mail
    address, and total units sold for the employee who sold the most Binder
    Prime brand products between November 1, 2013, and December 5, 2013. If
    there is a tie for most units sold, sort the output by employee last name. A
    correct answer will have two records:

    emp_num emp_fname emp_lname emp_email                 total
    83850	  RUSTY	    MILES	    M.RUSTY95@LGCOMPANY.COM	  23
    84134	  ROSALIE	  GARLAND	  G.ROSALI98@LGCOMPANY.COM  23
 */

/* 	Solution development */
/* 	Step 1: Create a table listing employee ids and the total Binder Prime products the 
	employee sold */
SELECT employee_id, Sum(line_qty) AS total
   FROM lginvoice i JOIN lgline l ON i.inv_num = l.inv_num
     JOIN lgproduct p ON l.prod_sku = p.prod_sku
     JOIN lgbrand b ON b.brand_id = p.brand_id
   WHERE brand_name = 'BINDER PRIME'
         AND INV_DATE BETWEEN '2013-11-01' AND '2013-12-05'
   GROUP BY employee_id;

/*	Step 2: Modify the query above, joining the result set
	this table to the employee table, including requisite data */
SELECT emp.emp_num, emp_fname, emp_lname, emp_email, total
FROM lgemployee emp JOIN
  (SELECT employee_id, Sum(line_qty) AS total
   FROM lginvoice i JOIN lgline l ON i.inv_num = l.inv_num
     JOIN lgproduct p ON l.prod_sku = p.prod_sku
     JOIN lgbrand b ON b.brand_id = p.brand_id
   WHERE brand_name = 'BINDER PRIME'
         AND INV_DATE BETWEEN '2013-11-01' AND '2013-12-05'
   GROUP BY employee_id) sub
    ON emp.emp_num = sub.employee_id;
    
/*	Step 3: Modify the above table to show only those records
	where the total is equal to the max of all totals */    
SELECT emp.emp_num, emp_fname, emp_lname, emp_email, total
FROM lgemployee emp JOIN
  (SELECT employee_id, Sum(line_qty) AS total
   FROM lginvoice i JOIN lgline l ON i.inv_num = l.inv_num
     JOIN lgproduct p ON l.prod_sku = p.prod_sku
     JOIN lgbrand b ON b.brand_id = p.brand_id
   WHERE brand_name = 'BINDER PRIME'
         AND INV_DATE BETWEEN '2013-11-01' AND '2013-12-05'
   GROUP BY employee_id) sub
    ON emp.emp_num = sub.employee_id
			WHERE total = (SELECT Max(total)
               FROM (SELECT employee_id, Sum(line_qty) AS total
                     FROM lginvoice i JOIN lgline l ON i.inv_num = l.inv_num
                       JOIN lgproduct p ON l.prod_sku = p.prod_sku
                       JOIN lgbrand b ON b.brand_id = p.brand_id
                     WHERE brand_name = 'BINDER PRIME'
                           AND INV_DATE BETWEEN '2013-11-01' AND '2013-12-05'
                     GROUP BY employee_id) as temp);
                     



/*  5. The purchasing manager is concerned about the impact of price on
    sales. Write a query to display the brand name, brand type, product SKU,
    product description, and price of any products that are not a premium brand,
    but that cost more than the most expensive premium brand products. A correct
    answer will return one record:

    brand_name  brand_type  prod_sku  prod_descript                               prod_price
    LONG HAUL	  CONTRACTOR	1964-OUT	Fire Resistant Top Coat, for Interior Wood	78.49
 */

/*	To visualize what we'r trying to display, it would be helpful to see all the premium
	and non-premium product prices */
SELECT brand_name, brand_type, prod_sku, prod_descript, prod_price
FROM lgproduct p JOIN lgbrand b ON p.brand_id = b.brand_id
WHERE brand_type = 'PREMIUM'
ORDER BY prod_price DESC;

SELECT brand_name, brand_type, prod_sku, prod_descript, prod_price
FROM lgproduct p JOIN lgbrand b ON p.brand_id = b.brand_id
WHERE brand_type <> 'PREMIUM'
ORDER BY prod_price DESC;

/*	We see from the results of this query, there is only one non-premium product 
	that is more expensive than all the premium products.  This query will select
    all products that are not premium whose price is more than the maximum price
    of the premium products*/

SELECT brand_name, brand_type, prod_sku, prod_descript, prod_price
FROM lgproduct p JOIN lgbrand b ON p.brand_id = b.brand_id
WHERE brand_type <> 'PREMIUM'
      AND prod_price > (SELECT Max(prod_price)
                        FROM lgproduct p JOIN lgbrand b ON p.brand_id = b.brand_id
                        WHERE brand_type = 'PREMIUM');


ROLLBACK;