/******************************************************************************
 Your Name,  Assignment Name
 CS364, Information Storage and Retrieval, Spring 2016
 Instructor: Lt Col Blair Watkinson

 Documentation:
 Your detailed documentation statement here

 Observations, Feedback, and questions:
 Any feedback or questions to the instructor here
 ******************************************************************************/

"use strict";



  var app = angular.module('inventory', []);

  app.controller('InventoryController', function() {
    this.aircrafts = planes;
  });

  app.controller('PanelController', function() {
    this.tab = 1;

    this.selectTab = function(setTab) {
      this.tab = setTab;
    };

    this.isSelected = function(checkTab) {
      return this.tab === checkTab;
    };
  });

  app.controller('OrderController', function() {
    this.attack = {};

    this.addAttack = function(aircraft) {
      aircraft.attacks.push(this.attack);
      this.attack = {};
      //this.$setPristine();
    };
  });

  var planes = [
    {
      name: "A-10",
      nickname: "Thunderbolt II",
      description: "The Fairchild Republic A-10 Thunderbolt II is an American twin-engine, straight wing jet aircraft developed by Fairchild-Republic in the early 1970s. It entered service in 1976, and is the only United States Air Force production-built aircraft designed solely for close air support, including attacking tanks, armored vehicles, and other ground targets.",
      firstflight: "1972-05-10",
      cost: 18800000,
      speed: "450 nm",
      ceiling: "45,000 ft",
      takeoffweight: "51,000 pounds",
      range: "2240 nm",
      armament: "one 30 mm GAU-8/A seven-barrel Gatling gun; up to 16,000 pounds of mixed ordnance on eight under-wing and three under-fuselage pylon stations",
      image: "images/450px-A-10_Thunderbolt_II_In-flight-2.jpg",
      attacks: []
    },
    {
      name: "AC-130",
      nickname: "Spectre",
      description: "The Lockheed AC-130 gunship is a heavily armed, long-endurance ground-attack variant of the C-130 Hercules transport aircraft. It carries a wide array of anti-ground oriented weapons that are integrated with sophisticated sensors, navigation, and fire control systems. Unlike other military fixed-wing aircraft, the AC-130 relies on visual targeting. Because its large profile and low operating altitudes (around 7,000 ft) make it an easy target, it usually flies close air support missions at night.",
      firstflight: "1996-01-17",
      cost: 132400000,
      speed: "300 mph",
      ceiling: "25,000 ft",
      takeoffweight: "155,000 pounds",
      range: "1,300 nm",
      armament: "40mm, 105mm cannons and 25mm gatling gun",
      image: "images/450px-AC-130H_Spectre_jettisons_flares.jpg",
      attacks: [{count:1,target:"Train bridge"}]
    },
    {
      name: "F-22",
      nickname: "Raptor",
      description: "The Lockheed Martin F-22 Raptor is a fifth-generation single-seat, twin-engine, all-weather stealth tactical fighter aircraft developed for the United States Air Force (USAF). The result of the USAF's Advanced Tactical Fighter program, the aircraft was designed primarily as an air superiority fighter, but has additional capabilities including ground attack, electronic warfare, and signals intelligence roles.[6] Lockheed Martin is the prime contractor and was responsible for the majority of the airframe, weapon systems, and final assembly of the F-22, while program partner Boeing provided the wings, aft fuselage, avionics integration, and training systems.",
      firstflight: "1997-09-07",
      cost: 150000000,
      speed: "2M",
      ceiling: "above 50,000 ft",
      takeoffweight: "83,5000 pounds",
      range: "1,600 nm",
      armament: "one M61A2 20-millimeter cannon with 480 rounds, internal side weapon bays carriage of two AIM-9 infrared (heat seeking) air-to-air missiles and internal main weapon bays carriage of six AIM-120 radar-guided air-to-air missiles (air-to-air loadout) or two 1,000-pound GBU-32 JDAMs and two AIM-120 radar-guided air-to-air missiles (air-to-ground loadout)",
      image: "images/450px-Lockheed_Martin_F-22A_Raptor_JSOH.jpg",
      attacks: [{count:4,target:"Weapons depot"},
        {count:2,target:"Fuel storages"}]
    }];

