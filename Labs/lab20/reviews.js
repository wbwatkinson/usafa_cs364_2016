db = db.getSiblingDB('reviews');
db.dropDatabase();
db = db.getSiblingDB('reviews');

db.potions.insert(
  {
    "name": "Invisibility",
    "vendor": "Kettlecooked",
    "price": 10.99,
    "score": 59,
    "tryDate": new Date(2012, 8, 13),
    "ingredients": ["newt toes", 42, "laughter"],
    "ratings": {"strength": 5, "flavor": 5},
    "color": "black",
    "categories": ["tasty", "budget"],
    "sizes": [34,64,80]
  }
)

db.potions.insert(
  {
    "name":"Shrinking",
    "vendor":"Kettlecooked",
    "price":8.99,
    "score":94,
    "ingredients": ["hippo", "secret", "mouse feet"],
    "ratings": {"flavor":2,"strength":3},
    "color": "green",
    "categories": ["tasty", "effective"],
    "sizes": [32,64,112]
  }
)

db.potions.insert(
  {
    "name":"Love",
    "vendor":"Brewers",
    "price":40.99,
    "score":84,
    "ratings": {"flavor":4, "strength":5},
    "color": "red",
    "categories": ["effective", "budget"],
    "sizes": [2,8,16]
  }
)

db.potions.insert(
  {
    "name":"Luck",
    "vendor":"Brewers",
    "price":25.00,
    "score":72,
    "ratings": {"flavor":5, "strength":3},
    "color": "red",
    "categories": ["tasty", "budget"],
    "sizes": [2,8,16]
  }
)

db.wands.insert(
  {
    "_id": ObjectId('f01a4524a6de4c8f98d4e8bf'),
    "name": "Journey End",
    "creator": "Foxmond",
    "level_required": 1,
    "price": 4.99,
    "powers": [
      "Molten",
      "Fire"
    ],
    "damage": {
      "magic": 0.7,
      "melee": 0.6
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('56098775627f32c737665997'),
    "name": "Malodorous",
    "maker": "Death",
    "level_required": 66,
    "price": 66.6,
    "powers": [
      "Fire",
      "Hugs"
    ],
    "damage": {
      "magic": 4,
      "melee": 2
    },
    "smell": true
  }
)

db.wands.insert(
  {
    "_id": ObjectId('0d2b33b9fd424edc8aef5747'),
    "name": "Windsong",
    "creator": "Foxmond",
    "level_required": 80,
    "price": 4.99,
    "powers": [
      "Wind",
      "Serenity"
    ],
    "damage": {
      "magic": 9.3,
      "melee": 6.1
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('5605463508ab03e5d899234c'),
    "name": "Doom Bringer",
    "creator": "Foxmond",
    "level_required": 100,
    "price": 99.99,
    "powers": [
      "Death",
      "Destruction"
    ],
    "damage": {
      "magic": 10,
      "melee": 10
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('2db8f9de713b486cb6196340'),
    "name": "Destiny Fire",
    "creator": "Sageseer",
    "level_required": 2,
    "price": 24.99,
    "powers": [
      "Fire",
      "Spark"
    ],
    "damage": {
      "magic": 2,
      "melee": 2.6
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('46d9fc0c020a415e8c1f6df9'),
    "name": "Omega",
    "creator": "Pinebriar",
    "level_required": 25,
    "price": 4.99,
    "powers": [
      "Mind",
      "Reason"
    ],
    "damage": {
      "magic": 2.5,
      "melee": 2.5
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('560820aa627f32c73766598e'),
    "name": "Pestilential",
    "maker": "Death",
    "level_required": 77,
    "price": 77.7,
    "powers": [
      "Fire",
      "Spell Casting"
    ],
    "damage": {
      "magic": 4,
      "melee": 2
    },
    "smell": true
  }
)

db.wands.insert(
  {
    "_id": ObjectId('5609e753bd62caa811c9abbd'),
    "name": "Riddle Bolt",
    "creator": "Moonsap",
    "level_required": 5,
    "price": 30.99,
    "powers": [
      "Earth",
      "Isolate"
    ],
    "damage": {
      "magic": 1.4,
      "melee": 0.4
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('5605400508ab03e5d8992349'),
    "name": "Death Bar",
    "creator": "Foxmond",
    "level_required": 100,
    "price": 99.99,
    "powers": [
      "Death",
      "Despair"
    ],
    "damage": {
      "magic": 10,
      "melee": 10
    }
  }
);
db.wands.insert(
  {
    "_id": ObjectId('92952c3a0ba344e8a87fcfb5'),
    "name": "World Shaper",
    "creator": "Olivemist",
    "level_required": 24,
    "price": 54.99,
    "powers": [
      "Earth",
      "Arcane"
    ],
    "damage": {
      "magic": 4.4,
      "melee": 3.2
    }
  }
);
db.wands.insert(
  {
    "_id": ObjectId('5605404d08ab03e5d899234b'),
    "name": "Widow Maker",
    "creator": "Foxmond",
    "level_required": 100,
    "price": 99.99,
    "powers": [
      "Death",
      "Horror"
    ],
    "damage": {
      "magic": 10,
      "melee": 10
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('c2b806d6edda4071839e4103'),
    "name": "Dream Caller",
    "creator": "Sageseer",
    "level_required": 74,
    "price": 64.95,
    "powers": [
      "Vision",
      "Mind"
    ],
    "damage": {
      "magic": 6.8,
      "melee": 3.4
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('6ff0dbb55e344d51a641c5bd'),
    "name": "Dream Bender",
    "maker": "Foxmond",
    "level_required": 10,
    "price": 34.9,
    "powers": [
      "Fire",
      "Love"],
    "damage": {
      "magic": 4,
      "melee": 2
    }
  }
)

db.wands.insert(
  {
    "_id": ObjectId('7aa48b7bccb84f29bdc859b4'),
    "name": "Cometfell",
    "creator": "Moonsap",
    "level_required": 10,
    "price": 150.95,
    "powers": [
      "Solar",
      "Love"
    ],
    "damage": {
      "magic": 1.7,
      "melee": 2.3
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('fc25d06ad017449e8c31141a'),
    "name": "Life Mender",
    "creator": "Pinebriar",
    "level_required": 51,
    "price": 4.99,
    "powers": [
      "Healing",
      "Blessing"
    ],
    "damage": {
      "magic": 7,
      "melee": 1
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('7e9aac326a2146cba479ae7d'),
    "name": "Spirit Spire",
    "creator": "Pinebriar",
    "level_required": 15,
    "price": 4.99,
    "powers": [
      "Air",
      "Restoration"
    ],
    "damage": {
      "magic": 1.4,
      "melee": 3
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('d0a77e57a0544ec7ad5a740b'),
    "name": "Storm Seeker",
    "creator": "Olivemist",
    "level_required": 96,
    "price": 55.99,
    "powers": [
      "Wind",
      "Static"
    ],
    "damage": {
      "magic": 2,
      "melee": 5
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('5404bf9eb971425e86832c93'),
    "name": "Sacred Sun",
    "creator": "Moonsap",
    "level_required": 13,
    "price": 33.99,
    "powers": [
      "Solar",
      "Bless"
    ],
    "damage": {
      "magic": 1.7,
      "melee": 2.3
    }
  }
);

db.wands.insert(
  {
    "_id": ObjectId('8bb40641abc24eba99505f3a'),
    "name": "Devotion Shift",
    "creator": "Pinebriar",
    "level_required": 2,
    "price": 34.99,
    "powers": [
      "Visions",
      "Soul"
    ],
    "damage": {
      "magic": 5,
      "melee": 1.7
    }
  }
);


db.logs.insert(
  {
    "name": "Power Log",
    "count": 2,
    "_id": "b3ad98a74b264f2d8d1d10772453b505"
  }
)

db.logs.insert(
  {
    "name": "Snow Log",
    "count": 44,
    "_id": "ca34d0ff29a44fc0bc72a3d337ad97ce"
  }
)

db.logs.insert(
  {
    "name": "Globe Weaver",
    "logs": 23,
    "_id": "2496868f7347480ba46c59f6dc014ed9"
  }
)

db.logs.insert(
  {
    "name": "Coyote Log",
    "logs": 11,
    "_id": "8469fef7eb6f4cc29d79057d3689b5fc"
  }
)

db.logs.insert(
  {
    "name": "Marble Log",
    "logs": 66,
    "_id": "691ea4586aa24c0b8ee3be7f0475931c"
  }
)
