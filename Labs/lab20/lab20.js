/******************************************************************************
 Your Name,  Lab 20
 CS364, Information Storage and Retrieval, Spring 2016
 Instructor: Lt Col Blair Watkinson

 Documentation:
 Your detailed documentation statement here

 Observations, Feedback, and questions:
 Any feedback or questions to the instructor here
 ******************************************************************************/

// reset reviews database
load('reviews.js')

//  1. Remove a single potion. Remove the potion named Love.
db.potions.remove(
  {
      "name": "Love"
  }
)
//  2. Remove multiple potions. Remove all potions whose vendor is Kettlecooked.
db.potions.remove(
  {
      "vendor": "Kettlecooked"
  }
)

// reset reviews database
load('reviews.js')

//  3. Update an existing potion. Change the price of the Love potion to 3.99
db.potions.update(
  {"name": "Love"},
  {"$set": {"price":3.99}}
)
db.potions.find({"name": "Love"})

/*  Update operators always begin with a $. This update will only be applied
 to the first matching document

 The WriteResult summarizes what update() did:
 WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 }) */

//  4. Neglecting the update operator (e.g., $set) replaces everything except
//  _id in the matching document.
db.potions.update(
  {"name": "Love"},
  {"price": 3.99}
)
db.potions.find({"name": "Love"})
/* This query will return no results, since the name property is replaced */

//  5. Update multiple documents. Change the vendor for all potions from
//  Kettlecooked to KC
db.potions.update(
  {"vendor": "Kettlecooked"},
  {"$set": {"vendor":"KC"}},
  {multi: true}
)

//  6. Increment a property of an existing document. Increment the count
//  property of the Shrinking potion
db.potions.update(
  {"name": "Shrinking"},
  {"$inc": {"count": 1}}
)

/* The count property of Shrinking doesn't exist, yet, so it's created and
 incremented at the same time
 */

//  7. Attempt an increment on a property that doesn't exist. Increment the
//  count property of the Love potion.
db.potions.update(
  {"name": "Love"},
  {"$inc": {"count": 1}}
)
/* No results found */

//  8. Update a document, or if it doesn't exist, create a new one. Upsert the
//  an incremented count value on the potion named Love
db.potions.update(
  {"name": "Love"},
  {"$inc": {"count":1}},
  {upsert: true}
)
db.potions.find({"name":"Love"}).pretty()

//  9. Running the same command will increment the count property and will not
//  create a new document.
db.potions.update(
  {"name": "Love"},
  {"$inc": {"count":1}},
  {"upsert": true}
)
db.potions.find({"name":"Love"}).pretty()

//  10. Removing fields from documents. Remove the color field from all potions.
db.potions.update(
  {},
  {"$unset": {"color": ""}},
  {"multi": true}
)
db.potions.find().pretty()

//  11. Update a field to a new name. Change all 'score' fields to 'grade'.
db.potions.update(
  {},
  {"$rename": {"score": "grade"}},
  {"multi": true}
)
db.potions.find().pretty()

//  12. Change the value of a field. Change the 'secret' ingredient to 42
db.potions.update(
  {"ingredients": "secret"},
  {"$set": {"ingredients": 42}}
)
db.potions.find({"name":"Shrinking"})
/* This would actually replace all incredients with the number 42. See
 example 13 to show how to replace just the 'secret' ingredient */

load('reviews.js')
//  13. Change the value of a field. Change the 'secret' ingredient to 42.
//  Secret is the second value of the array, with an index of 1
db.potions.update(
  {"name": "Shrinking"},
  {"$set": {"ingredients.1": 42}}
)
db.potions.find({"name": "Shrinking"}).pretty()

load('reviews.js')
//  14. Change the value of a field, where the position is unknown. Change the
//  the 'secret' ingredient to 42.
db.potions.update(
  {"ingredients": "secret"},
  {"$set": {"ingredients.$": 42}},
  {"multi": true}
)
db.potions.find({"name": "Shrinking"}).pretty()

//  15. Update an embedded value. Change the strength of the Shrinking potion to
//  5
db.potions.update(
  {"name": "Shrinking"},
  {"$set": {"ratings.strength": 5}}
)
db.potions.find({"name": "Shrinking"}).pretty()

//  Other update operators include $max, $min, and $mul
//  See documentation https://docs.mongodb.org/manual/reference/operator/update/

//  16. Remove values from an array. Remove the last category from the
//  Shrinking potions categories array
db.potions.update(
  {"name": "Shrinking"},
  {"$pop": {"categories": 1}}
) // 1 removes the last element, -1 removes the first element
db.potions.find({"name": "Shrinking"}).pretty()

//  17. Add values to the end of an array. Add the budget category to the end
//  of the categories list for the Shrinking potion
db.potions.update(
  {"name": "Shrinking"},
  {"$push": {"categories": "budget"}}
)
db.potions.find({"name": "Shrinking"}).pretty()

//  18. Add value to the end of an array, unless it is already present. Add the
//  budget category to the Shrinking potion, unless it is already present
db.potions.update(
  {"name": "Shrinking"},
  {"$addToSet": {"categories": "budget"}}
)
db.potions.find({"name": "Shrinking"}).pretty()

//  19. Remove value from an array. Remove the tasty category from the
//  Shrinking potion
db.potions.update(
  {"name": "Shrinking"},
  {"$pull": {"categories": "tasty"}}
)
db.potions.find({"name": "Shrinking"}).pretty()

/******************************************************************************
 * Lab Exercises
 ******************************************************************************/
//  1. Write the command to remove the wand named "Doom Bringer"


//  2. Write the command to remove all wands having "Death" in their powers.


//  3. Write the command to update the wand with the name of "Devotion Shift"
//  and set the price to 5.99


//  4. Write the command to update all wands having "Fire" in their list of
//  powers to a minimum level_required of 2. Hint: use the $max update operator.


//  5. Using the logs collection which contains documents that record the name
//  and count for each wand, write the command that will increment the count
//  field for the wand named "Dream Bender", inserting the document if it
//  doesn't already exist.


//  6. Write the command to remove the smell field from all documents in the
//  wands collection.


//  7. Write the command to change the field called creator to maker in all
//  documents in the wands collection.


//  8. Write the command that will change the value "Fire" to "Fire Deflection"
// in the powers array of the wand named "Dream Bender"


//  9. Write the command that will change the value "Love" to "Love Burst" in
//  the powers array of all wands


//  10. Add the power "Spell Casting" to the end of the powers array of the wand
//  named "Dream Bender"


//  11. Add the power "Spell Casting" to the powers array of every wand if it
//  doesn't already exist


//  12. Multiply both the melee and magic values by 10. You'll need to separate
//  commands. Hint: Refer to the MongoDB documentation to learn how to use the
//  $mul operator: https://docs.mongodb.org/manual/reference/operator/update/mul/
