/******************************************************************************
 Your Name,  Lab 22
 CS364, Information Storage and Retrieval, Spring 2016
 Instructor: Lt Col Blair Watkinson

 Documentation:
 Your detailed documentation statement here

 Observations, Feedback, and questions:
 Any feedback or questions to the instructor here
 ******************************************************************************/

// reset reviews database
load('reviews.js')

//  1. Aggregations: Group documents. Display the unique vendors in the potions
//  collection.
    db.potions.aggregate(
      [{"$group": {"_id": "$vendor"}}]
    )

//  2. Aggregations: Count grouped documents. Count the number of potions made
//  by each vendor
    db.potions.aggregate([
      {"$group": {"_id":"$vendor", "total": {"$sum": 1}}}
    ])

//  3. Aggregations: Accumulate a value. Get the total grade by vendor
    db.potions.aggregate([
      {"$group": {
        "_id": "$vendor",
        "total": {"$sum":1},
        "grade_total": {"$sum": "$grade"}
      }}
    ])

//  4. Aggregations: Average a value. Get the average grade by vendor
    db.potions.aggregate([
      {"$group": {
        "_id": "$vendor",
        "total": {"$sum":1},
        "avg_grade": {"$avg": "$grade"}
      }}
    ])

//  5. Aggregations: Max value. Get the max grade by vendor
    db.potions.aggregate([
      {"$group": {
        "_id": "$vendor",
        "max_grade": {"$max": "$grade"}
      }}
    ])

//  6. Aggregations: Combining aggregations. Get the max and min grade by vendor
    db.potions.aggregate([
      {"$group": {
        "_id": "$vendor",
        "max_grade": {"$max": "$grade"},
        "min_grade": {"$min": "$grade"}
      }}
    ])

//  7. Aggregation pipeline: $match. Match all potions with unicorn in the
//  ingredients
    db.potions.aggregate([
      {"$match": {"ingredients": "unicorn"}}
    ]).pretty()

//  8. Aggregation pipeline: count the number of potions having unicorn as an
//  ingredient by vendor
    db.potions.aggregate([
      {"$match": {"ingredients": "unicorn"}},
      {"$group":
        {
          "_id": "$vendor",
          "potion_count": {"$sum": 1}
        }
      }
    ])

//  9. Aggregation pipeline: Find the top 3 vendors (by grade) having potions
//  under $15
    db.potions.aggregate([
      {"$match": {"price": {"$lt": 15}}},
      {"$group":
        {
          "_id": "$vendor",
          "avg_grade": {"$avg": "$grade"}
        }
      },
      {"$sort": {"avg_grade": -1}},
      {"$limit": 3}
    ])

//  10. Aggregation optimization. Execute the previous aggregation, but limit
//  the data sent from one stage to the next
    db.potions.aggregate([
      {"$match": {"price": {"$lt": 15}}},
      {"$project": {"_id": false, "vendor": true, "grade": true}},
      {"$group":
      {
        "_id": "$vendor",
        "avg_grade": {"$avg": "$grade"}
      }
      },
      {"$sort": {"avg_grade": -1}},
      {"$limit": 3}
    ])

load('reviews.js')


/******************************************************************************
 * Lab Exercises
 ******************************************************************************/
//  1. Write the command to list all unique wand makers in the database.


//  2. Write an aggregate that will report the number of wands for each
// magic damage score.


//  3. Write an aggregate that reports the total cost of all wands made by each
//  wand maker.


//  4. Write an aggregate that reports the average price of wands by level
//  required. Sort ascending by level required.


//  5. Write an aggregate that groups the wands by their maker and reports the
//  total number of wands produced by that maker, maximum magic damage by that
//  maker, and the lowest price of all wands by that maker.


//  6. Write an aggregate of all makers that produce a wand with the power "Air
//  Bolt". Show the lowest price per maker for wands that have "Air Bolt" power.


//  7. Write an aggregate to match wands that have a price under 50. Report the
//  results by wand maker, displaying only the makers whose average magic damage
//  is greater than 40. Hint: you will need a match command following the group
//  command.


//  8. Write an aggregate that will find the top 5 wand makers (by maximum magic
//  damage). Optimize your query by sending only the data necessary to
//  subsequent stages. Display both the wand maker and the maximum magic damage
//  that maker creates, sorted by maximum magic damage.




