
/*  Run the lab14_ConstructCo_schema file */
USE cs364_lab14;

/*  1. Write the SQL code to create a copy of employee, naming the copy emp_2.
    Then write the SQL code that will add the attributes emp_pct and proj_num to
    the structure.  The emp_pct is the bonus percentage to be paid to each
    employee.  The new attribute characteristics are:
        emp_pct     NUMERIC(4,2)
        proj_num    CHAR(3)
 */


/*  2. Write the SQL code to change emp_pct to the following values (you will
    need several SQL commands to complete this):
        emp_num is 103, emp_pct should be 3.85
        emp_num is 104 or 108, emp_pct should be 10.00
        emp_num is 102, emp_pct should be 8.00
        emp_num is 101 or 105, emp_pct should be 5.00
        emp_num is 109, emp_pct should be 2.00
        emp_num is 106, emp_pct should be 6.20
        emp_num is 107, emp_pct should be 5.15
 */


/*  3. Using a single command sequence, write the SQL code that will change the
    project number (proj_num) to 18 for all employees whose job classification
    (job_code) is 500. */


/*  4. Using a single command sequence, write the SQL code that will change the
    project number (proj_num) to 25 for all employees whose job classification
    (job_code) is 502 or higher. */


/*  5. Write the SQL code that will change the proj_num to 14 for employees who
    were hired before January 1, 1994, and who job code is at least 501. */


/*  7. Write the SQL code that will list all employee name and hire date
    information from the emp_2 table, sorted by last name, then by first
    name, then by middle initial */


/*  8. Write the SQL code to find that average bonus percentage in the emp_2
    table */


/*  9. Write the SQL code that will produce a listing of all the date in the
    emp_2 table in descending order by the bonus percentage */


/*  10. Write the SQL code that will list only the distinct project numbers
    in the emp_2 table */


/*  11. Write the SQL code that will delete the table emp_2 */


/*  12. Using the assignment table, write the SQL code that will yield the total
    number of hours worked for each employee and the total charges stemming from
    those hours worked.  The first several rows of running this query are below
    emp_num emp_lname   SumOfassign_hours   SumOfAssignCharge
    101     News        3.1                 387.50
    103     Arbough     19.7                1664.65
    104     Ramoras     11.9                1218.70 */


/*  13. Write a query to produce the total number of hours and charges for
    each of the projects represented in the assignment table.  The output is
    shown below
    proj_num        SumOfassign_hours   SumOfassign_charge
    16              20.5                1806.52
    18              23.7                1544.80
    22              27.0                2593.16
    25              19.4                1668.16 */


/*  14. Write the SQL code to count the number of employees */

/*  15. Write the SQL code to display the project information for the project
    with the largest project balance*/

