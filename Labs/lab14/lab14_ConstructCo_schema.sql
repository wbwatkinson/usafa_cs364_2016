/* Database Systems, Coronel/Morris */
/* Type of SQL : MySQL  */
DROP DATABASE IF EXISTS cs364_lab14;
CREATE DATABASE IF NOT EXISTS cs364_lab14;
USE cs364_lab14;

DROP TABLE IF EXISTS job;
DROP TABLE IF EXISTS assignment;
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS project;

CREATE TABLE job (
    job_code        VARCHAR(3)  PRIMARY KEY,
    job_description VARCHAR(25),
    job_chg_hour    NUMERIC(8,2),
    job_last_update DATE
);

CREATE TABLE employee (
    emp_num         VARCHAR(3)  PRIMARY KEY,
    emp_lname       VARCHAR(15),
    emp_fname       VARCHAR(15),
    emp_initial     VARCHAR(1),
    emp_hiredate    DATE,
    job_code        VARCHAR(3)  REFERENCES job(job_code),
    emp_years       INT(3)
);

CREATE TABLE project (
    proj_num        VARCHAR(3)  PRIMARY KEY,
    proj_name       VARCHAR(25),
    proj_value      FLOAT(8),
    proj_balance    FLOAT(8),
    emp_num         VARCHAR(3)  REFERENCES employee(emp_num)
);

CREATE TABLE assignment (
    assign_num      INT(5)      PRIMARY KEY,
    assign_date     DATE,
    proj_num        VARCHAR(3)  REFERENCES project(proj_num),
    emp_num         VARCHAR(3)  REFERENCES employee(emp_num),
    assign_job      VARCHAR(3),
    assign_chg_hr   NUMERIC(8,2),
    assign_hours    NUMERIC(8,1),
    assign_charge   NUMERIC(8,2)
);

INSERT INTO assignment VALUES('1001','2014-3-22','18','103','503','84.5','3.5','295.75');
INSERT INTO assignment VALUES('1002','2014-3-22','22','117','509','34.55','4.2','145.11');
INSERT INTO assignment VALUES('1003','2014-3-22','18','117','509','34.55','2','69.10');
INSERT INTO assignment VALUES('1004','2014-3-22','18','103','503','84.5','5.9','498.55');
INSERT INTO assignment VALUES('1005','2014-3-22','25','108','501','96.75','2.2','212.85');
INSERT INTO assignment VALUES('1006','2014-3-22','22','104','501','96.75','4.2','406.35');
INSERT INTO assignment VALUES('1007','2014-3-22','25','113','508','50.75','3.8','192.85');
INSERT INTO assignment VALUES('1008','2014-3-22','18','103','503','84.5','0.9','76.05');
INSERT INTO assignment VALUES('1009','2014-3-23','15','115','501','96.75','5.6','541.80');
INSERT INTO assignment VALUES('1010','2014-3-23','15','117','509','34.55','2.4','82.92');
INSERT INTO assignment VALUES('1011','2014-3-23','25','105','502','105','4.3','451.5');
INSERT INTO assignment VALUES('1012','2014-3-23','18','108','501','96.75','3.4','328.95');
INSERT INTO assignment VALUES('1013','2014-3-23','25','115','501','96.75','2','193.5');
INSERT INTO assignment VALUES('1014','2014-3-23','22','104','501','96.75','2.8','270.9');
INSERT INTO assignment VALUES('1015','2014-3-23','15','103','503','84.5','6.1','515.45');
INSERT INTO assignment VALUES('1016','2014-3-23','22','105','502','105','4.7','493.5');
INSERT INTO assignment VALUES('1017','2014-3-23','18','117','509','34.55','3.8','131.29');
INSERT INTO assignment VALUES('1018','2014-3-23','25','117','509','34.55','2.2','76.01');
INSERT INTO assignment VALUES('1019','2014-3-24','25','104','501','110.5','4.9','541.45');
INSERT INTO assignment VALUES('1020','2014-3-24','15','101','502','125','3.1','387.5');
INSERT INTO assignment VALUES('1021','2014-3-24','22','108','501','110.5','2.7','298.35');
INSERT INTO assignment VALUES('1022','2014-3-24','22','115','501','110.5','4.9','541.45');
INSERT INTO assignment VALUES('1023','2014-3-24','22','105','502','125','3.5','437.5');
INSERT INTO assignment VALUES('1024','2014-3-24','15','103','503','84.5','3.3','278.85');
INSERT INTO assignment VALUES('1025','2014-3-24','18','117','509','34.55','4.2','145.11');

/* -- */

INSERT INTO employee VALUES('101','News','John','G','2000-11-8','502','4');
INSERT INTO employee VALUES('102','Senior','David','H','1989-7-12','501','15');
INSERT INTO employee VALUES('103','Arbough','June','E','1996-12-1','503','8');
INSERT INTO employee VALUES('104','Ramoras','Anne','K','1987-11-15','501','17');
INSERT INTO employee VALUES('105','Johnson','Alice','K','1993-2-1','502','12');
INSERT INTO employee VALUES('106','Smithfield','William','','2004-6-22','500','0');
INSERT INTO employee VALUES('107','Alonzo','Maria','D','1993-10-10','500','11');
INSERT INTO employee VALUES('108','Washington','Ralph','B','1991-8-22','501','13');
INSERT INTO employee VALUES('109','Smith','Larry','W','1997-7-18','501','7');
INSERT INTO employee VALUES('110','Olenko','Gerald','A','1995-12-11','505','9');
INSERT INTO employee VALUES('111','Wabash','Geoff','B','1991-4-4','506','14');
INSERT INTO employee VALUES('112','Smithson','Darlene','M','1994-10-23','507','10');
INSERT INTO employee VALUES('113','Joenbrood','Delbert','K','1996-11-15','508','8');
INSERT INTO employee VALUES('114','Jones','Annelise','','1993-8-20','508','11');
INSERT INTO employee VALUES('115','Bawangi','Travis','B','1992-1-25','501','13');
INSERT INTO employee VALUES('116','Pratt','Gerald','L','1997-3-5','510','8');
INSERT INTO employee VALUES('117','Williamson','Angie','H','1996-6-19','509','8');
INSERT INTO employee VALUES('118','Frommer','James','J','2005-1-4','510','0');

/* -- */

INSERT INTO job VALUES('500','Programmer','35.75','2013-11-20');
INSERT INTO job VALUES('501','Systems Analyst','96.75','2013-11-20');
INSERT INTO job VALUES('502','Database Designer','125','2014-3-24');
INSERT INTO job VALUES('503','Electrical Engineer','84.5','2013-11-20');
INSERT INTO job VALUES('504','Mechanical Engineer','67.9','2013-11-20');
INSERT INTO job VALUES('505','Civil Engineer','55.78','2013-11-20');
INSERT INTO job VALUES('506','Clerical Support','26.87','2013-11-20');
INSERT INTO job VALUES('507','DSS Analyst','45.95','2013-11-20');
INSERT INTO job VALUES('508','Applications Designer','48.1','2014-3-24');
INSERT INTO job VALUES('509','Bio Technician','34.55','2013-11-20');
INSERT INTO job VALUES('510','General Support','18.36','2013-11-20');

/* -- */

INSERT INTO project VALUES('15','Evergreen','1453500','1002350','103');
INSERT INTO project VALUES('18','Amber Wave','3500500','2110346','108');
INSERT INTO project VALUES('22','Rolling Tide','805000','500345.2','102');
INSERT INTO project VALUES('25','Starflight','2650500','2309880','107');
