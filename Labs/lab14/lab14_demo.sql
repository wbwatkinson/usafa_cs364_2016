USE cs364_lesson18;

/* 7.5 Additional Data Definition Commands */

/* 7.5.1 Changing a column's data type */
ALTER TABLE     product
MODIFY COLUMN   p_descript  VARCHAR(45);


/* 7.5.2 Changing a column's data characteristics */
ALTER TABLE     product
MODIFY          p_price DECIMAL(9,2);


/* 7.5.3 Adding a column */
ALTER TABLE     product
ADD             p_salecode      CHAR(1);


/* 7.5.4 Dropping a column */
ALTER TABLE     vendor
DROP COLUMN     v_order;

/* 7.5.5 Advanced data updates */
START TRANSACTION;

UPDATE  product
SET     p_salecode = '2'
WHERE   p_code = '1546-QQ2';

UPDATE  product
SET     p_salecode = '1'
WHERE   p_code IN ('2232/QWE','2232/QTY');

SELECT  p_code, p_descript, p_indate,
        p_price,p_salecode
FROM    product;

UPDATE  product
SET     p_salecode = '2'
WHERE   p_indate < '2013-12-25';

UPDATE  product
SET     p_salecode = '1'
WHERE   p_indate >= '2014-01-16' AND p_indate <= '2014-02-10';

SELECT  p_code,p_descript,p_indate,p_price,p_salecode
FROM    product;

COMMIT; /* To not execute changes, execute the ROLLBACK command instead */

SELECT  p_code,p_descript,p_indate,p_price,p_salecode
FROM    product;

START TRANSACTION;

UPDATE	product
SET		p_qoh = p_qoh + 20
WHERE	p_code = '2232/QWE';

UPDATE	product
SET		p_price = p_price *1.10
WHERE	p_price < 50.00;

SELECT  p_code,p_descript,p_indate,p_price,p_salecode
FROM    product;

ROLLBACK;

SELECT  p_code,p_descript,p_indate,p_price,p_salecode
FROM    product;

/* 7.5.6 Copying parts of tables */
CREATE TABLE part(
	part_code		CHAR(8),
    part_descript	CHAR(35),
    part_price		DECIMAL(8,2),
    v_code			INTEGER,
    PRIMARY KEY (part_code));
    
INSERT INTO part	(part_code, part_descript, part_price, v_code)
SELECT				p_code,p_descript,p_price,v_code FROM product;

SELECT * FROM part;

/* Alternative method */

DROP TABLE IF EXISTS part;
CREATE TABLE part AS
SELECT	p_code AS part_code, p_descript AS part_descipt, p_price AS part_price, v_code
FROM	product;

SELECT * FROM part;

/* 7.5.7 Adding primary and foreign key designations */
ALTER TABLE	part
	ADD		PRIMARY KEY (part_code);

ALTER TABLE	part
	ADD		FOREIGN KEY (v_code) references vendor(v_code);

/* You can make multiple modifications at the same time */
#ALTER TABLE	part
#	ADD		PRIMARY KEY (part_code),
#	ADD		FOREIGN KEY (v_code) references vendor(v_code);

/* 	The following command won't work because the primary and foreign keys
	have already been defined on the line table */
#ALTER TABLE	line
#	ADD		PRIMARY KEY (inv_number, line_number),
#	ADD		FOREIGN KEY (inv_number) REFERENCES invoice(inv_number),
#   ADD		FOREIGN KEY (p_code) REFERENCES product(p_code);

/* 7.5.8 Deleting a table from the database */

/*  You cannot drop a table that is on the "one" side of any relationship.  Attempting to do
	so will violate referential integrity constraints */
DROP TABLE PART;
DROP TABLE IF EXISTS PART;

/* 7.6 Additional Select Query Keywords */

/* 7.6.1 Ordering a listing */
SELECT      p_code, p_descript, p_indate, p_price
FROM        product
ORDER BY    p_price;

/* Descending order */
SELECT      p_code, p_descript, p_indate, p_price
FROM        product
ORDER BY    p_price DESC;

/* Cascasing order sequence */
SELECT      emp_lname, emp_fname, emp_initial, emp_areacode, emp_phone
FROM        employee
ORDER BY    emp_lname, emp_fname, emp_initial;

/* Combined with other commands */
SELECT		p_descript, v_code, p_indate, p_price
FROM		product
WHERE		p_indate < '2014-01-21'
AND			p_price <= 50.00
ORDER BY	v_code, p_price DESC;

/* 7.6.2 Listing unique values */
SELECT DISTINCT	v_code
FROM			product;

/* 7.6.3 Aggregate functions */
/* Count */
SELECT	COUNT(DISTINCT v_code)
FROM	product;

SELECT	COUNT(DISTINCT v_code)
FROM	product
WHERE	p_price <= 10.00;

SELECT	COUNT(*)
FROM	product
WHERE	p_price <= 10.00;

/* Max and Min */
/* 	Find the most expensive product:
	This will not work... you cannot use MAX on the right side of the comparison operator 
	MAX can only be used in the column list of a SELECT statement */
#SELECT	p_code, p_descript, p_price
#FROM	product
#WHERE	p_price = MAX(p_price);

/* Instead use this */
SELECT	p_code, p_descript, p_price
FROM	product
WHERE	p_price IN (SELECT MAX(p_price) FROM product);

/*	Find the product with the highest inventory value */
SELECT	*
FROM	product
WHERE	p_qoh * p_price = (SELECT MAX(p_qoh*p_price) FROM product);

/* Sum */
SELECT 	SUM(cus_balance) AS totbalance
FROM	customer;

SELECT	SUM(p_qoh * p_price) AS totvalue
FROM	product;

/* Average */
SELECT	AVG(p_price) FROM product;

/* All products whose price is less than the average */
SELECT		p_code, p_descript, p_qoh, p_price, v_code
FROM		product
WHERE		p_price < (SELECT avg(p_price) FROM product)
ORDER BY	p_price DESC;

/* 7.6.4 Grouping data */
SELECT * FROM product;

/* Find the minimum price for each sale code */
SELECT		p_salecode, MIN(p_price)
FROM		product
GROUP BY	p_salecode
ORDER BY	p_salecode;

/* Find the average price within each sale code */
SELECT		p_salecode, AVG(p_price)
FROM		product
GROUP BY	p_salecode
ORDER BY	p_salecode;

/* Find the number of products from each vendor */
SELECT		v_code, COUNT(DISTINCT p_code)
FROM		product
GROUP BY	v_code;

/* 	Find the number of products and average price of products from each vendor 
	if the average price of products from that vendor is less than $10 */
SELECT		v_code, COUNT(DISTINCT p_code), AVG(p_price)
FROM 		product
GROUP BY	v_code
HAVING		AVG(p_price) < 10;

/*	Find the inventory cost from each vendor is the inventory cost from the vendor 
	is greater than $500.  Display results in descending order on price.  */
SELECT		v_code, SUM(p_qoh*p_price) AS totcost
FROM		product
GROUP BY	v_code
HAVING		totcost > 500
ORDER BY	totcost DESC;