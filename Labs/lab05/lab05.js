/******************************************************************************
 Your Name,  Assignment Name
 CS364, Information Storage and Retrieval, Spring 2016
 Instructor: Lt Col Blair Watkinson

 Documentation:
 Your detailed documentation statement here

 Observations, Feedback, and questions:
 Any feedback or questions to the instructor here
 ******************************************************************************/

var usafaClasses = [
  {
    class: '2000',
    exemplar: 'General James H. &ldquo;Jimmy&rdquo; Doolittle',
    graddate: '31 May 2000'
  },
  {
    class: '2001',
    exemplar: 'Brigadier General William &ldquo;Billy&rdquo; Mitchell',
    graddate: '30 May 2001'
  },
  {class: '2002', exemplar: 'Captain Lance P. Sijan', graddate: '29 May 2002'},
  {class: '2003', exemplar: 'Major Richard I. Bong', graddate: '28 May 2003'},
  {
    class: '2004',
    exemplar: 'Captain Eddie Rickenbacker',
    graddate: '2 Jun 2004'
  },
  {
    class: '2005',
    exemplar: 'General George S. Patton Jr.',
    graddate: '1 Jun 2005'
  },
  {
    class: '2006',
    exemplar: 'General Carl A. &ldquo;Tooey&rdquo; Spaatz',
    graddate: '31 May 2006'
  },
  {
    class: '2007',
    exemplar: 'Lieutenant Colonel Virgil I. &ldquo;Gus&rdquo; Grissom',
    graddate: '30 May 2007'
  },
  {class: '2008', exemplar: '1LT Karl W. Richter', graddate: '28 May 2008'},
  {
    class: '2009',
    exemplar: 'Colonel Hubert &ldquo;Hub&rdquo; Zemke',
    graddate: '27 May 2009'
  },
  {class: '2010', exemplar: '2Lt Frank Luke Jr.', graddate: '26 May 2010'},
  {
    class: '2011',
    exemplar: 'Brigadier General Robin Olds',
    graddate: '25 May 2011'
  },
  {
    class: '2012',
    exemplar: 'General of the AF Henry H. &ldquo;Hap&rdquo; Arnold',
    graddate: '23 May 2012'
  },
  {class: '2013', exemplar: 'General Curtis E. Lemay', graddate: '29 May 2013'},
  {
    class: '2014',
    exemplar: 'Lieutenant Colonel Jay Zeamer Jr.',
    graddate: '28 May 2014'
  },
  {
    class: '2015',
    exemplar: 'Wilbur and Orville Wright',
    graddate: '28 May 2015'
  },
  {class: '2016', exemplar: 'Major David Brodeur', graddate: ''},
  {
    class: '2017',
    exemplar: 'Col George Everett &lsquo;Bud&rsquo; Day',
    graddate: ''
  }];

/* insertTable(classes) generates a table from an array of objects */
function insertTable(classes) {

}

/* Add a JQuery function that listens for the document to be ready and then
executes.  Within that function, call the insertTable method, and then create
mouseenter, mouseleave, and click events
 */


