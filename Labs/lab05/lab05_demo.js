/******************************************************************************
 Your Name,  Assignment Name
 CS364, Information Storage and Retrieval, Spring 2016
 Instructor: Lt Col Blair Watkinson

 Documentation:
 Your detailed documentation statement here

 Observations, Feedback, and questions:
 Any feedback or questions to the instructor here
 ******************************************************************************/

"use strict";

var fruits = [
  {count: 3, kind: "oranges"},
  {count: 52, kind: "grapes"},
  {count: 6, kind: "bananas"}
];

$('document').ready(function () {

  var fruitStrings = fruits.map(function (fruit) {
    return "<p>There are " + fruit.count + " " + fruit.kind + " in the basket</p>";
  });

  fruitStrings.forEach(function (fruitString) {
    console.log(fruitString);
  });

  fruitStrings.forEach(function (fruitString) {
    $('div').append(fruitString);
  });
});