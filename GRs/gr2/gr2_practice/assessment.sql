--  Instructions
--  Within MySQL Workbench with a connection to MySQL running in XAMP, run
--    the following sql scripts:
--    1. sakila-schema.sql
--    2. sakila-data-modified.sql.  When prompted, say OK to using the LATIN1
--       character set encoding name.
--  Reference the sakila EER diagram describing the Sakila database's schema:
--    1. From the Home tab, open the sakila.mwb file as an existing EER
--    2. When prompted to rename the indexes using the standard renaming, say
--       'Rename'
--  On the answer_sheet.sql, answer the following questions:


--  1. (20 points) Create a soaring database for tracking soaring pilots and the
--     maneuvers they perform during competitions.  (See also the tables in
--     question 2 for an idea what the database should look like.)
--     a.	For the pilots, store a pilot ID number, a pilot last name, and a
--        pilot phone number.  Ensure you set an appropriate primary key.
--     b.	For the maneuvers, store a maneuver performed ID number, maneuver
--        name, maneuver grade, and maneuver date.  Ensure you set an
--        appropriate primary key.  Ensure you define any foreign keys that are
--        appropriate.
--     c.	Require that all pilots and maneuvers have values for all attributes.
--     d.	Track the pilot that performed each maneuver assuming that each
--        maneuver performance can be done by only one pilot but that each pilot
--        can do many maneuver performances.
--     e.	Ensure that no pilot can be deleted out of the database until all her
--        maneuvers are manually deleted first.
--     f.	If a pilot changes pilot ID numbers, ensure the pilot ID is updated
--        appropriately in the maneuvers table.


--  2.  (20 points) Add the following information to your pilot maneuvers
--      performed database:
--
--      Pilots:
--      Pilot ID        Pilot Last Name         Pilot Phone
--      001             Yeager                  7195551212
--      002             Doolittle               3165551212
--      003             Earhart                 2125551212
--
--      Maneuvers Performed:
--      Maneuver ID     Pilot       Maneuver Name     Maneuver Grade    Maneuver Date
--      0001            Doolittle   Split S           C                 13 Jan 2015
--      0002            Yeager      Wingover          A                  3 Dec 2014
--      0003            Earhart     Wingover          B                 13 Jan 2015


--  3.  (20 points) Write SQL commands to do the following:
--      a. There has been a change to the grading criteria, and all wingover
--         maneuvers will now be ungraded.  Change the database so that all
--         wingover maneuvers are graded 'P' for performed.
--      b. Remove all maneuvers performed before year 2015.
--      c. Show all attributes for all pilots.

--  For the following questions, use the Sakila database
--  4.  (20 points) Write a query to obtain the customer ID, staff ID, rental
--      ID, payment date, and amount for all payments greater than eight dollars
--      made in either July or August of 2005.  Order them by payment date.
--      (There are 673 such records.)


--  5.  (16 points) Write a query to obtain the first name and last name of
--      each customer who has not rented a video.  The result set is below:
--
--      first_name    last_name
--      Bryan         Williams


--  6.  (24 points) Report the average number of times that videos beginning
--      with the letter A were rented. (Hint: each video was rented at least
--      once, so you need not check for videos never checked out.)  Use the
--      column name average_times_rented in your result.  The result set is
--      below.
--
--      average_times_rented
--      17.6585


--  7.  (30 points) Produce a report showing the number of times each actor has
--      acted in each category of film.   List only those actors who performed
--      in a particular category 6 or more times.  Order by performances so that
--      the actor who performed the most times in one category is listed first.
--      Show the following fields: actor first name, actor last name, category,
--      and number performances.  The first several rows are shown below (your
--      solution may have a different order after the first record).
--
--      first_name      last_name   Category    Count(film_id)
--      Ben             Willis      Sports      9
--      Julia           McQueen     Horror      7
--      Humphrey        Willis      Foreign     7


