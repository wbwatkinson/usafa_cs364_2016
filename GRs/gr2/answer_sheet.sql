--  Instructions
--  Within MySQL Workbench with a connection to MySQL running in XAMP, run
--    the following sql scripts:
--    1. sakila-schema.sql
--    2. sakila-data-modified.sql.  When prompted, say OK to using the LATIN1
--       character set encoding name.
--  Reference the sakila EER diagram describing the Sakila database's schema:
--    1. From the Home tab, open the sakila.mwb file as an existing EER
--    2. When prompted to rename the indexes using the standard renaming, say
--       'Rename'


-- CS 364 Assessment Answer Sheet

-- Name:

-- Section:

-- #1 --------------------------------------------------------------------------


-- #2 --------------------------------------------------------------------------


-- #3 --------------------------------------------------------------------------


-- FOR THE REMAINDER OF THE ASSESSMENT USE THE SAKILA DATABASE --

USE SAKILA;

-- #4 --------------------------------------------------------------------------


-- #5 --------------------------------------------------------------------------


-- #6 --------------------------------------------------------------------------


-- #7 --------------------------------------------------------------------------
